/*!******************************************************************************
 * @file data_out.c
 * @brief output function for frizz
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <xtensa/tie/frizz_simd4w_que.h>
#include "dataform.h"
#include "command.h"
#include "data_out.h"

#define DATA_FORM_NUM(x) (sizeof(x) / sizeof(int))

static void data_out_put( FRIZZ_DATA_HEADER *ph, void *pd )
{
	unsigned int i;
	oq_sensor_out_push( ph->w );
	for( i = 0; i < ph->num; i++ ) {
		oq_sensor_out_push( ( ( unsigned int* )pd )[i] );
	}
}

static void data_out_generic_core( unsigned char id, unsigned char num, void *p )
{
	FRIZZ_DATA_HEADER head;
	// setting header
	head.prefix = FRIZZ_DATA_PREFIX;
	head.data_id = id;
	head.num = num;
	// output
	data_out_put( &head, p );
}

static void data_out_uint1( unsigned char data_id, unsigned int val )
{
	FRIZZ_DATA_FORM_UINT1 data;
	// data
	data.val[0] = val;
	// output
	data_out_generic_core( data_id, DATA_FORM_NUM( data ), &data );
}

static void data_out_float1( unsigned char data_id, frizz_tie_fp val )
{
	FRIZZ_DATA_FORM_FLOAT1 data;
	// data
	data.val[0] = val;
	// output
	data_out_generic_core( data_id, DATA_FORM_NUM( data ), &data );
}

static void data_out_float3( unsigned char data_id, frizz_tie_fp *val )
{
	FRIZZ_DATA_FORM_FLOAT3 data;
	unsigned int i;
	// data
	for( i = 0; i < DATA_FORM_NUM( data ); i++ ) {
		data.val[i] = val[i];
	}
	// output
	data_out_generic_core( data_id, DATA_FORM_NUM( data ), &data );
}

static void data_out_float3x3p1( unsigned char data_id, frizz_tie_fp *val0, frizz_tie_fp *val1, frizz_tie_fp *val2, frizz_tie_fp val3 )
{
	FRIZZ_DATA_FORM_FLOAT10 data;
	unsigned int i;
	// data
	for( i = 0; i < 3; i++ ) {
		data.val[0 + i] = val0[i];
	}
	for( i = 0; i < 3; i++ ) {
		data.val[3 + i] = val1[i];
	}
	for( i = 0; i < 3; i++ ) {
		data.val[6 + i] = val2[i];
	}
	data.val[9] = val3;
	// output
	data_out_generic_core( data_id, DATA_FORM_NUM( data ), &data );
}

static void data_out_float9( unsigned char data_id, frizz_tie_fp *val )
{
	FRIZZ_DATA_FORM_FLOAT9 data;
	unsigned int i;
	// data
	for( i = 0; i < DATA_FORM_NUM( data ); i++ ) {
		data.val[i] = val[i];
	}
	// output
	data_out_generic_core( data_id, DATA_FORM_NUM( data ), &data );
}

/**
 * @brief general purpose output
 *
 * @param id data ID #FRIZZ_DATA_ID
 * @param num payload word number
 * @param p payload
 */
void data_out_generic( unsigned char id, unsigned char num, void *p )
{
	data_out_generic_core( id, num, p );
}

/**
 * @brief FW version output
 *
 * @param ver firmware version
 */
void data_out_fw_version( unsigned int ver )
{
	data_out_uint1( FRIZZ_DATA_ID_FW_VERSION, ver );
}

/**
 * @brief 9D sensor output
 *
 * @param gyro[3] gyro 0:x, 1:y, 2:z
 * @param accl[3] accl 0:x, 1:y, 2:z
 * @param magn[3] magn 0:x, 1:y, 2:z
 * @param dtm sampling time
 */
void data_out_9dsensors( frizz_tie_fp gyro[3], frizz_tie_fp accl[3], frizz_tie_fp magn[3], frizz_tie_fp dtm )
{
	data_out_float3x3p1( FRIZZ_DATA_ID_9DSENSORS, gyro, accl, magn, dtm );
}

/**
 * @brief 3D accel  sensor output
 *
 * @param data[3] 0:x, 1:y, 2:z
 */
void data_out_accl( frizz_tie_fp data[3] )
{
	data_out_float3( FRIZZ_DATA_ID_ACCELEROMETER, data );
}

/**
 * @brief 3D magnet sensor output
 *
 * @param data[3] 0:x, 1:y, 2:z
 */
void data_out_magn( frizz_tie_fp data[3] )
{
	data_out_float3( FRIZZ_DATA_ID_MAGNETOMETER, data );
}

/**
 * @brief 3D gyro sensor output
 *
 * @param data[3] 0:x, 1:y, 2:z
 */
void data_out_gyro( frizz_tie_fp data[3] )
{
	data_out_float3( FRIZZ_DATA_ID_GYROSCOPE, data );
}

/**
 * @brief pressure sensor output
 *
 * @param data[3] 0:x, 1:y, 2:z
 */
void data_out_baro( frizz_tie_fp data )
{
	data_out_float1( FRIZZ_DATA_ID_BAROMETER, data );
}

/**
 * @brief slope sensor output
 *
 * @param data[3] 0:yaw, 1:pitch, 2:roll
 */
void data_out_orientation( frizz_tie_fp data[3] )
{
	data_out_float3( FRIZZ_DATA_ID_ORIENTATION, data );
}

/**
 * @brief PDR positioning result output
 *
 * @param loc[2] relative position 0:x, 1:y
 * @param vel[2] relative speed 0:x, 1:y
 * @param state PDR state
 * @param step_cnt step count
 * @param total_dst accumulation distance(m)
 */
void data_out_pdr_output( frizz_tie_fp loc[2], frizz_tie_fp vel[2], unsigned int state, unsigned int step_cnt, frizz_tie_fp total_dst )
{
	FRIZZ_DATA_HEADER head;
	FRIZZ_DATA_FORM_PDR_OUTPUT data;

	// header
	head.prefix = FRIZZ_DATA_PREFIX;
	head.data_id = FRIZZ_DATA_ID_PDR_OUTPUT;
	head.num = sizeof( FRIZZ_DATA_FORM_PDR_OUTPUT ) / sizeof( int );
	// data
	data.loc[0] = loc[0];
	data.loc[1] = loc[1];
	data.vel[0] = vel[0];
	data.vel[1] = vel[1];
	data.state = state;
	data.step_cnt = step_cnt;
	data.total_dst = total_dst;
	// output
	data_out_put( &head, &data );
}

/**
 * @brief Hard-Iron parameter output
 *
 * @param data[3] Hard-Iron parameter
 */
void data_out_hardiron_param( frizz_tie_fp data[3] )
{
	data_out_float3( FRIZZ_DATA_ID_HARDIRON_PARAM, data );
}

/**
 * @brief Soft-Iron parameter output
 *
 * @param data[9] Soft-Iron parameter
 */
void data_out_softiron_param( frizz_tie_fp data[9] )
{
	data_out_float9( FRIZZ_DATA_ID_SOFTIRON_PARAM, data );
}

