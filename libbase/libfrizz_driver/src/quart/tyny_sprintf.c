#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "tyny_sprintf.h"

static int tiny_vsprintf_d( long val , char* buff , int zero_flag , int width )
{
	int				i;
	char			tmp_buffer_d[16];
	char			*ptmp	= tmp_buffer_d + 14;
	int				len		= 0;
	int				minus	= 0;
	int				first	= 0;
	memset( tmp_buffer_d, 0, sizeof( tmp_buffer_d ) );
	if( val == 0 ) {
		*( ptmp-- ) = '0';
		len++;
	} else {
		if( val < 0 ) {
			val = ~val;
			val++;
			minus = 1;
		}
		first = 1;
		while( val || first ) {
			first = 0;
			if( len >= sizeof( tmp_buffer_d ) - 2 ) {
				break;
			}

			*ptmp = ( val % 10 ) + '0';
			val = val / 10;
			ptmp--;
			len++;
		}
	}

	if( zero_flag ) {
		if( minus ) {
			width--;
		}
		while( len < width ) {
			*( ptmp-- ) =  '0';
			len++;
		}
		if( minus ) {
			*( ptmp-- ) = '-';
			len++;
		}
	} else {
		if( minus ) {
			*( ptmp-- ) = '-';
			len++;
		}
		while( len < width ) {
			*( ptmp-- ) =  ' ';
			len++;
		}
	}

	for( i = 0 ; i < len ; i++ ) {
		*( buff++ ) = *( ++ptmp );
	}

	return len;
}

static int tiny_vsprintf_u( unsigned long val , char* buff , int zero_flag , int width )
{
	int				i;
	char			tmp_buffer_d[16];
	char			*ptmp	= tmp_buffer_d + 14;
	int				len		= 0;
	int				first	= 0;
	memset( tmp_buffer_d, 0, sizeof( tmp_buffer_d ) );
	if( val == 0 ) {
		*( ptmp-- ) = '0';
		len++;
	} else {
		first = 1;
		while( val || first ) {
			first = 0;
			if( len >= sizeof( tmp_buffer_d ) - 2 ) {
				break;
			}

			*ptmp = ( val % 10 ) + '0';
			val = val / 10;
			ptmp--;
			len++;
		}
	}

	if( zero_flag ) {
		while( len < width ) {
			*( ptmp-- ) =  '0';
			len++;
		}
	} else {
		while( len < width ) {
			*( ptmp-- ) =  ' ';
			len++;
		}
	}

	for( i = 0 ; i < len ; i++ ) {
		*( buff++ ) = *( ++ptmp );
	}

	return len;
}


static int tiny_vsprintf_f( double val , char* buff , int zero_flag , int width, int under )
{
	int				i;
	char			tmp_buffer_f[32];
	char			*ptmp		= tmp_buffer_f + 30;
	int				val_int		= 0;
	double			val_under	= 0;
	double			val_under_tmp = 0;
	int				val_tmp		= 0;
	int				len			= 0;
	int				minus		= 0;
	unsigned int	first		= 0;
	int				under_nolen = 0;

	if( under == 0 ) {
		under = 9;		// 32-bit Int Max Length
		under_nolen = 1;
	}

	memset( tmp_buffer_f, 0, sizeof( tmp_buffer_f ) );
	if( val == 0.0 ) {
		while( under ) {
			*( ptmp-- ) = '0';
			under--;
		}
		*( ptmp-- ) = '.';
		*( ptmp-- ) = '0';
		len++;
	} else {
		val_int		= ( int ) val ;
		val_under	= val - ( double ) val_int;
		if( val_under < 0.0 ) {
			minus = 1;
			val_under = val_under * -1.0;
		}
		if( val_int < 0 ) {
			val_int = ~val_int;
			val_int++;
			minus = 1;
		}
		first = 1;
		val_under_tmp = val_under;
		for( i = 0 ; i < under ; i++ ) {
			val_under = val_under * 10;
			first     = first     * 10;
			if( ( ( val_under_tmp * first ) == ( float )( ( int )val_under ) ) && ( under_nolen != 0 ) ) {
				break;
			}
		}
		first     = first     / 10;
		val_tmp = ( int ) val_under;
		while( val_tmp || first ) {
			if( len >= sizeof( tmp_buffer_f ) - 2 ) {
				break;
			}

			*ptmp = ( val_tmp % 10 ) + '0';
			val_tmp = val_tmp / 10;
			first   = first   / 10;
			ptmp--;
			len++;
		}
		*ptmp = '.';
		ptmp--;
		len++;

		first = 1;
		while( val_int || first ) {
			first = 0;
			if( len >= sizeof( tmp_buffer_f ) - 2 ) {
				break;
			}

			*ptmp = ( val_int % 10 ) + '0';
			val_int = val_int / 10;
			ptmp--;
			len++;
		}

	}

	if( zero_flag ) {
		if( minus ) {
			width--;
		}
		while( len < width ) {
			*( ptmp-- ) =  '0';
			len++;
		}
		if( minus ) {
			*( ptmp-- ) = '-';
			len++;
		}
	} else {
		if( minus ) {
			*( ptmp-- ) = '-';
			len++;
		}
		while( len < width ) {
			*( ptmp-- ) =  ' ';
			len++;
		}
	}

	for( i = 0 ; i < len ; i++ ) {
		*( buff++ ) = *( ++ptmp );
	}

	return len;
}

static int tiny_vsprintf_h( unsigned long val, char* buff, int capital, int zero_flag, int width )
{
	int				i;
	char			tmp_buffer_h[16];
	char			*ptmp	= tmp_buffer_h + 14;
	int				len		= 0;
	char			str_a;

	memset( tmp_buffer_h, 0, sizeof( tmp_buffer_h ) );
	if( capital ) {
		str_a = 'A';
	} else {
		str_a = 'a';
	}

	if( val == 0 ) {
		*( ptmp-- ) = '0';
		len++;
	} else {
		while( val ) {
			if( len >= sizeof( tmp_buffer_h ) - 2 ) {
				break;
			}
			*ptmp = ( unsigned char )( val % 16 );
			if( *ptmp > 9 ) {
				*ptmp += str_a - 10;
			} else {
				*ptmp += '0';
			}

			val >>= 4;
			ptmp--;
			len++;
		}
	}

	while( len < width ) {
		*( ptmp-- ) =  zero_flag ? '0' : ' ';
		len++;
	}

	for( i = 0 ; i < len ; i++ ) {
		*( buff++ ) = *( ++ptmp );
	}

	return len;
}

static int tiny_vsprintf_c( int ch, char* buff )
{
	*buff = ( char )ch;
	return 1;
}


static int tiny_vsprintf_s( char* str, char* buff )
{
	int		count = 0;
	while( *str ) {
		*( buff++ ) = *str;
		str++;
		count++;
	}
	return count;
}

void tiny_vsprintf( char* buff, char* fmt, va_list arg )
{
	int			len;
	int			size;
	int			zeroflag, width, under;

	size	= 0;
	len		= 0;

	while( *fmt ) {
		if( *fmt == '%' ) {
			zeroflag = width = under = 0;
			fmt++;

			if( *fmt == '0' ) {
				fmt++;
				zeroflag = 1;
			}
			if( ( *fmt >= '0' ) && ( *fmt <= '9' ) ) {
				width = *( fmt++ ) - '0';
			}
			if( *fmt == '.' ) {
				fmt++;
				if( ( *fmt >= '0' ) && ( *fmt <= '9' ) ) {
					under = *( fmt++ ) - '0';
				}
			}

			switch( *fmt ) {
			case 'd':
			case 'l':
				size = tiny_vsprintf_d( va_arg( arg, signed long ), buff, zeroflag, width );
				break;
			case 'u':
				size = tiny_vsprintf_u( va_arg( arg, unsigned long ), buff, zeroflag, width );
				break;
			case 'x':
				size = tiny_vsprintf_h( va_arg( arg, unsigned long ), buff, 0, zeroflag, width );
				break;
			case 'X':
				size = tiny_vsprintf_h( va_arg( arg, unsigned long ), buff, 1, zeroflag, width );
				break;
			case 'c':
				size = tiny_vsprintf_c( va_arg( arg, int ), buff );
				break;
			case 's':
				size = tiny_vsprintf_s( va_arg( arg, char* ), buff );
				break;
			case 'f':
				size = tiny_vsprintf_f( va_arg( arg, double ), buff, zeroflag, width, under );
				break;
			default:
				len++;
				*( buff++ ) = *fmt;
				break;
			}
			len		+= size;
			buff	+= size;
			fmt++;
		} else {
			*( buff++ ) = *( fmt++ );
			len++;
		}
	}

	*buff = '\0';

	va_end( arg );
	return;
}



void tiny_sprintf( char* buff, char* fmt, ... )
{
	va_list		arg;
	//
	va_start( arg, fmt );
	tiny_vsprintf( buff, fmt, arg );
	va_end( arg );
	return;
}

void tiny_printf( char* fmt, ... )
{
	va_list		arg;
	char		buff[1024];
	//
	va_start( arg, fmt );
	tiny_vsprintf( buff, fmt, arg );
	va_end( arg );
	//puts(buff);
	fputs( ( const char * )buff, stdout );
	return;
}

