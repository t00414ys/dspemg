/*!******************************************************************************
 * @file uart.c
 * @brief source code for UART Driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <xtensa/xtruntime.h>
#include <xtensa/hal.h>
#include <xtensa/config/core.h>
#include <xtensa/config/system.h>

#include "frizz_peri.h"
#include "uart.h"

#define	D_UART_FIFO_INT_DISABLE		// Disable interrupt for FIFO Reading/Writing
/**
 * @brief IE Register Bit Flag
 */
#define UART_IE_HW_FLOW		0x10	// HW flow control enable
#define UART_IE_CTS			0x08	// CTS interrupt
#define UART_IE_FIFO_EMPTY	0x04	// transmission FIFO Empty
#define UART_IE_RX			0x02	// receive data detect
#define UART_IE_ERROR		0x01	// error detect

/**
 * @brief FC Register Bit Flag
 */
#define UART_FC_TX_RESET	0x04	// transmission FIFO reset
#define UART_FC_RX_RESET	0x02	// receive FIFO reset

/**
 * @brief UART Module Context
 */
typedef struct {
	unsigned int	freq;
	sUART_Setting	set;
	uart_callback	pf;
	void*			arg;
} sUART_Context;
sUART_Context g_uart;

/**
 * @brief interrupt handler
 */
static void int_handler( void )
{
	unsigned int ii, ls;
	ii = *REGUART_II;
	ls = *REGUART_LS;
	if( ii & UART_IE_CTS ) {
		*REGUART_II = UART_IE_CTS;
	}
	if( ii & UART_IE_FIFO_EMPTY ) {
		*REGUART_II = UART_IE_FIFO_EMPTY;
	}
	if( ii & UART_IE_RX ) {
		*REGUART_II = UART_IE_RX;
	}
	if( ii & UART_IE_ERROR ) {
		*REGUART_II = UART_IE_ERROR;
	}
	if( g_uart.pf != 0 ) {
		g_uart.pf( g_uart.arg, ls );
	}
}

/**
 * @brief Xtensa Interrupt setting for UART
 */
static int init_interrupt( void )
{
	_xtos_ints_off( 1 << INTERRUPT_NO_UART );
	// setting ISR
	_xtos_set_interrupt_handler( INTERRUPT_NO_UART, ( _xtos_handler )int_handler );
	// setting IMask
	_xtos_ints_on( 1 << INTERRUPT_NO_UART );
	return 0;
}

/**
 * @brief UART Block Init
 */
static void init_register( void )
{
	sUART_Setting* set;
	unsigned int tmp;
	float		 ftmp;
	// disable Interrupt
	*REGUART_IE = 0x00;
	// setting switch to DL Register
	*REGUART_LC = 0x80;
	// stop baudrate
	*REGUART_DL = 0x00;
	// clear status
	*REGUART_FC = UART_FC_RX_RESET | UART_FC_TX_RESET;	// 1Byte, FIFO reset
	//*REGUART_II = *REGUART_II;
	*REGUART_II = 0x000F;								// Interrupt Clear
	// setting data format
	set = &( g_uart.set );
	*REGUART_LC |=
		/*	     ((0)											<< 6) |	// disable break signal
			     ((0)											<< 5) |	// disable stick parity
		*/ ( ( set->parity )									<< 4 ) |	// parity check type
		( ( ( set->parity == UART_ParityNone ) ? 0 : 1 )	<< 3 ) |	// parity check enable
		( ( set->stop )									<< 2 ) |	// stop bit
		( ( set->data )									<< 0 )	// data bit num
		;
	// setting HW Flow Control: threshold for RTS
	*REGUART_MC = set->fifo_num;	// 1 Byte
	// setting buadrate

	// SDK 1.0 Source
	//tmp = g_uart.freq >> 4;
	//tmp = (2 * tmp + set->baudrate) / (2 * (set->baudrate));

	// bps = CLK_PERI / (16 * REG_DL)
	// Ex) REG_DL=1   : bps =(40MHz/(16*1) = 2.5Mbps
	//     REG_DL=255 : bps =(40MHz/(16*1) = 9.804KHz
	// bps = freq / (16 * REG_DL);
	// 1 =  freq / (16 * REG_DL * bps);		/* Divide both sides by bps */
	// REG_DL =  freq / (16 * bps);			/* multiple both sides by REG_DL */

	ftmp = ( float )( g_uart.freq ) / ( float )( 16 * ( set->baudrate ) );
	tmp = ( int )( ftmp + 0.5 );				// to round to the nearest whole number
	*REGUART_DL = tmp;
	// ref to RB/TR
	*REGUART_LC &= ~0x80;
	// enable all Interrupt
	tmp = ( UART_IE_FIFO_EMPTY | UART_IE_RX | UART_IE_ERROR );
	if( set->flow == UART_FlowControlHardware ) {
		tmp |= ( UART_IE_HW_FLOW | UART_IE_CTS );
	}
	*REGUART_IE = tmp;
}

int uart_init( unsigned int freq, sUART_Setting* set, uart_callback pf, void* arg )
{
	g_uart.freq = freq;
	g_uart.set = *set;
	g_uart.pf = pf;
	g_uart.arg = arg;
	init_register();
	init_interrupt();
	return 0;
}

int uart_reset( void )
{
	init_register();
	return 0;
}

UART_LS uart_get_status( unsigned int* rx_num )
{
	UART_LS ls = *REGUART_LS;
	*rx_num = *REGUART_RC;
	return ls;
}

int uart_snd_data( unsigned char *data, unsigned int size )
{
	unsigned int count;
	unsigned int emp = *REGUART_TF - *REGUART_TC;
	if( emp < size ) {
		size = emp;
	}
	for( count = 0; count < size; count++ ) {
		*REGUART_TR = *data;
		data++;
	}
	//set interrupt
	return ( int ) size;
}

int uart_get_data( unsigned char *data, unsigned int size )
{
	unsigned int count;
	count = *REGUART_RC;
	if( count < size ) {
		size = count;
	}
	for( count = 0; count < size; count++ ) {
#ifdef	D_UART_FIFO_INT_DISABLE
		_xtos_set_intlevel( 5 );
#endif
		*data = *REGUART_RB;
#ifdef	D_UART_FIFO_INT_DISABLE
		_xtos_set_intlevel( 0 );
#endif
		data++;
	}
	if( 0 < size ) {
		// enable Rx interrupt
		*REGUART_IE |= UART_IE_RX;
	}

	return ( int ) size;
}

void uart_lock( void )
{
	_xtos_ints_off( 1 << INTERRUPT_NO_UART );
}

void uart_unlock( void )
{
	// unlock
	_xtos_ints_on( 1 << INTERRUPT_NO_UART );
}

