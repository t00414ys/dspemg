/*!******************************************************************************
 * @file gpio.h
 * @brief header for GPIO driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __GPIO_H__
#define __GPIO_H__
/** @addtogroup GPIO
 */
/** @ingroup GPIO */
/* @{ */

#ifdef __cplusplus
extern "C" {
#endif

/** @enum eGPIO_NO
 *  @brief GPIO NO
 */
typedef enum {
	GPIO_NO_0 = 0,		///< GPIO0 terminal
	GPIO_NO_1,			///< GPIO1 terminal
	GPIO_NO_2,			///< GPIO2 terminal
	GPIO_NO_3,			///< GPIO3 terminal
	GPIO_NO_DELIMITER	///< Delimiter
} eGPIO_NO;

/** @enum eGPIO_MODE
 *  @brief GPIO MODE
 */
typedef enum {
	GPIO_MODE_OUT = 0,	///< GPIO output
	GPIO_MODE_IN,     	///< GPIO input
	GPIO_MODE_PWM,    	///< PWM output
	GPIO_MODE_DISABLE,	///< input and output disable
	GPIO_MODE_IN_PINT,	///< GPIO input(Active High interrupt)
	GPIO_MODE_IN_NINT,	///< GPIO input(Active Low interrupt)
	GPIO_MODE_DELIMITER ///< Delimiter
} eGPIO_MODE;

/**
 * @brief	Callback function of GPIO interrupt.
 *
 * @param [in] args	parameter of gpio_init()
 * @return void
 *
 * @note Only GPIO03 LEVEL supports GPIO interruption.
 */
typedef void ( *gpio_callback )( void* args );

/**
 * @brief Initialize GPIO.
 *
 * @param [in] fp	Pointer to callback function called when GPIO interrupt is occurred.
 * @param [in] args	Argument of callback function.
 * @return void
 *
 * @note Only GPIO03 LEVEL supports GPIO interruption.
 */
void gpio_init( gpio_callback fp, void* args );

/**
 * @brief	Interrupt settings.
 *
 * @param [in] no	GPIO number(GPIO0 - GPIO3)
 * @param [in] mode	GPIO mode
 * @return void
 *
 */
void gpio_set_mode( eGPIO_NO no, eGPIO_MODE mode );

/**
 * @brief GPIO Iterrupt Setting.
 *
 * @param [in] f_int	0:Disabling intterupt, Others:Enabling intterupt
 * @return void
 *
 * @note Only GPIO03 supports GPIO interruption.
 */
void gpio_set_interrupt( int f_int );

/**
 * @brief gpio output value setting
 *
 * @param [in] no	GPIO number(GPIO0 - GPIO3)
 * @return void
 *
 * @note Setting specified GPIO mode to output mode by gpio_set_mode() is necessary in advance.
 */
void gpio_toggle_data( eGPIO_NO no );
/**
 * @brief Set output value.
 *
 * @param [in] no	GPIO number(GPIO0 - GPIO3)
 * @param [in] bit	0:Negate output<BR>
 *					1:Assert output
 * @return void
 *
 * @note Setting specified GPIO mode to output mode by gpio_set_mode() is necessary in advance.
 */
void gpio_set_data( eGPIO_NO no, int bit );

/**
 * @brief Get output value.
 *
 * @param [in] no	GPIO number(GPIO0 - GPIO3)
 * @return Input level. 0:Low, 1:High
 *
 * @note Setting specified GPIO mode to input mode by gpio_set_mode() is necessary in advance.
 */
int gpio_get_data( eGPIO_NO no );

/**
 * @brief Set PWM output frequency. When set 0 as PWM output frequency, PWM stops.
 *
 * @param [in] core_freq	frizz core clock frequency<BR>
 *							When use external OSC as frizz core clock, set the frequency of the external OSC.<BR>
 *							When use internal OSC as frizz core clock, set 40MHz.
 * @param [in] pwm_freq		PWM output frequency
 * @return void
 *
 * @note Setting specified GPIO mode to PWM mode by gpio_set_mode() is necessary in advance.
 */
void gpio_set_pwm0_freq( int core_freq, int pwm_freq );

/**
 * @brief Set PWM output frequency. When set 0 as PWM output frequency, PWM stops.
 *
 * @param [in] core_freq	frizz core clock frequency<BR>
 *							When use external OSC as frizz core clock, set the frequency of the external OSC.<BR>
 *							When use internal OSC as frizz core clock, set 40MHz.
 * @param [in] pwm_freq		PWM output frequency
 * @return void
 *
 * @note Setting specified GPIO mode to PWM mode by gpio_set_mode() is necessary in advance.
 */
void gpio_set_pwm1_freq( int core_freq, int pwm_freq );

/**
 * @brief	Set the number of count PWM outputs at 1 cycle.<BR>
 *			Clock divided by PWM output frequency specified by gpio_set_pwm0_freq() is used for count. <BR>
 *			The number of count at 1 cycle = 2^(1 + pulse_count)
 *
 * @param [in] pulse_count The number of count PWM outputs at 1 cycle
 * @return void
 */
void gpio_set_pwm0_pulse_count( int pulse_count );

/**
 * @brief	Set the number of count PWM outputs at 1 cycle.<BR>
 *			Clock divided by PWM output frequency specified by gpio_set_pwm1_freq() is used for count. <BR>
 *			The number of count at 1 cycle = 2^(1 + pulse_count)
 *
 * @param [in] pulse_count The number of count PWM outputs at 1 cycle
 * @return void
 */
void gpio_set_pwm1_pulse_count( int pulse_count );

/**
 * @brief	Set the number of count of high period PWM outputs at 1 cycle.<BR>
 *			Clock divided by PWM output frequency specified by gpio_set_pwm0_freq() is used for count. <BR>
 *			The number of count of high period must be shorter than the count set by gpio_set_pwm0_pulse_count().
 * @param [in] high The number of count of high period PWM outputs at 1 cycle.
 * @return void
 *
 * @note Please correctly set GPIO mode by gpio_set_mode(), before call this function.
 */
void gpio_set_pwm0_duty( int high );

/**
 * @brief	Set the number of count of high period PWM outputs at 1 cycle.<BR>
 *			Clock divided by PWM output frequency specified by gpio_set_pwm1_freq() is used for count. <BR>
 *			The number of count of high period must be shorter than the count set by gpio_set_pwm1_pulse_count().
 * @param [in] high The number of count of high period PWM outputs at 1 cycle.
 * @return void
 *
 * @note Please correctly set GPIO mode by gpio_set_mode(), before call this function.
 */
void gpio_set_pwm1_duty( int high );

#ifdef __cplusplus
}
#endif //__cplusplus

/* @} */
#endif // #ifndef __GPIO_H__
