/*!******************************************************************************
 * @file command.h
 * @brief format for command to FrizzFW
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __COMMAND_H__
#define __COMMAND_H__

#if! defined(RUN_ON_PC)
#include <xtensa/tie/frizz_simd4w_que.h>
#define FLOAT_TYPE	frizz_tie_fp
#else
#define FLOAT_TYPE	float
#endif

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Header for Command
 */
typedef union {
	union {
		unsigned int	w;
	};
	struct {
		unsigned short	imm;		///< immediately value
		unsigned char	param_num;	///< num of parameters
		unsigned char	cmd_id;		///< command ID
	};
} FRIZZ_COMMAND_HEADER;

/**
 * @brief break code
 * @note command status clear
 */
#define FRIZZ_COMMAND_BREAK_CODE	0xFF00FF80

/**
 * @brief uint x1
 */
typedef union {
	struct {
		unsigned int	w[1];
	};
	struct {
		unsigned int	val[1];
	};
} FRIZZ_COMMAND_FORM_UINT1;

/**
 * @brief float x1
 */
typedef union {
	struct {
		unsigned int	w[1];
	};
	struct {
		FLOAT_TYPE		val[1];
	};
} FRIZZ_COMMAND_FORM_FLOAT1;

/**
 * @brief float x2
 */
typedef union {
	struct {
		unsigned int	w[2];
	};
	struct {
		FLOAT_TYPE		val[2];
	};
} FRIZZ_COMMAND_FORM_FLOAT2;

/**
 * @brief float x3
 */
typedef union {
	struct {
		unsigned int	w[3];
	};
	struct {
		FLOAT_TYPE		val[3];
	};
} FRIZZ_COMMAND_FORM_FLOAT3;

/**
 * @brief float x9
 */
typedef union {
	struct {
		unsigned int	w[9];
	};
	struct {
		FLOAT_TYPE		val[9];
	};
} FRIZZ_COMMAND_FORM_FLOAT9;

typedef union {
	unsigned int				w[1];
	FRIZZ_COMMAND_FORM_UINT1	uint1;	///< uint x1
	FRIZZ_COMMAND_FORM_FLOAT1	float1;	///< float x1
	FRIZZ_COMMAND_FORM_FLOAT2	float2;	///< float x2
	FRIZZ_COMMAND_FORM_FLOAT3	float3;	///< float x3
	FRIZZ_COMMAND_FORM_FLOAT9	float9;	///< float x9
} FRIZZ_COMMAND_FORM;

#define FRIZZ_COMMAND_INIT_SENSOR	(1 << 0)
#define FRIZZ_COMMAND_INIT_PDR		(1 << 1)
#define FRIZZ_COMMAND_INIT_HSE		(1 << 2)

/**
 * @brief Command ID
 */
typedef enum {

	/**
	 * @brief getting FW version
	 *
	 * useformat: none
	 * @return Firmware Version
	 */
	FRIZZ_COMMAND_ID_GET_FW_VERSION = 0,

	/**
	 * @brief FW initialize
	 *
	 * useformat: none
	 * @param imm initialize flag
	 * @return initialize flag
	 */
	FRIZZ_COMMAND_ID_INIT,

	/**
	 * @brief FW end command
	 *
	 * useformat: none
	 * @return 0:OK
	 */
	FRIZZ_COMMAND_ID_END,

	/**
	 * @brief sensor update interval setting
	 *
	 * useformat: FRIZZ_COMMAND_FORM_UINT1
	 * @param imm target sensor
	 * @param val[0] update interval(msec)
	 * @return 0:OK
	 */
	FRIZZ_COMMAND_ID_SET_DELAY,

	/**
	 * @brief calibration start
	 *
	 * useformat: none
	 * @return 0
	 */
	FRIZZ_COMMAND_ID_CALIB_START,

	/**
	 * @brief calibration stop
	 *
	 * useformat: none
	 * @return 0
	 */
	FRIZZ_COMMAND_ID_CALIB_STOP,

	/**
	 * @brief Hard Iron value set
	 *
	 * useformat: #FRIZZ_COMMAND_FORM_FLOAT3
	 * @param val[] hard-iron parameter
	 * @return 0:OK
	 */
	FRIZZ_COMMAND_ID_CALIB_SET_HARDIRON_PARAM,

	/**
	 * @brief Soft Iron value set
	 *
	 * useformat: #FRIZZ_COMMAND_FORM_FLOAT9
	 * @param val[] soft-iron parameter
	 * @return 0:OK
	 */
	FRIZZ_COMMAND_ID_CALIB_SET_SOFTIRON_PARAM,

	/**
	 * @brief PDR calculate start
	 *
	 * useformat: none
	 * @return 0
	 */
	FRIZZ_COMMAND_ID_PDR_START,

	/**
	 * @brief PDR calculate stop
	 *
	 * useformat: none
	 * @return 0
	 */
	FRIZZ_COMMAND_ID_PDR_STOP,

	/**
	 * @brief PDR walk parameter set
	 *
	 * useformat: #FRIZZ_COMMAND_FORM_FLOAT2
	 * @param val[0] walk A(slope)
	 * @param val[1] walk B(intercept)
	 * @return 0:OK
	 */
	FRIZZ_COMMAND_ID_PDR_SET_WALK_PARAM,

	/**
	 * @brief PDR fix stride setting
	 *
	 * useformat: #FRIZZ_COMMAND_FORM_FLOAT1
	 * @param val[0] fix stride(m)
	 * @return 0:OK
	 */
	FRIZZ_COMMAND_ID_PDR_SET_FIX_STRIDE,

	/**
	 * @brief PDR stride mode setting
	 *
	 * useformat: none
	 * @param imm stride mode #PDR_STRIDE_MODE
	 * @return stride mode
	 */
	FRIZZ_COMMAND_ID_PDR_SET_STRIDE_MODE,

	/**
	 * @brief PDR hold mode setting
	 *
	 * useformat: none
	 * @param imm hold mode #PDR_HOLD_MODE
	 * @return hold mode
	 */
	FRIZZ_COMMAND_ID_PDR_SET_HOLD_MODE,

	/**
	 * @brief PDR walk step clear
	 *
	 * useformat: none
	 * @return 0
	 */
	FRIZZ_COMMAND_ID_PDR_CLEAR_STEP_CNT,

	/**
	 * @brief relative position correction
	 *
	 * useformat: #FRIZZ_COMMAND_FORM_FLOAT2
	 * @param val[0] x-axis offset(m)
	 * @param val[1] y-axis offset(m)
	 * @return 0:OK
	 */
	FRIZZ_COMMAND_ID_PDR_SET_POS_OFST,

	/**
	 * @brief traveling direction correction
	 *
	 * useformat: #FRIZZ_COMMAND_FORM_FLOAT1
	 * @param val[0] traveling direction angle offset(rad)
	 * @return 0:OK
	 */
	FRIZZ_COMMAND_ID_PDR_SET_YAW_OFST,

	/**
	 * @brief geo magnet information setting
	 *
	 * useformat: #FRIZZ_COMMAND_FORM_FLOAT3
	 * @param val[0] magnetic force
	 * @param val[1] inclination
	 * @param val[2] deviation
	 * @return 0:OK
	 */
	FRIZZ_COMMAND_ID_PDR_SET_GEOMAG_PARAM,

	/**
	 * @brief forcibly command break
	 *
	 * useformat: none #FRIZZ_COMMAND_BREAK_CODE
	 * @return 0
	 */
	FRIZZ_COMMAND_ID_BREAK = 0xFF,
} FRIZZ_COMMAND_ID;

#ifdef __cplusplus
}
#endif

#endif//__COMMAND_H__
