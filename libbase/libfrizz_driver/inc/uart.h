/*!******************************************************************************
 * @file uart.h
 * @brief header for uart driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __UART_H__
#define __UART_H__
/** @addtogroup UART
 */
/** @ingroup UART */
/* @{ */

#ifdef __cplusplus
extern "C" {
#endif
//#define UART_DEBUG // uart debug mode

/**
 * @name UART state flag
 * @{ */
#define UART_LS	unsigned int			///< UART STATE
#define UART_LS_CTS				0x80	///< CTS state
#define UART_LS_TX_NONE			0x40	///< without send data state
#define UART_LS_TX_FIFO_EMPTY	0x20	///< transmission FIFO Empty
#define UART_LS_BREAK			0x10	///< break detection
#define UART_LS_E_FRAMING		0x08	///< framing error
#define UART_LS_E_PARITY		0x04	///< parity error
#define UART_LS_E_OVER_RUN		0x02	///< Rx FIFO overrun
#define UART_LS_RX				0x01	///< receive data detection
#define UART_LS_ERROR			(UART_LS_E_FRAMING | UART_LS_E_PARITY | UART_LS_E_OVER_RUN)	///< error state
/**  @} */

/**
 * @brief communication complete callback
 *
 * @param [in]	void*	args
 * @param [in]	UART_LS	state flag
 * @return void
 */
typedef void ( *uart_callback )( void* arg, UART_LS ls );

/** @enum eUART_Parity
 *  @brief Parity setting
 */
typedef enum {
	UART_ParityOdd = 0,	///< Parity ODD
	UART_ParityEven,	///< Parity EVEN
	UART_ParityNone,	///< Parity NONE
} eUART_Parity;

/** @enum eUART_StopBit
 *  @brief Stop Bit setting
 */
typedef enum {
	UART_StopBit1 = 0,	///< 1bit
	UART_StopBit2,		///< 2bit
} eUART_StopBit;

/** @enum eUART_DataBit
 *  @brief Data bit num
 */
typedef enum {
	UART_DataBit5 = 0,	///< 5bit
	UART_DataBit6,		///< 6bit
	UART_DataBit7,		///< 7bit
	UART_DataBit8,		///< 8bit
} eUART_DataBit;

/** @enum eUART_FlowControl
 *  @brief Flow Control
 */
typedef enum {
	UART_FlowControlNone = 0,	///< No flow control
	UART_FlowControlHardware,	///< Hardware flow Control
} eUART_FlowControl;

/** @struct sUART_Setting
 *  @brief UART Setting
 */
typedef struct {
	unsigned int		baudrate;	///< baudrate setting with enumeration QUART_BAUDRATE_MODE<BR>
	///< Though Baudrate can be also set directly, the rate may not be able to be the target rate due to division resolution.<BR>
	///< For the detail, please refer to "frizz Users Manual"
	unsigned int		fifo_num;	///< Threshold of number of receive data in HW flow contro(1 - 128 bytes)
	eUART_Parity		parity;		///< Parity setting with enumeration eUART_Parity
	eUART_StopBit		stop;		///< Stop Bit setting with enumeration eUART_StopBit
	eUART_DataBit		data;		///< Number of data bit setting with eUART_DataBit
	eUART_FlowControl	flow;		///< FlowControl setting with enumeration eUART_FlowControl
} sUART_Setting;

/**
 * @brief Enable UART block and initialize UART driver.
 *
 * @param [in]	freq	frizz core clock frequency<BR>
 *                      When use external OSC as frizz core clock, set the frequency of the external OSC.<BR>
 *                      When use internal OSC as frizz core clock, set 40MHz.
 * @param [in]	set		Setting value
 * @param [in]	pf		Pointer to callback function called when UART transmission is completed.<BR>
 *                      When calling callback function isn't necessary, set 0(NULL).
 * @param [in]	arg		Argument of callback function<BR>
 * @return The return value is 0.
 */
int uart_init( unsigned int freq, sUART_Setting* set, uart_callback pf, void* arg );

/**
 * @brief Reset UART with the setting specified when uart_init() is called .
 *
 * @retval 0		Success
 * @retval Others	Failed
 */
int uart_reset( void );

/**
 * @brief Get the number of received data in receive FIFO and the value of UART Line Status Register.
 *
 * @param [out]	rx_num	Number of received data in receive FIFO
 * @return The value of UART Line Status Register.<BR>
 *         For the detail of the register, refer to "frizz Users Manual"
 */
UART_LS uart_get_status( unsigned int* rx_num );

/**
 * @brief	Set data to send to UART transmission FIFO.<BR>
 *			If there isn't enough free space on transmission FIFO,<BR>
 *			set data only free space size.<BR>
 *
 * @param [in]	data	Send data
 * @param [in]	size	Send data size.<BR>
 *                      Upper limit of size is transmission FIFO(128 byte) size.
 * @return Number of data set
 */
int uart_snd_data( unsigned char *data, unsigned int size );

/**
 * @brief	Get data from UART receive FIFO with specified size and store the data to specified buffer.<BR>
 *			This function is blocked until reading from UART receive FIFO with specified size is completed.<BR>
 *			The number of data ready to read can be checked by uart_get_status().
 *
 * @param [in]	data	Pointer to buffer stores received data
 * @param [in]	size	Data size to get
 *
 * @return Size of received data buffer
 */
int uart_get_data( unsigned char *data, unsigned int size );

/**
 * @brief Disable UART interrupt
 *
 * @return void
 */
void uart_lock( void );

/**
 * @brief Enable UART interrupt.
 *
 * @return void
 */
void uart_unlock( void );

#ifdef __cplusplus
}
#endif //__cplusplus

/* @} */
#endif // #ifndef __UART_H__
