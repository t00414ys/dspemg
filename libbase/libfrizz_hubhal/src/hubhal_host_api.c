/*!******************************************************************************
 * @file hubhal_host_api.c
 * @brief source code for hubhal_host_api
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "hubhal_format.h"
#include "uart.h"
#include "hubhal_in.h"
#include "hubhal_out.h"
#include "hubhal_in_state.h"
#include "frizz_peri.h"
#include "frizz_env.h"
#include "hubhal_out_txt.h"
#include "frizz_env.h"

#if defined(USE_HOST_API_UART_IN)
extern hubhal_in_info_t g_hubhal_in;

/**
 * @brief uart receive inteface
 *
 * @param none
  */
void hubhal_host_uart_in()
{

	unsigned int size = 0;
	unsigned char in_data[4];
	unsigned int data;

	hubhal_in_info_t *obj = &g_hubhal_in;
	int f_ack = 0;

	while( uart_get_status( &size ) & UART_LS_RX ) {

		if( size >= 4 ) {
			uart_get_data( in_data, 4 );
			data = ( unsigned int )( ( ( ( in_data[0] ) & 0xFF ) << 24 ) | ( ( ( in_data[1] ) & 0xFF ) << 16 ) | ( ( ( in_data[2] ) & 0xFF ) << 8 ) | ( ( in_data[3] ) & 0xFF ) );
		} else {
			continue;
		}

		if( data == HUBHAL_FORMAT_BREAK_CODE ) {
			obj->head.w = data;
			obj->num = 0;
			obj->state = HUBHAL_IN_STATE_NONE;
			f_ack = 1;
		} else if( obj->state == HUBHAL_IN_STATE_READING ) {
			obj->data[obj->num] = data;
			obj->num++;
			if( obj->head.num == obj->num ) {
				obj->state = HUBHAL_IN_STATE_CMD;
			}
			f_ack = 1;
		} else if( obj->state == HUBHAL_IN_STATE_NONE ) {
			obj->head.w = data;
			obj->num = 0;
			if( obj->head.prefix == 0xFF && obj->head.type == 0x81 ) {
				if( 0 < obj->head.num ) {
					obj->state = HUBHAL_IN_STATE_READING;
					f_ack = 1;
				}
			}
		}
		hubhal_out_txt_ack( f_ack, data );
		if( f_ack == 0 ) {
			obj->state = HUBHAL_IN_STATE_NONE;
		}
	}
}
#endif
