/*!******************************************************************************
 * @file hubhal_block.h
 * @brief source code for hubhal_block.c
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __HUBHAL_BLOCK_H__
#define __HUBHAL_BLOCK_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief management for information of blocking
 */
typedef struct {
	volatile unsigned int			ts;			///< current timestamp
	volatile unsigned int			sys_tick;	///< current tick
} hubhal_block_info_t;


/**
 * @brief Init
 *
 * @param [in] freq using clock [Hz]
 */
void hubhal_block_init( unsigned int freq );

/**
 * @brief get timestamp
 *
 * @return current timestamp[msec]
 */
unsigned int hubhal_block_get_ts( void );

/**
 * @brief blocking for target timestamp or interrupt
 *
 * @param [in] target_ts target timestamp[msec]
 * @param [in] target_output_num target output_num[word]
 *
 * @return current timestamp[msec]
 */
unsigned int hubhal_block_exec( unsigned int target_ts, unsigned int target_output_num );

/**
 * @brief blocking for target timestamp or interrupt
 *
 * @param [in] power mode
 * @param [in] gpio number
 *
 * @return int status
 */
int hubhal_set_power_mode( unsigned int power_mode, unsigned int gpio_num );

/**
 * @brief use mode flag
 */
typedef struct {
	unsigned int	power_mode_flag;
	unsigned int	gpio_no;
	unsigned int	gpio_status;
} use_mode_t;

extern use_mode_t use_mode;

/**
 * @brief get frizz time
 *
 * @param [in] sec_cnt 	sec pointer
 * @param [in] nsec_cnt nano sec pointer
 *
 * @return none
 */
void check_frizz_times( unsigned long *sec_cnt, unsigned long *nsec_cnt );



#ifdef __cplusplus
}
#endif

#endif//__HUBHAL_BLOCK_H__
