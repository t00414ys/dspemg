/*!******************************************************************************
 * @file hubhal_host_api.h
 * @brief source code for hubhal_host_api.h
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __HUBHAL_HOST_API_H__
#define __HUBHAL_HOST_API_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief uart receive interface
 *
 * @param none
  */
void hubhal_host_uart_in();

#ifdef __cplusplus
}
#endif

#endif//__HUBHAL_HOST_API_H__
