/*!******************************************************************************
 * @file    ssd1306_api.h
 * @brief   ssd1306 sensor api header
 * @par     copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __SSD1306_API_H__
#define __SSD1306_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int ssd1306_init( unsigned int param );
void ssd1306_ctrl( int f_ena );
unsigned int ssd1306_rcv( unsigned int tick );
int ssd1306_conv( frizz_fp *data );
int ssd1306_setparam( void *ptr );
unsigned int ssd1306_get_ver( void );
unsigned int ssd1306_get_name( void );
int ssd1306_get_condition( void *data );

#ifdef __cplusplus
}
#endif

#endif // __SSD1306_H__

