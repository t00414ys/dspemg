/*!******************************************************************************
 * @file    mpl115a2_api.h
 * @brief   mpl115a2 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MPL115A2_API_H__
#define __MPL115A2_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int mpl115a2_init( unsigned int param );
void mpl115a2_ctrl( int f_ena );
unsigned int mpl115a2_rcv( unsigned int tick );	// you can call it after 3msec later from mpl115a2_cmd().
int mpl115a2_conv( frizz_fp *data );

int mpl115a2_setparam( void *ptr );
unsigned int mpl115a2_get_ver( void );
unsigned int mpl115a2_get_name( void );
int mpl115a2_get_condition( void *data );

#ifdef __cplusplus
}
#endif


#endif
