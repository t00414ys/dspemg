/*!******************************************************************************
 * @file    ads1191.h
 * @brief   ads1191 sensor driver header
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 * @par     Data Sheet
 *          ADXL362 3-Axis Accelerometer Data Sheet APS-048-0029v1.7
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ADXL362_H__
#define __ADXL362_H__
#include "ads1191_api.h"

/*
 *
 *		By environment, you must edit this section.
 *
 */

/* GPIO number to be assigned to the chip select signal */
#define ADS1191_SPI_CS_NO			(GPIO_NO_0)
//#define ADS1191_SPI_FREQ			(20000000) // 1/2
//#define ADS1191_SPI_FREQ			(10000000) // 1/4
//#define ADS1191_SPI_FREQ			( 8000000) // 1/5
//#define ADS1191_SPI_FREQ			( 5000000) // 1/8
//#define ADS1191_SPI_FREQ			( 4000000) // 1/10
//#define ADS1191_SPI_FREQ			( 2000000) // 1/20
//#define ADS1191_SPI_FREQ			( 1600000) // 1/25
#define ADS1191_SPI_FREQ			( 1000000) // 1/40
//#define ADS1191_SPI_FREQ			(  800000) // 1/50
//#define ADS1191_SPI_FREQ			(  500000) // 1/80




#if 1
/* Register setting */

/* Device Settings (Read-Only Registers) */
/* 						アドレス　 // リセット値*/
#define ADS1191_ID			(0x00) // (0x50)	[0101 0000]	ads1191
#define ADS1191_CONFIG1		(0x01) // (0x06)	[0000 0110]	8kSPS
#define ADS1191_CONFIG2		(0x02) // (0xA0)	[1010 0000]	Enable internal reference buffer
#define ADS1191_LOFF		(0x03) // (0x10)	[0001 0000]	RLF_LOFF sense is disabled

/* Global Settings Across Channels */
#define ADS1191_CH1SET		(0x04) // (0x00)	[0000 0000]	Normal operation, Gain 6, Normal electrode input
#define ADS1191_CH2SET		(0x05) // (0x00)	[0000 0000]	Normal operation, Gain 6, Normal electrode input
#define ADS1191_RLD_SENS	(0x06) // (0x20)	[0010 0000]	RLD buffer is enabled
#define ADS1191_LOFF_SENS	(0x07) // (0x00)	[0000 0000]
#define ADS1191_LOFF_STAT	(0x08) // (0x00)	[000- ----]

/* GPIO and OTHER Registers */
#define ADS1191_MISC1		(0x09) // (0x02)	[0000 0010]
#define ADS1191_MISC2		(0x0A) // (0x02)	[0000 0010]
#define ADS1191_GPIO		(0x0B) // (0x0F)	[0000 1111]


/* SPI Command Definitions (SPI コマンド定義 */

/* System Commands */
#define ADS1191_WAKEUP		(0x02)		// Wake-up from standby mode
#define ADS1191_STANDBY		(0x04)		// Enter standby mode
#define ADS1191_RESET		(0x06)		// Reset the device
#define ADS1191_START		(0x08)		// Start/restart (synchronize) conversions
#define ADS1191_STOP		(0x0A)		// Stop conversion
#define ADS1191_OFFSETCAL	(0x1A)		// Channel offset calibration

/* Data Read Commands */
#define ADS1191_RDATAC		(0x10)		// Enable Read Data Continuous mode. This mode is the default mode at power-up.
#define ADS1191_SDATAC		(0x11)		// Stop Read Data Continuously mode
#define ADS1191_RDATA		(0x12)		// Read data by command; supports multiple read back.

/* Register Read Commands */

#define ADS1191_RREG       (0x20)
#define ADS1191_WREG       (0x40)

#endif

/* Gain Setting */
#define ADS1191_CH1SET_GAIN6		(0x00) // default	[0000 0000] Gain 6
#define ADS1191_CH1SET_GAIN1		(0x10) // 			[0001 0000] Gain 1
#define ADS1191_CH1SET_GAIN2		(0x20) // 			[0010 0000] Gain 2
#define ADS1191_CH1SET_GAIN3		(0x30) // 			[0011 0000] Gain 3
#define ADS1191_CH1SET_GAIN4		(0x40) // 			[0100 0000] Gain 4
#define ADS1191_CH1SET_GAIN8		(0x50) // 			[0101 0000] Gain 8
#define ADS1191_CH1SET_GAIN12		(0x60) // 			[0110 0000] Gain 12



#endif
