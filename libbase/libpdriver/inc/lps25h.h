/*!******************************************************************************
 * @file    lps25h.h
 * @brief   lps25h sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __LPS25H_H__
#define __LPS25H_H__
#include "lps25h_api.h"


// LPS25H is fully compatible with LPS25HB
/*
 *
 *		The rest of this is okay not change
 *
 */
// Definition of pin:SPI_SDO
#define PRES_lps25h_I2C_ADDRESS_H    0x5D		// high(pin:SDO)
#define PRES_lps25h_I2C_ADDRESS_L    0x5C		// low(pin:SDO)

#define LPS25H_DEVICE_ID    0xBD

#define BIT(x) ( (x) )

#define MEMS_SET                                        0x01
#define MEMS_RESET                                      0x00

//Register Definition
#define LPS25H_WHO_AM_I				0x0F  // device identification register

#define LPS25H_RES_CFG_REG                          0x10
#define LPS25H_RES_AVGT                         BIT(2)
#define LPS25H_RES_AVGP                         BIT(0)

// frequency
#define	LPS25H_FREQ_ONESHOT		0
#define	LPS25H_FREQ_1HZ			1
#define	LPS25H_FREQ_7HZ			2
#define	LPS25H_FREQ_12_5HZ		3
#define	LPS25H_FREQ_25HZ		4

// CONTROL REGISTER 1
#define LPS25H_CTRL_REG1			0x20
#define LPS25H_POWER_DOWN			BIT(7)
#define LPS25H_ODR_BIT				BIT(4)
#define LPS25H_INT_ENABLE			BIT(3)
#define LPS25H_BDU				BIT(2)
#define LPS25H_RESET_AZ				BIT(1)
#define LPS25H_SIM				BIT(0)

// CONTROL REGISTER 2
#define LPS25H_CTRL_REG2			0x21
#define LPS25H_BOOT				BIT(7)
#define LPS25H_FIFO_EN				BIT(6)
#define LPS25H_FIFO_MEAN_DEC		        BIT(4)
#define LPS25H_SW_RESET				BIT(2)
#define LPS25H_AUTO_ZERO			BIT(1)
#define LPS25H_ONE_SHOT				BIT(0)

// CONTROL REGISTER 3
#define LPS25H_CTRL_REG3			0x22
#define LPS25H_INT_H_L				BIT(7)
#define LPS25H_PP_OD				BIT(6)
#define LPS25H_INT_CFG				BIT(0)

// CONTROL REGISTER 4
#define LPS25H_CTRL_REG4			0x23
#define LPS25H_INT1_P_EMPTY			BIT(3)
#define LPS25H_INT1_P_WTM			BIT(2)
#define LPS25H_INT1_P_OVERRUN		        BIT(1)
#define LPS25H_INT1_P_DRDY			BIT(0)


//INTERRUPT CONFIG 5
#define LPS25H_INT_CFG_REG			0x24
#define LPS25H_INT_CFG_REG_LIR     		BIT(6)
#define LPS25H_INT_CFG_REG_PLE			BIT(3)
#define LPS25H_INT_CFG_REG_PHE			BIT(1)


//STATUS_REG
#define LPS25H_STATUS_REG			      0x27
#define LPS25H_P_OR                                   BIT(5)
#define LPS25H_T_OR                                   BIT(4)
#define LPS25H_P_DA                                   BIT(1)
#define LPS25H_T_DA                                   BIT(0)


//FIFO CONTROL REGISTER
#define LPS25H_FIFO_CTRL_REG                           0x2E
#define LPS25H_FM                                      BIT(5)
#define LPS25H_FTH                                     BIT(0)

//FIFO STATUS
#define LPS25H_FIFO_SRC_REG				0x2f
#define LPS25H_FIFO_WTM_STATUS				BIT(7)
#define LPS25H_FIFO_FULL				BIT(6)
#define LPS25H_FIFO_EMPTY				BIT(5)
#define LPS25H_FIFO_DIFF_POINT				BIT(0)


//FIFO Source Register bit Mask
#define LPS25H_FIFO_SRC_WTM                            0x80
#define LPS25H_FIFO_SRC_FULL                           0x40
#define LPS25H_FIFO_SRC_EMPTY                          0x20


//OUTPUT REGISTER
#define LPS25H_PRESS_OUT_XL					0x28
#define LPS25H_PRESS_OUT_L					0x29
#define LPS25H_PRESS_OUT_H					0x2a
#define LPS25H_TEMP_OUT_L					0X2b
#define LPS25H_TEMP_OUT_H					0X2c


//STATUS REGISTER bit mask
#define LPS25H_STATUS_REG_P_OR                        	0x20
#define LPS25H_STATUS_REG_T_OR                          0x10
#define LPS25H_STATUS_REG_P_DA                          0x02
#define LPS25H_STATUS_REG_T_DA                          0x01


#define LPS25H_MEMS_I2C_ADDRESS			        0x33


//It must be defined when you want to read multiple bytes incrementing the register address
#define LPS25H_I2C_BURSTREAD	        0x80

#endif // __LPS25H_H__

