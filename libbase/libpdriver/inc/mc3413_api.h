/*!******************************************************************************
 * @file    mc3413_api.h
 * @brief   mc3413 sensor api header
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MC3413_API_H__
#define __MC3413_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int mc3413_init( unsigned int param );
void mc3413_ctrl_accl( int f_ena );
unsigned int mc3413_rcv_accl( unsigned int tick );
int mc3413_conv_accl( frizz_fp data[3] );

int mc3413_setparam_accl( void *ptr );
unsigned int mc3413_get_ver( void );
unsigned int mc3413_get_name( void );

int mc3413_get_condition( void *data );
int mc3413_get_raw_data( void *data );

#ifdef __cplusplus
}
#endif


#endif

