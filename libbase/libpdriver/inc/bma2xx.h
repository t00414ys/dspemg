/*!******************************************************************************
 * @file    bma2xx.h
 * @brief   bma2xx sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __BMA2XX_H__
#define __BMA2XX_H__
#include "bma2xx_api.h"

/*
 *
 *		By environment, you must edit this file
 *
 */


/*
 * This is use accel sensor range (To select either one)
 *(DEFINITED) : Enable  use accl sensor range
 *(~DEFINITED): Disable  use accl sensor range
 */
//#define		ACC_2G_RANGE
//#define		ACC_4G_RANGE
//#define		ACC_8G_RANGE
#define		ACC_16G_RANGE






#define CTRL_ACTIVATE					(1)
#define CTRL_DEACTIVATE					(0)

#define BMA2XX_I2C_ADDRESS_H			0x19
#define BMA2XX_I2C_ADDRESS_L			0x18

#define	BMA2XX_WHOAMI_ID_BMA250E		0xF9
#define	BMA2XX_WHOAMI_ID_BMA255			0xFA
#define	BMA2XX_WHOAMI_ID_BMA280			0xFB

/**************************************************************/
/**\name	REGISTER ADDRESS DEFINITIONS    */
/**************************************************************/
#define BMA2x2_CHIP_ID_REG                      0x00
#define BMA2x2_X_AXIS_LSB_REG                   0x02
#define BMA2x2_X_AXIS_MSB_REG                   0x03
#define BMA2x2_Y_AXIS_LSB_REG                   0x04
#define BMA2x2_Y_AXIS_MSB_REG                   0x05
#define BMA2x2_Z_AXIS_LSB_REG                   0x06
#define BMA2x2_Z_AXIS_MSB_REG                   0x07
#define BMA2x2_TEMP_REG							0x08
#define BMA2x2_STAT1_REG						0x09
#define BMA2x2_STAT2_REG						0x0A
#define BMA2x2_STAT_TAP_SLOPE_REG				0x0B
#define BMA2x2_STAT_ORIENT_HIGH_REG				0x0C
#define BMA2x2_STAT_FIFO_REG					0x0E
#define BMA2x2_RANGE_SELECT_REG                 0x0F
#define BMA2x2_BW_SELECT_REG                    0x10
#define BMA2x2_MODE_CTRL_REG                    0x11
#define BMA2x2_LOW_NOISE_CTRL_REG               0x12
#define BMA2x2_DATA_CTRL_REG                    0x13
#define BMA2x2_RST_REG                          0x14
#define BMA2x2_INTR_ENABLE1_REG                 0x16
#define BMA2x2_INTR_ENABLE2_REG                 0x17
#define BMA2x2_INTR_SLOW_NO_MOTION_REG          0x18
#define BMA2x2_INTR1_PAD_SELECT_REG             0x19
#define BMA2x2_INTR_DATA_SELECT_REG             0x1A
#define BMA2x2_INTR2_PAD_SELECT_REG             0x1B
#define BMA2x2_INTR_SOURCE_REG                  0x1E
#define BMA2x2_INTR_SET_REG                     0x20
#define BMA2x2_INTR_CTRL_REG                    0x21
#define BMA2x2_LOW_DURN_REG                     0x22
#define BMA2x2_LOW_THRES_REG                    0x23
#define BMA2x2_LOW_HIGH_HYST_REG                0x24
#define BMA2x2_HIGH_DURN_REG                    0x25
#define BMA2x2_HIGH_THRES_REG                   0x26
#define BMA2x2_SLOPE_DURN_REG                   0x27
#define BMA2x2_SLOPE_THRES_REG                  0x28
#define BMA2x2_SLOW_NO_MOTION_THRES_REG         0x29
#define BMA2x2_TAP_PARAM_REG                    0x2A
#define BMA2x2_TAP_THRES_REG                    0x2B
#define BMA2x2_ORIENT_PARAM_REG                 0x2C
#define BMA2x2_THETA_BLOCK_REG                  0x2D
#define BMA2x2_THETA_FLAT_REG                   0x2E
#define BMA2x2_FLAT_HOLD_TIME_REG               0x2F
#define BMA2x2_FIFO_WML_TRIG                    0x30
#define BMA2x2_SELFTEST_REG                     0x32
#define BMA2x2_EEPROM_CTRL_REG                  0x33
#define BMA2x2_SERIAL_CTRL_REG                  0x34
#define BMA2x2_OFFSET_CTRL_REG                  0x36
#define BMA2x2_OFFSET_PARAMS_REG                0x37
#define BMA2x2_OFFSET_X_AXIS_REG                0x38
#define BMA2x2_OFFSET_Y_AXIS_REG                0x39
#define BMA2x2_OFFSET_Z_AXIS_REG                0x3A
#define BMA2x2_GP0_REG                          0x3B
#define BMA2x2_GP1_REG                          0x3C
#define BMA2x2_FIFO_MODE_REG                    0x3E
#define BMA2x2_FIFO_DATA_OUTPUT_REG             0x3F

/**************************************************************/
/**\name	ACCEL RESOLUTION DEFINITION   */
/**************************************************************/
#define BMA2x2_12_RESOLUTION                    0
#define BMA2x2_10_RESOLUTION                    1
#define BMA2x2_14_RESOLUTION                    2

/****************************************************/
/**\name  RANGE AND BANDWIDTH SELECT     */
/***************************************************/
#define BMA2x2_RANGE_2G                 3
/**< sets range to +/- 2G mode */
#define BMA2x2_RANGE_4G                 5
/**< sets range to +/- 4G mode */
#define BMA2x2_RANGE_8G                 8
/**< sets range to +/- 8G mode */
#define BMA2x2_RANGE_16G                12
/**< sets range to +/- 16G mode */


#define BMA2x2_BW_7_81HZ        0x08
/**< sets bandwidth to LowPass 7.81HZ  */
#define BMA2x2_BW_15_63HZ       0x09
/**< sets bandwidth to LowPass 15.63HZ  */
#define BMA2x2_BW_31_25HZ       0x0A
/**< sets bandwidth to LowPass 31.25HZ  */
#define BMA2x2_BW_62_50HZ       0x0B
/**< sets bandwidth to LowPass 62.50HZ  */
#define BMA2x2_BW_125HZ         0x0C
/**< sets bandwidth to LowPass 125HZ  */
#define BMA2x2_BW_250HZ         0x0D
/**< sets bandwidth to LowPass 250HZ  */
#define BMA2x2_BW_500HZ         0x0E
/**< sets bandwidth to LowPass 500HZ  */
#define BMA2x2_BW_1000HZ        0x0F
/**< sets bandwidth to LowPass 1000HZ  */

/******************************************/
/**\name  SLEEP DURATION SELECT     */
/******************************************/
#define BMA2x2_SLEEP_DURN_0_5MS        0x05
/* sets sleep duration to 0.5 ms  */
#define BMA2x2_SLEEP_DURN_1MS          0x06
/* sets sleep duration to 1 ms */
#define BMA2x2_SLEEP_DURN_2MS          0x07
/* sets sleep duration to 2 ms */
#define BMA2x2_SLEEP_DURN_4MS          0x08
/* sets sleep duration to 4 ms */
#define BMA2x2_SLEEP_DURN_6MS          0x09
/* sets sleep duration to 6 ms*/
#define BMA2x2_SLEEP_DURN_10MS         0x0A
/* sets sleep duration to 10 ms */
#define BMA2x2_SLEEP_DURN_25MS         0x0B
/* sets sleep duration to 25 ms */
#define BMA2x2_SLEEP_DURN_50MS         0x0C
/* sets sleep duration to 50 ms */
#define BMA2x2_SLEEP_DURN_100MS        0x0D
/* sets sleep duration to 100 ms */
#define BMA2x2_SLEEP_DURN_500MS        0x0E
/* sets sleep duration to 500 ms */
#define BMA2x2_SLEEP_DURN_1S           0x0F
/* sets sleep duration to 1 s */


/******************************************/
/**\name  LATCH DURATION     */
/******************************************/
#define BMA2x2_LATCH_DURN_NON_LATCH    0x00
/* sets LATCH duration to NON LATCH  */
#define BMA2x2_LATCH_DURN_250MS        0x01
/* sets LATCH duration to 250 ms */
#define BMA2x2_LATCH_DURN_500MS        0x02
/* sets LATCH duration to 500 ms */
#define BMA2x2_LATCH_DURN_1S           0x03
/* sets LATCH duration to 1 s */
#define BMA2x2_LATCH_DURN_2S           0x04
/* sets LATCH duration to 2 s*/
#define BMA2x2_LATCH_DURN_4S           0x05
/* sets LATCH duration to 4 s */
#define BMA2x2_LATCH_DURN_8S           0x06
/* sets LATCH duration to 8 s */
#define BMA2x2_LATCH_DURN_LATCH        0x07
/* sets LATCH duration to LATCH */
#define BMA2x2_LATCH_DURN_NON_LATCH1   0x08
/* sets LATCH duration to NON LATCH1 */
#define BMA2x2_LATCH_DURN_250US        0x09
/* sets LATCH duration to 250 Us */
#define BMA2x2_LATCH_DURN_500US        0x0A
/* sets LATCH duration to 500 Us */
#define BMA2x2_LATCH_DURN_1MS          0x0B
/* sets LATCH duration to 1 Ms */
#define BMA2x2_LATCH_DURN_12_5MS       0x0C
/* sets LATCH duration to 12.5 Ms */
#define BMA2x2_LATCH_DURN_25MS         0x0D
/* sets LATCH duration to 25 Ms */
#define BMA2x2_LATCH_DURN_50MS         0x0E
/* sets LATCH duration to 50 Ms */
#define BMA2x2_LATCH_DURN_LATCH1       0x0F
/* sets LATCH duration to LATCH*/

/******************************************/
/**\name  MODE SETTINGS     */
/******************************************/
#define BMA2x2_MODE_NORMAL             0
#define BMA2x2_MODE_LOWPOWER1          1
#define BMA2x2_MODE_SUSPEND            2
#define BMA2x2_MODE_DEEP_SUSPEND       3
#define BMA2x2_MODE_LOWPOWER2          4
#define BMA2x2_MODE_STANDBY            5

/*  constant*/
#if defined(ACC_2G_RANGE)
#define BMA2XX_ACC_RANGE			BMA2x2_RANGE_2G
#define BMA2XX_ACCEL_PER_LSB_BMA250E		((float)(1./256.))
#define BMA2XX_ACCEL_PER_LSB_BMA255			((float)(1./1024.))
#define BMA2XX_ACCEL_PER_LSB_BMA280			((float)(1./4096.))
#endif

#if defined(ACC_4G_RANGE)
#define BMA2XX_ACC_RANGE			BMA2x2_RANGE_4G
#define BMA2XX_ACCEL_PER_LSB_BMA250E		((float)(1./128.))
#define BMA2XX_ACCEL_PER_LSB_BMA255			((float)(1./512.))
#define BMA2XX_ACCEL_PER_LSB_BMA280			((float)(1./2048.))
#endif

#if defined(ACC_8G_RANGE)
#define BMA2XX_ACC_RANGE			BMA2x2_RANGE_8G
#define BMA2XX_ACCEL_PER_LSB_BMA250E		((float)(1./64.))
#define BMA2XX_ACCEL_PER_LSB_BMA255			((float)(1./256.))
#define BMA2XX_ACCEL_PER_LSB_BMA280			((float)(1./1024.))
#endif

#if defined(ACC_16G_RANGE)
#define BMA2XX_ACC_RANGE			BMA2x2_RANGE_16G
#define BMA2XX_ACCEL_PER_LSB_BMA250E		((float)(1./32.))
#define BMA2XX_ACCEL_PER_LSB_BMA255			((float)(1./128.))
#define BMA2XX_ACCEL_PER_LSB_BMA280			((float)(1./512.))
#endif

#endif
