/*!******************************************************************************
 * @file    mpl115a2.h
 * @brief   mpl115a2 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MPL115A2_H__
#define __MPL115A2_H__
#include "mpl115a2_api.h"




/*
 *
 *		The rest of this is okay not change
 *
 */

// Constant
// Guass = 10^2*uT
// 0.15 (uT/LSB) => 0.0015 (Gauss/LSB)
#define MPL115A2_COEFF_MUL		((float)(0.6353861192570869990224828934506f))
#define MPL115A2_COEFF_ADD		((float)(500.f))

#define MPL115A2_SCALE_A0		((float)(0.125f))
#define MPL115A2_SCALE_B1		((float)(0.0001220703125f))
#define MPL115A2_SCALE_B2		((float)(0.00006103515625f))
#define MPL115A2_SCALE_C12		((float)(0.0000002384185791015625f))

#define MPL115A2_I2C_ADDRESS			0x60

#define MPL115A2_P_MSB					0x00
#define MPL115A2_P_LSB					0x01
#define MPL115A2_T_MSB					0x02
#define MPL115A2_T_LSB					0x03

#define MPL115A2_A0_MSB					0x04
#define MPL115A2_A0_LSB					0x05
#define MPL115A2_B1_MSB					0x06
#define MPL115A2_B1_LSB					0x07
#define MPL115A2_B2_MSB					0x08
#define MPL115A2_B2_LSB					0x09
#define MPL115A2_C12_MSB				0x0A
#define MPL115A2_C12_LSB				0x0B

#define MPL115A2_CONVERT				0x12

#endif
