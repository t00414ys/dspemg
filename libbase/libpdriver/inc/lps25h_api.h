/*!******************************************************************************
 * @file    zpa2326_api.h
 * @brief   zpa2326 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __LPS25H_API_H__
#define __LPS25H_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int lps25h_init( unsigned int param );
void lps25h_ctrl( int f_ena );
unsigned int lps25h_rcv( unsigned int tick );
int lps25h_conv( frizz_fp* press_data );
//void ctrl_pres_lps25h_datarate(int f_rate);

int lps25h_setparam( void *ptr );
unsigned int lps25h_get_ver( void );
unsigned int lps25h_get_name( void );

int lps25h_get_condition( void *data );


#ifdef __cplusplus
}
#endif


#endif // __LPS25H_API_H__
