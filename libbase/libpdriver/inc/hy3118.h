/*!******************************************************************************
 * @file    hy3118.h
 * @brief   hy3118 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __HY3118_H__
#define __HY3118_H__
#include "hy3118_api.h"

/*
 *
 *		By environment, you must edit this file
 *
 */
#define HY31118_ENABLE_CHN2
#define HY31118_ENABLE_CHN3

#define HY3118_I2C_ADDRESS			0x50

/******************************************************************************/
/* Filename:HY311x_REG.h                                                      */
/* HY311x Register Definitions                                                */
/******************************************************************************/

#define  HY311x_SYS  0x00 //System configuration control register (8bit)
#define APO    0x40 //Auto Power off
#define IRQEN  0x20 //SDA interrupt function
#define ENADC  0x10 //ADC control
#define ENLDO  0x08 //Enable LDO control
#define ENREFO 0x04 //Enable REFO control
#define ENOP   0x02 //Enable rail-to-rail OPAMP
#define POWERDOWN   0x00 //Disable all

#define  HY311x_ADC1 0x01 //ADC configuration control register 1(8bit) (ADC input select)
//INN[2:0] Negative input voltage selection
#define INN_AIN1     0x00
#define INN_AIN2     0x08
#define INN_AIN3     0x10
#define INN_AIN4     0x18
#define INN_REFO     0x20
#define INN_OPO      0x28
#define INN_VSSA     0x30
#define INN_AGND_0P1 0x38

//INP[2:0] Positive input voltage selection
#define INP_AIN1     0x00
#define INP_AIN2     0x01
#define INP_AIN3     0x02
#define INP_AIN4     0x03
#define INP_REFO     0x04
#define INP_OPO      0x05
#define INP_VSSA     0x06
#define INP_AGND_0P1 0x07

#define  HY311x_ADC2 0x02 //ADC configuration control register 2(8bit)
//VRPS[1:0] Possitive reference voltage selection
#define VRPS_Vrefp_UnBff 0x00 //(Unbuffered)
#define VRPS_VDDA        0x40
#define VRPS_Vrefp_Bff   0x80 //(buffered)
#define VRPS_V12         0xC0 //Internal reference voltage V12

//VRNS[1:0] Negative reference voltage selection
#define VRNS_Vrefn_UnBff 0x00 //(Unbuffered)
#define VRNS_VSSA        0x10
#define VRNS_Vrefn_Bff   0x20 //(buffered)
#define VRNS_V12         0x30 //Internal reference voltage V12

//DCSET[3:0] DC offset input voltage selection (VREF = REFP-REFN)
#define DCSET_VREF        0x00 //<0000> 0 VREF
#define DCSET_VREF_0p125  0x01 //<0001> +1/8 VREF
#define DCSET_VREF_0p25   0x02 //<0010> +1/4 VREF
#define DCSET_VREF_0p375  0x03 //<0011> +3/8 VREF
#define DCSET_VREF_0p5    0x04 //<0100> +1/2 VREF
#define DCSET_VREF_0p625  0x05 //<0101> +5/8 VREF
#define DCSET_VREF_0p75   0x06 //<0110> +3/4 VREF
#define DCSET_VREF_0p875  0x07 //<0111> +7/8 VREF
#define DCSET_VREF_0      0x08 //<1000> 0 VREF
#define DCSET_VREF_n0p125 0x09 //<1001> -1/8 VREF
#define DCSET_VREF_n0p25  0x0A //<1010> -1/4 VREF
#define DCSET_VREF_n0p375 0x0B //<1011> -3/8 VREF
#define DCSET_VREF_n0p5   0x0C //<1100> -1/2 VREF
#define DCSET_VREF_n0p625 0x0D //<1101> -5/8 VREF
#define DCSET_VREF_n0p75  0x0E //<1110> -3/4 VREF
#define DCSET_VREF_n0p875 0x0F //<1111> -7/8 VREF

#define  HY311x_ADC3 0x03 //ADC configuration control register 3(8bit)
//OSCS[1:0] Oscillator source select
#define  OSCS_Int327KHz   0x00 //<00> Internal oscillator 327KHz
#define  OSCS_Int1000KHz  0x40 //<01> Internal oscillator 1000KHz
#define  OSCS_ExtDiv15    0x80 //<10> External oscillator divider by 15
#define  OSCS_ExtDiv5     0xC0 //<11> External oscillator divider by 5

//FRb [0] Full reference range select
#define  FRb_Full 0x00 //<0> Full reference range input
#define  FRb_Half 0x20 //<1> 1/2 reference range input

//PGA[2:0] Input signal gain for modulator
#define  PGA_Dis 0x00 //<000> PGA Disable
#define  PGA_8   0x04 //<001> Gain = 8
//<010> Reservations
#define  PGA_16  0x0C //<011> Gain = 16
//<100> Reservations
//<101> Reservations
//<110> Reservations
#define  PGA_32  0x1C //<111> Gain = 32

//ADGN[1:0] Input signal gain for modulator
#define  ADGN_1  0x00 //<00> Gain = 1
#define  ADGN_2  0x01 //<01> Gain = 2
//<10> Reservations
#define  ADGN_4  0x03 //<11> Gain = 4

#define  HY311x_ADC4 0x04 //ADC configuration control register 3(8bit)
//LDO[1:0] LDO output voltage selection
#define  LDO_3p3   0x00 //<00> 3.3V
#define  LDO_3p0   0x40 //<01> 3.0V
#define  LDO_2p7   0x80 //<10> 2.7V
#define  LDO_2p4   0xC0 //<11> 2.4V

//REFO[0] Reference voltage selection
#define  REFO_1p2  0x00 //<0> REFO= 1.2V
#define  REFO_1p5  0x20 //<1> REFO = 1.5V

//HS[0] High conversion rate
#define  HS_Slow   0x00 //<0> Slow sampling rate (327KHz)
#define  HS_High   0x10 //<1> High sampling rate (1000K Hz)

//OSR[2:0] ADC output rate select
#define  OSR_128   0x00 //<000> 2560sps / 7680sps
#define  OSR_256   0x02 //<001> 1280sps / 3840sps
#define  OSR_512   0x04 //<010> 640sps / 1920sps
#define  OSR_1024  0x06 //<011> 320sps / 960sps
#define  OSR_2048  0x08 //<100> 160sps / 480sps
#define  OSR_4096  0x0A //<101> 80sps / 240sps
#define  OSR_8192  0x0C //<110> 40sps / 120sps
#define  OSR_32768 0x0E //<111> 10sps / 30sps

#define  HY311x_ADO 0x05//ADC Output Code(24bit)
/*       ADO[23:1] ADC output Code               */
/*       ADOH[7:0] ADC output Code of ADO[23:16] */
/*       ADOM[7:0] ADC output Code of ADO[15:8]  */
/*       ADOL[7:1] ADC output Code of ADO[7:1]   */

#define  ADST 0x01 //[0] ADC output Code Status,<0> Information has been read,<1> Information is not read

#define  SET_SYS ENADC | ENLDO | ENREFO;
//#define  SET_SYS IRQEN | ENADC | ENLDO | ENREFO;
#define  SET_ADC1 INN_AIN1 | INP_AIN2;
#define  SET_ADC2 VRPS_V12 | VRNS_VSSA;
#define  SET_ADC3 OSCS_ExtDiv15 | FRb_Full;
#define  SET_ADC4 LDO_2p4 | REFO_1p2 | OSR_32768;


#endif	// __HY3118_H__
