/*!******************************************************************************
 * @file    hts221.h
 * @brief   hts221 sensor driver header
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __HTS221_H__
#define __HTS221_H__
#include "hts221_api.h"


#define PRES_hts221_I2C_ADDRESS    0x5F


#define HTS221_DEVICE_ID    0xBC


#define BIT(x) ( (x) )

//Register Definition
#define HTS221_WHO_AM_I				0x0F  // device identification register


#define HTS221_AV_CONF			0x10
/* Humidity Sensor Resolution */
#define HTS221_H_RESOLUTION_4    (0x00)  /* Resolution set to 0.4 %RH */
#define HTS221_H_RESOLUTION_8    (0x01)  /* Resolution set to 0.3 %RH */
#define HTS221_H_RESOLUTION_16   (0x02)  /* Resolution set to 0.2 %RH */
#define HTS221_H_RESOLUTION_32   (0x03)  /* Resolution set to 0.15 %RH */
#define HTS221_H_RESOLUTION_64   (0x04)  /* Resolution set to 0.1 %RH */
#define HTS221_H_RESOLUTION_128  (0x05)  /* Resolution set to 0.07 %RH */
#define HTS221_H_RESOLUTION_256  (0x06)  /* Resolution set to 0.05 %RH */
#define HTS221_H_RESOLUTION_512  (0x07)  /* Resolution set to 0.03 %RH */

/* Temperature Sensor Resolution */
#define HTS221_T_RESOLUTION_2    (0x00)  /* Resolution set to 0.08 DegC */
#define HTS221_T_RESOLUTION_4    (0x08)  /* Resolution set to 0.05 DegC */
#define HTS221_T_RESOLUTION_8    (0x10)  /* Resolution set to 0.04 DegC */
#define HTS221_T_RESOLUTION_16   (0x18)  /* Resolution set to 0.03 DegC */
#define HTS221_T_RESOLUTION_32   (0x20)  /* Resolution set to 0.02 DegC */
#define HTS221_T_RESOLUTION_64   (0x28)  /* Resolution set to 0.015 DegC */
#define HTS221_T_RESOLUTION_128  (0x30)  /* Resolution set to 0.01 DegC */
#define HTS221_T_RESOLUTION_256  (0x38)  /* Resolution set to 0.007 DegC */

// frequency
#define	HTS221_FREQ_ONESHOT		0
#define	HTS221_FREQ_1HZ			1
#define	HTS221_FREQ_7HZ			2
#define	HTS221_FREQ_12_5HZ		3


// CONTROL REGISTER 1
#define HTS221_CTRL_REG1			0x20
#define HTS221_POWER_DOWN			BIT(7)
#define HTS221_BDU				BIT(2)
#define HTS221_ODR_BIT				BIT(0)

// CONTROL REGISTER 2
#define HTS221_CTRL_REG2			0x21
#define HTS221_BOOT				BIT(7)
#define HTS221_HEATER			BIT(1)
#define HTS221_ONE_SHOT				BIT(0)

// CONTROL REGISTER 3
#define HTS221_CTRL_REG3			0x22
#define HTS221_DRDY_H_L				BIT(7)
#define HTS221_PP_OD				BIT(6)
#define HTS221_DRDY			BIT(2)

//STATUS_REG
#define HTS221_STATUS_REG			0x27
#define HTS221_H_DA                                   BIT(1)
#define HTS221_T_DA                                   BIT(0)

//OUTPUT REGISTER
#define HTS221_HUMIDITY_OUT_L			0x28
#define HTS221_HUMIDITY_OUT_H			0x29
#define HTS221_TEMP_OUT_L				0x2a
#define HTS221_TEMP_OUT_H				0x2b

#define HTS221_H0_rH_x2			0x30
#define HTS221_H1_rH_x2			0x31
#define HTS221_H0_T0_OUT			0x36
#define HTS221_H1_T0_OUT			0x3a
#define HTS221_T0_OUT				0x3c
#define HTS221_T1_OUT				0x3e

#define HTS221_ODR_MASK    0x03
#define HTS221_ONE_SHOT_MASK    0x01

//It must be defined when you want to read multiple bytes incrementing the register address
#define HTS221_I2C_BURSTREAD	        0x80

#endif // __HTS221_H__

