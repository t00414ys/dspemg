/*!******************************************************************************
 * @file    adxl362.c
 * @brief   Mc3413 physical sensor driver
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "spi.h"
#include "timer.h"
#include "adxl362.h"
#include "config_type.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_ADXL362

#define		DRIVER_VER_MAJOR		(1)				// Major Version
#define		DRIVER_VER_MINOR		(0)				// Minor Version
#define		DRIVER_VER_DETAIL		(0)				// Detail Version

struct {
	frizz_fp			scale;		// scaler
	unsigned char		buff[6];	// transmission buffer
	unsigned char		spi_mode;
	unsigned int		cs_no;
	unsigned int		target_freq;
	setting_direction_t	setting;	// direction of device
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[3];
	//
} g_adxl362;

#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char	g_init_done = D_INIT_NONE;


//declare a constant for adxl362_reg_write and adxl362_reg_read function.
enum spi_cs_keep {CS_TURN_UP = 0, CS_KEEP = 1};
static const unsigned int cmd_size = 1;
static const unsigned int addr_size = 1;
unsigned char setting_value = ( ADXL362_ACCL_RANGE | ACC_BANDWIDTH | ADXL362_ACCL_ODR );

static void adxl362_reg_write( unsigned char addr, unsigned char* tx_data, unsigned char length )
{
	unsigned char write_command = 0x0a;
	unsigned char* no_read = 0;
	spi_trans_data( &write_command, no_read, cmd_size, g_adxl362.cs_no, CS_KEEP );
	spi_trans_data( &addr, no_read, addr_size, g_adxl362.cs_no, CS_KEEP );
	spi_trans_data( tx_data, no_read, length, g_adxl362.cs_no, CS_TURN_UP );
}

static void adxl362_reg_read( unsigned char addr, unsigned char* rx_data, unsigned char length )
{
	unsigned char read_command = 0x0b;
	unsigned char* no_write = 0;
	unsigned char* no_read = 0;
	spi_trans_data( &read_command, no_read, cmd_size, g_adxl362.cs_no, CS_KEEP );
	spi_trans_data( &addr, no_read, addr_size, g_adxl362.cs_no, CS_KEEP );
	spi_trans_data( no_write, rx_data, length, g_adxl362.cs_no, CS_TURN_UP );
}

int adxl362_init( unsigned int param )
{
	const unsigned char soft_reset_reg_addr = 0x1f;
	unsigned char reset_code = 'R';
	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	/* parameter */
	g_adxl362.scale = as_frizz_fp( ADXL362_ACCL_PER_LSB );

	/* setting direction default set */
	if( param == 0 ) {
		g_adxl362.setting.map_x		= DEFAULT_MAP_X;
		g_adxl362.setting.map_y		= DEFAULT_MAP_Y;
		g_adxl362.setting.map_z		= DEFAULT_MAP_Z;
		g_adxl362.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_adxl362.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_adxl362.setting.negate_z	= SETTING_DIRECTION_ASSERT;
	} else {
		EXPAND_MAP(	param,
					g_adxl362.setting.map_x,		g_adxl362.setting.map_y,		g_adxl362.setting.map_z,
					g_adxl362.setting.negate_x,	g_adxl362.setting.negate_y,	g_adxl362.setting.negate_z );
	}

	g_adxl362.cs_no = ADXL362_SPI_CS_NO;
	g_adxl362.target_freq = ADXL362_SPI_FREQ;
	g_adxl362.spi_mode = 0;
	spi_init( g_ROSC2_FREQ, g_adxl362.target_freq, g_adxl362.spi_mode, g_adxl362.cs_no );

	/* Read device ID */
	// adxl362
	{
		unsigned char read_id[3];
		adxl362_reg_read( ADXL362_DEVID_AD_REG_ADD, read_id, 3 );
		if( read_id[0] != ADXL362_DEVID_AD_REG_VAL ) {
			return RESULT_ERR_INIT;
		}
		if( read_id[1] != ADXL362_DEVID_MST_REG_VAL ) {
			return RESULT_ERR_INIT;
		}
		if( read_id[2] != ADXL362_PARTID_REG_VAL ) {
			return RESULT_ERR_INIT;
		}
	}

	/* soft Reset */
	adxl362_reg_write( soft_reset_reg_addr, &reset_code, 1 );

	/* setting for Range and Output Data Rate */
	adxl362_reg_write( ADXL362_FILTER_CTL_REG_ADD, &setting_value, 1 );

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}

void adxl362_ctrl_accl( int f_ena )
{
	unsigned char setting_value;
	spi_init( g_ROSC2_FREQ, g_adxl362.target_freq, g_adxl362.spi_mode, g_adxl362.cs_no );	//Since the setting after initialization is overwritten somewhere , it will re-initialize .
	if( f_ena ) {
		setting_value = ( ADXL362_MEASURE_MEASURE | ADXL362_LOW_NOISE );
	} else {
		setting_value = ( ADXL362_MEASURE_STANDBY | ADXL362_LOW_NOISE );
	}

	adxl362_reg_write( ADXL362_POWER_CTL_REG_ADD, &setting_value, 1 );
}

unsigned int adxl362_rcv_accl( unsigned int tick )
{
	const unsigned char status_reg_addr = 0x0b;
	const unsigned char axis_data_reg_addr = 0x0e;
	const unsigned char data_ready_bit = 0x01;
	unsigned char status;

	//	spi_init( g_ROSC2_FREQ, g_adxl362.target_freq, g_adxl362.spi_mode, g_adxl362.cs_no );
	adxl362_reg_read( status_reg_addr, &status, 1 );
	if( ( status & data_ready_bit ) == data_ready_bit ) {
		adxl362_reg_read( axis_data_reg_addr, g_adxl362.buff, sizeof( g_adxl362.buff ) );
		g_adxl362.recv_result = 0;
	} else {
		g_adxl362.recv_result = 1;
	}
	return 0;
}

int adxl362_conv_accl( frizz_fp data[3] )
{
	frizz_fp4w f4w_buff;			// x:0, y:1, z:2
	frizz_fp* fz = ( frizz_fp* )&f4w_buff;
	float* fp = ( float* )&f4w_buff;
	sensor_util_half_t s_buff[3];	// x:0, y:1, z:2

	if( g_adxl362.recv_result != 0 ) {
		data[0] = g_adxl362.lasttime_data[0];
		data[1] = g_adxl362.lasttime_data[1];
		data[2] = g_adxl362.lasttime_data[2];
		return RESULT_SUCCESS_CONV;
	}

	/* data for Accel using adxl362 */
	s_buff[0].ubyte[0] = g_adxl362.buff[0];	// XOUT_EX_L
	s_buff[0].ubyte[1] = g_adxl362.buff[1];	// XOUT_EX_H
	s_buff[1].ubyte[0] = g_adxl362.buff[2];	// YOUT_EX_L
	s_buff[1].ubyte[1] = g_adxl362.buff[3];	// YOUT_EX_H
	s_buff[2].ubyte[0] = g_adxl362.buff[4];	// ZOUT_EX_L
	s_buff[2].ubyte[1] = g_adxl362.buff[5];	// ZOUT_EX_H

	// to 4way float
	// {Dx, Dy, Dz} = {Sx, Sy, Sz} adjust sensor and board angle.
	fp[0] = ( float )( g_adxl362.setting.negate_x ? ( -s_buff[g_adxl362.setting.map_x].half ) : ( s_buff[g_adxl362.setting.map_x].half ) );
	fp[1] = ( float )( g_adxl362.setting.negate_y ? ( -s_buff[g_adxl362.setting.map_y].half ) : ( s_buff[g_adxl362.setting.map_y].half ) );
	fp[2] = ( float )( g_adxl362.setting.negate_z ? ( -s_buff[g_adxl362.setting.map_z].half ) : ( s_buff[g_adxl362.setting.map_z].half ) );

	// convert
	f4w_buff = g_adxl362.scale * f4w_buff;
	data[0] = fz[0];
	data[1] = fz[1];
	data[2] = fz[2];

	g_adxl362.lasttime_data[0] = data[0];
	g_adxl362.lasttime_data[1] = data[1];
	g_adxl362.lasttime_data[2] = data[2];

	return RESULT_SUCCESS_CONV;
}

int adxl362_setparam_accl( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_adxl362.setting.map_x		= setting->map_x;
		g_adxl362.setting.map_y		= setting->map_y;
		g_adxl362.setting.map_z		= setting->map_z;
		g_adxl362.setting.negate_x	= setting->negate_x;
		g_adxl362.setting.negate_y	= setting->negate_y;
		g_adxl362.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

int adxl362_get_condition( void *data )
{
	return g_adxl362.device_condition;
}

int adxl362_get_raw_data( void *data )
{
	short *temp_buffer = ( short* )data;
	temp_buffer[0] = ( short )( ( g_adxl362.buff[1] << 8 ) | g_adxl362.buff[0] );
	temp_buffer[1] = ( short )( ( g_adxl362.buff[3] << 8 ) | g_adxl362.buff[2] );
	temp_buffer[2] = ( short )( ( g_adxl362.buff[5] << 8 ) | g_adxl362.buff[4] );

	return	RESULT_SUCCESS_SET;
}

unsigned int adxl362_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int adxl362_get_name()
{
	return	D_DRIVER_NAME;
}


