/*!******************************************************************************
 * @file    bmm150.c
 * @brief   bmm150 sensor driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <string.h>
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "timer.h"
#include "bmm150.h"
#include "config_type.h"
#include "if/magn_calib_raw_if.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_BMM150


#define		DRIVER_VER_MAJOR		(1)						// Major Version
#define		DRIVER_VER_MINOR		(1)						// Minor Version
#define		DRIVER_VER_DETAIL		(0)						// Detail Version

struct {
	frizz_fp					scale;						// scaler
	unsigned char				buff[8];					// transmission buffer
	setting_direction_t			setting;
	unsigned char				ctrl;
	unsigned char				addr;
	unsigned char				device_id;
	magnet_calib_raw_result_t	save_status;

	// ** detect a device error **
	unsigned int				device_condition;
	int							recv_result;
	frizz_fp					lasttime_data[3];
	//
} g_bmm150;

struct {
	char						dig_x1;						/**< trim x1 data */
	char						dig_y1;						/**< trim y1 data */

	char						dig_x2;						/**< trim x2 data */
	char 						dig_y2;						/**< trim y2 data */

	unsigned short				dig_z1;						/**< trim z1 data */
	short						dig_z2;						/**< trim z2 data */
	short						dig_z3;						/**< trim z3 data */
	short						dig_z4;						/**< trim z4 data */

	unsigned char				dig_xy1;					/**< trim xy1 data */
	char						dig_xy2;					/**< trim xy2 data */

	unsigned short				dig_xyz1;					/**< trim xyz1 data */
} g_trimming_bmm150;


#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char		g_init_done = D_INIT_NONE;

static unsigned char	add_tbl[] = {
	BMM150_I2C_ADDRESS_10,	///< Magnet Sensor(made from BMM)
	BMM150_I2C_ADDRESS_11,	///< Magnet Sensor(made from BMM)
	//  BMM150_I2C_ADDRESS_12,	///< Magnet Sensor(made from BMM)
	//  BMM150_I2C_ADDRESS_13		///< Magnet Sensor(made from BMM)
};

int bmm150_init_trim_registers( void )
{
	unsigned char temp_buf[2];
	i2c_read( g_bmm150.addr, BMM150_DIG_X1, ( unsigned char * ) &g_trimming_bmm150.dig_x1, 1 );
	i2c_read( g_bmm150.addr, BMM150_DIG_Y1, ( unsigned char * ) &g_trimming_bmm150.dig_y1, 1 );
	i2c_read( g_bmm150.addr, BMM150_DIG_X2, ( unsigned char * ) &g_trimming_bmm150.dig_x2, 1 );
	i2c_read( g_bmm150.addr, BMM150_DIG_Y2, ( unsigned char * ) &g_trimming_bmm150.dig_y2, 1 );
	i2c_read( g_bmm150.addr, BMM150_DIG_XY1, ( unsigned char * ) &g_trimming_bmm150.dig_xy1, 1 );
	i2c_read( g_bmm150.addr, BMM150_DIG_XY2, ( unsigned char * ) &g_trimming_bmm150.dig_xy2, 1 );

	i2c_read( g_bmm150.addr, BMM150_DIG_Z1_LSB, temp_buf, 2 );
	g_trimming_bmm150.dig_z1 = ( unsigned short )( ( ( unsigned int )temp_buf[1] << 8 ) + temp_buf[0] );
	i2c_read( g_bmm150.addr, BMM150_DIG_Z2_LSB, temp_buf, 2 );
	g_trimming_bmm150.dig_z2 = ( signed short )( ( ( signed int )temp_buf[1] << 8 ) + temp_buf[0] );
	i2c_read( g_bmm150.addr, BMM150_DIG_Z3_LSB, temp_buf, 2 );
	g_trimming_bmm150.dig_z3 = ( signed short )( ( ( signed int )temp_buf[1] << 8 ) + temp_buf[0] );
	i2c_read( g_bmm150.addr, BMM150_DIG_Z4_LSB, temp_buf, 2 );
	g_trimming_bmm150.dig_z4 = ( signed short )( ( ( signed int )temp_buf[1] << 8 ) + temp_buf[0] );
	i2c_read( g_bmm150.addr, BMM150_DIG_XYZ1_LSB, temp_buf, 2 );
	g_trimming_bmm150.dig_xyz1 = ( unsigned short )( ( ( unsigned int )temp_buf[1] << 8 ) + temp_buf[0] );

	return 0 ;
}

short bmm150_compensate_XYZ( short mag_data, unsigned short data_resist, char XYZ )
{
	unsigned short data_r;
	short inter_retval = 0;
	int	temp_z;

	data_r = ( data_resist != C_BMM050_ZERO_U8X ) ? data_resist : g_trimming_bmm150.dig_xyz1;

	if( ( XYZ == 0 ) || ( XYZ == 1 ) ) {	// x-axis
		if( mag_data == -4096 ) {
			return -32768;
		}
		if( ( data_r == C_BMM050_ZERO_U8X ) && ( g_trimming_bmm150.dig_xyz1 == C_BMM050_ZERO_U8X ) ) {
			return -32768;
		}
		inter_retval = ( ( short )( ( ( unsigned short )( ( ( ( int )g_trimming_bmm150.dig_xyz1 ) << 14 ) / ( data_r ) ) ) - ( ( unsigned short )BMM050_HEX_FOUR_THOUSAND ) ) );

		inter_retval = ( ( short )( ( ( ( int )mag_data ) * ( ( ( ( ( ( ( ( int )g_trimming_bmm150.dig_xy2 ) * ( ( ( ( int )inter_retval ) *
																		( ( int )inter_retval ) ) >>	7 ) ) +
																	  ( ( ( int )inter_retval ) * ( ( int )( ( ( short )g_trimming_bmm150.dig_xy1 ) << 7 ) ) ) ) >> 9 ) + ( ( int )BMM050_HEX_ONE_LACK ) ) * ( ( int )( ( ( short )g_trimming_bmm150.dig_x2 ) +
																			  ( ( short )BMM050_HEX_A_ZERO ) ) ) ) >>  12 ) ) >> 13 ) ) + ( ( ( short )g_trimming_bmm150.dig_x1 ) << 3 );
	}

	/*
		if(XYZ == 1)		// y-axis is the same as x-axis
		{
			if(mag_data == -4096)
				return -32768;
			if (data_r == C_BMM050_ZERO_U8X) {
				return -32768;
			}
			inter_retval = ((short)(((unsigned short)((((int)g_trimming_bmm150.dig_xyz1)<< 14)/(data_r))) -((unsigned short)BMM050_HEX_FOUR_THOUSAND)));

			inter_retval = ((short)((((int)mag_data) * ((((((((int)g_trimming_bmm150.dig_xy2) *((((int)inter_retval) *
							((int)inter_retval)) >>	7)) +
						     (((int)inter_retval) * ((int)(((short)g_trimming_bmm150.dig_xy1) << 7)))) >> 9) +  ((int)BMM050_HEX_ONE_LACK)) * ((int)(((short)g_trimming_bmm150.dig_y2) +
						  ((short)BMM050_HEX_A_ZERO)))) >>  12)) >> 13)) +(((short)g_trimming_bmm150.dig_y1)<< 3);
		}
	*/
	if( XYZ == 2 ) {	// z-axis
		if( mag_data == -16384 ) {
			return -32768;
		}
		if( ( g_trimming_bmm150.dig_z2 == BMM050_ZERO_U8X ) || ( g_trimming_bmm150.dig_z1 == BMM050_ZERO_U8X )  || ( data_r == BMM050_ZERO_U8X ) ) {
			return -32768;
		}

#if 0 // this interval not use
		inter_retval = ( ( short )( ( ( unsigned short )( ( ( ( int )g_trimming_bmm150.dig_xyz1 ) << 14 ) / ( data_r ) ) ) - ( ( unsigned short )BMM050_HEX_FOUR_THOUSAND ) ) );
#endif

		temp_z = ( ( ( ( ( int )( mag_data - g_trimming_bmm150.dig_z4 ) ) << 15 ) -	( ( ( ( int )g_trimming_bmm150.dig_z3 ) * ( ( int )( ( ( short )data_r ) -	( ( short ) g_trimming_bmm150.dig_xyz1 ) ) ) )	>> 2 ) ) /
				   ( g_trimming_bmm150.dig_z2 + ( ( short )( ( ( ( ( int )g_trimming_bmm150.dig_z1 ) * ( ( ( ( short )data_r ) << 1 ) ) ) + ( C_BMM050_ONE_U8X << 15 ) ) >> 16 ) ) ) );

		/* saturate result to +/- 2 mT */
		inter_retval = ( short )temp_z;

		if( temp_z > BMM050_POSITIVE_SATURATION_Z ) {
			inter_retval =  BMM050_POSITIVE_SATURATION_Z;
		} else {
			if( temp_z < BMM050_NEGATIVE_SATURATION_Z ) {
				inter_retval = BMM050_NEGATIVE_SATURATION_Z;
			}
		}
	}


	return inter_retval;
}

int bmm150_init( unsigned int param )
{
	int ret, i;
	unsigned char buff, tmp;

	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	/* parameter */
	g_bmm150.scale = as_frizz_fp( BMM150_MICRO_TESLA_PER_LSB );

	g_bmm150.ctrl = BMM150_REGULAR_DR | BMM150_FORCED_MODE;

	if( param == 0 ) {
		/* setting direction default set */
		g_bmm150.setting.map_x		= DEFAULT_MAP_X;
		g_bmm150.setting.map_y		= DEFAULT_MAP_Y;
		g_bmm150.setting.map_z		= DEFAULT_MAP_Z;
		g_bmm150.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_bmm150.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_bmm150.setting.negate_z	= SETTING_DIRECTION_ASSERT;
	} else {
		EXPAND_MAP(	param,
					g_bmm150.setting.map_x,		g_bmm150.setting.map_y,		g_bmm150.setting.map_z,
					g_bmm150.setting.negate_x,	g_bmm150.setting.negate_y,	g_bmm150.setting.negate_z );
	}

	/* recognition */
	for( i = 0 ; i < NELEMENT( add_tbl ) ; i++ ) {
		g_bmm150.addr = add_tbl[i];
		g_bmm150.device_id = 0;
		ret = i2c_read( g_bmm150.addr, BMM150_CHIP_ID, &g_bmm150.device_id, 1 );
		if( ret == 0 ) {
			// Save the contents of the POWER_CONTROL register
			tmp       = 0;
			i2c_read( g_bmm150.addr, BMM150_POWER_CONTROL, &tmp, 1 );
			//change suspend to sleep.
			buff      = BMM150_POWER_SLEEP;
			ret = i2c_write( g_bmm150.addr, BMM150_POWER_CONTROL, &buff, 1 );
			if( ret == 0 ) {
				mdelay( 2 );
				g_bmm150.device_id = 0;
				ret = i2c_read( g_bmm150.addr, BMM150_CHIP_ID, &g_bmm150.device_id, 1 );
				if( ret == 0 ) {
					if( g_bmm150.device_id == BMM150_WHOAMI_ID ) {
						break;
					}
				}
				// Return the contents of the POWER_CONTROL register
				buff      = tmp;
				ret = i2c_write( g_bmm150.addr, BMM150_POWER_CONTROL, &buff, 1 );
			}
		}
	}
	if( g_bmm150.device_id != BMM150_WHOAMI_ID ) {
		return RESULT_ERR_INIT;
	}

	bmm150_init_trim_registers();

	/* Set the data rate for ??? mode */
	i2c_read( g_bmm150.addr, BMM150_CONTROL, &buff, 1 );
	buff &= ~0x38;
	buff |= BMM150_REGULAR_DR;
	i2c_write( g_bmm150.addr, BMM150_CONTROL, &buff, 1 );

	/* Set the XY-repetitions number for ??? mode */
	buff = BMM150_REGULAR_REPXY;
	i2c_write( g_bmm150.addr, BMM150_REP_XY, &buff, 1 );

	/* Set the Z-repetitions number  for ??? mode */
	buff = BMM150_REGULAR_REPZ;
	i2c_write( g_bmm150.addr, BMM150_REP_Z, &buff, 1 );

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}

void bmm150_ctrl( int f_ena )
{
	unsigned char       buff;

	i2c_read( g_bmm150.addr, BMM150_CONTROL, &buff, 1 );
	buff &= ~0x06;

	if( f_ena == CTRL_ACTIVATE ) {
		buff = g_bmm150.ctrl;
	} else if( f_ena == CTRL_DEACTIVATE ) {
		buff      = BMM150_SLEEP_MODE;
	}

	i2c_write( g_bmm150.addr, BMM150_CONTROL, &buff, 1 );

}

unsigned int bmm150_rcv( unsigned int tick )
{
	g_bmm150.recv_result = i2c_read( g_bmm150.addr, BMM150_DATAX_LSB, &g_bmm150.buff[0], sizeof( g_bmm150.buff ) );
	if( g_bmm150.recv_result == 0 ) {
		g_bmm150.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_bmm150.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}

	// write to update data
	i2c_write( g_bmm150.addr, BMM150_CONTROL, &g_bmm150.ctrl, 1 );
	return 0;
}

int bmm150_conv( frizz_fp data[3] )
{
	frizz_fp4w			f4w_buff;		// x:0, y:1, z:2
	frizz_fp			*fz = ( frizz_fp* )&f4w_buff;
	float				*fp = ( float* )&f4w_buff;
	sensor_util_half_t	s_buff[4];

	if( g_bmm150.recv_result != 0 ) {
		data[0] = g_bmm150.lasttime_data[0];
		data[1] = g_bmm150.lasttime_data[1];
		data[2] = g_bmm150.lasttime_data[2];
		return RESULT_SUCCESS_CONV;
	}

	s_buff[0].half = ( ( ( ( signed int )( ( signed char )g_bmm150.buff[1] ) ) <<  5 ) |		//type cast with sign extension -> arithmetic shift
					   ( ( signed int )( ( ( g_bmm150.buff[0] ) >>  3 ) & 0x1F ) ) );				//logical shift -> AND operation -> type cast with zero extension
	s_buff[1].half = ( ( ( ( signed int )( ( signed char )g_bmm150.buff[3] ) ) <<  5 ) |
					   ( ( signed int )( ( g_bmm150.buff[2] >>  3 ) & 0x1F ) ) );
	s_buff[2].half = ( ( ( ( signed int )( ( signed char )g_bmm150.buff[5] ) ) <<  7 ) |
					   ( ( signed int )( ( g_bmm150.buff[4] >>  1 ) & 0x7F ) ) );
	s_buff[3].half = ( ( ( ( unsigned short )( ( unsigned int )g_bmm150.buff[7] ) ) <<  6 ) |
					   ( ( g_bmm150.buff[6] >>  2 ) & 0x7F ) );

	/* Compensation for X-Y-Z axis */
	s_buff[0].half = bmm150_compensate_XYZ( s_buff[0].half, s_buff[3].uh, 0 );
	s_buff[1].half = bmm150_compensate_XYZ( s_buff[1].half, s_buff[3].uh, 1 );
	s_buff[2].half = bmm150_compensate_XYZ( s_buff[2].half, s_buff[3].uh, 2 );

	//5bit+8bit->13bit and adjust sensor and board angle.
	// {Dx, Dy, Dz} = {-Sy, -Sx, -Sz}
	fp[0] = ( float )( g_bmm150.setting.negate_x ? ( -s_buff[g_bmm150.setting.map_x].half ) : ( s_buff[g_bmm150.setting.map_x].half ) );
	fp[1] = ( float )( g_bmm150.setting.negate_y ? ( -s_buff[g_bmm150.setting.map_y].half ) : ( s_buff[g_bmm150.setting.map_y].half ) );
	fp[2] = ( float )( g_bmm150.setting.negate_z ? ( -s_buff[g_bmm150.setting.map_z].half ) : ( s_buff[g_bmm150.setting.map_z].half ) );
	//fp[3] = 100*(100/BMM150_MICRO_TESLA_PER_LSB);
	f4w_buff = g_bmm150.scale * f4w_buff;

	data[0] = fz[0];
	data[1] = fz[1];
	data[2] = fz[2];
	//data[3] = fz[3];

	g_bmm150.lasttime_data[0] = data[0];
	g_bmm150.lasttime_data[1] = data[1];
	g_bmm150.lasttime_data[2] = data[2];

	return RESULT_SUCCESS_CONV;
}

int bmm150_setparam( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_bmm150.setting.map_x		= setting->map_x;
		g_bmm150.setting.map_y		= setting->map_y;
		g_bmm150.setting.map_z		= setting->map_z;
		g_bmm150.setting.negate_x	= setting->negate_x;
		g_bmm150.setting.negate_y	= setting->negate_y;
		g_bmm150.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

int bmm150_get_condition( void *data )
{
	return g_bmm150.device_condition;
}

unsigned int bmm150_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int bmm150_get_name()
{
	return	D_DRIVER_NAME;
}


int bmm150_calib_get_status( void *result )
{
	*( magnet_calib_raw_result_t* )result = g_bmm150.save_status;
	return 0;
}
//
int bmm150_calib_get_data( void *data )
{
	return 0;
}
//
int bmm150_calib_set_data( void *data )
{
	return 0;
}
