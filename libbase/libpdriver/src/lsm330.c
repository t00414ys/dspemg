/*!******************************************************************************
 * @file    lsm330.c
 * @brief   lsm330 sensor driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "lsm330.h"
#include "config_type.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_LSM330


#define		DRIVER_VER_MAJOR		(1)						// Major Version
#define		DRIVER_VER_MINOR		(1)						// Minor Version
#define		DRIVER_VER_DETAIL		(0)						// Detail Version

typedef struct {
	frizz_fp				scale;		// scaler
	unsigned char			buff[6];	// transmission buffer
	setting_direction_t		setting;
	// ** detect a device error **
	unsigned int			device_condition;
	int						recv_result;
	frizz_fp				lasttime_data[3];
	//
} lsm330_accl_sensor_t;

typedef struct {
	frizz_fp4w				scale;		// scaler
	unsigned char			buff[8];	// transmission buffer
	setting_direction_t		setting;
	// ** detect a device error **
	unsigned int			device_condition;
	int						recv_result;
	frizz_fp				lasttime_data[4];
	//
} lsm330_gyro_sensor_t;

struct {
	lsm330_accl_sensor_t	accl;
	lsm330_gyro_sensor_t	gyro;
	unsigned char			pwr_mgmt_accl;
	unsigned char			pwr_mgmt_gyro;
	unsigned char			addr[2];
	unsigned char			device_id[2];

} g_lsm330;

#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char		g_init_done = D_INIT_NONE;

static unsigned char	add_tbl_accl[] = {
	LSM330_ACC_addr,
};

static unsigned char	add_tbl_gyro[] = {
	LSM330_GYRO_addr,
};


int lsm330_init( unsigned int param )
{

	int ret, i;
	unsigned char		data;

	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	// parameter
	// for Accelerometer
	g_lsm330.accl.scale = as_frizz_fp( LSM330_ACCEL_PER_LSB );
	// for Gyroscope
	( ( float* ) & ( g_lsm330.gyro.scale ) )[0] =
		( ( float* ) & ( g_lsm330.gyro.scale ) )[1] =
			( ( float* ) & ( g_lsm330.gyro.scale ) )[2] = LSM330_GYRO_PER_LSB;
	( ( float* ) & ( g_lsm330.gyro.scale ) )[3] = LSM330_TEMPERATURE_PER_LSB;

	g_lsm330.pwr_mgmt_accl = acc_power_down;		// CTRL_REG5_A(X-axis enabled,Y-axis enabled,Z-axis enabled)
	g_lsm330.pwr_mgmt_gyro = gyro_powerdown;		// CTRL_REG1_G(X-axis enabled,Y-axis enabled,Z-axis enabled)

	/* setting direction default set */
	if( param == 0 ) {
		g_lsm330.gyro.setting.map_x		= DEFAULT_MAP_X;
		g_lsm330.gyro.setting.map_y		= DEFAULT_MAP_Y;
		g_lsm330.gyro.setting.map_z		= DEFAULT_MAP_Z;
		g_lsm330.gyro.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_lsm330.gyro.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_lsm330.gyro.setting.negate_z	= SETTING_DIRECTION_ASSERT;
		//
		g_lsm330.accl.setting.map_x		= DEFAULT_MAP_X;
		g_lsm330.accl.setting.map_y		= DEFAULT_MAP_Y;
		g_lsm330.accl.setting.map_z		= DEFAULT_MAP_Z;
		g_lsm330.accl.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_lsm330.accl.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_lsm330.accl.setting.negate_z	= SETTING_DIRECTION_ASSERT;
	} else {
		EXPAND_MAP(	param,
					g_lsm330.gyro.setting.map_x,	g_lsm330.gyro.setting.map_y,	g_lsm330.gyro.setting.map_z,
					g_lsm330.gyro.setting.negate_x,	g_lsm330.gyro.setting.negate_y,	g_lsm330.gyro.setting.negate_z );
		EXPAND_MAP(	param,
					g_lsm330.accl.setting.map_x,	g_lsm330.accl.setting.map_y,	g_lsm330.accl.setting.map_z,
					g_lsm330.accl.setting.negate_x,	g_lsm330.accl.setting.negate_y,	g_lsm330.accl.setting.negate_z );
	}


	//LSM330 G_sensor init

	///////////////////
	//
	//  ACCEL Init
	//
	// @@ i2c address search
	for( i = 0 ; i < NELEMENT( add_tbl_accl ) ; i++ ) {
		g_lsm330.addr[0] =  add_tbl_accl[i];
		g_lsm330.device_id[0] = 0;			// read id
		ret = i2c_read( g_lsm330.addr[0], LSM330_WHO_AM_I_A, &g_lsm330.device_id[0], 1 );
		if( ret == I2C_RESULT_SUCCESS ) {
			if( g_lsm330.device_id[0] == LSM330_WHO_AM_I_ID_A ) {
				break;
			}
		}
	}

	if( g_lsm330.device_id[0] != LSM330_WHO_AM_I_ID_A ) {
		return RESULT_ERR_INIT;
	}
	// @@

	//set LSM330_CTRL_REG6_A 0x24
	// bandwidth 800Hz & sensitivity 4g
	data      = acc_bandwidth_800Hz | acc_4G;
	ret = i2c_write( g_lsm330.addr[0], LSM330_CTRL_REG6_A, &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	//set LSM330_CTRL_REG5_A 0x20
	// set G sensor to power down in the initial mode
	data      = g_lsm330.pwr_mgmt_accl;
	ret = i2c_write( g_lsm330.addr[0], LSM330_CTRL_REG5_A, &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}


	///////////////////
	//
	//  GYRO Init
	//
	// @@ i2c address search
	for( i = 0 ; i < NELEMENT( add_tbl_gyro ) ; i++ ) {
		g_lsm330.addr[1] =  add_tbl_gyro[i];
		g_lsm330.device_id[1] = 0;			// read id
		ret = i2c_read( g_lsm330.addr[1], LSM330_WHO_AM_I_G, &g_lsm330.device_id[1], 1 );
		if( ret == 0 ) {
			if( g_lsm330.device_id[1] == LSM330_WHO_AM_I_ID_G ) {
				break;
			}
		}
	}

	if( g_lsm330.device_id[1] != LSM330_WHO_AM_I_ID_G ) {
		return RESULT_ERR_INIT;
	}
	// @@

	//set LSM330_CTRL_REG4_G 0x23
	// set Gyro sensitivity 500 dps
	data      = gyro_BDU_on | gyro_FS_500dps;
	ret = i2c_write( g_lsm330.addr[1], LSM330_CTRL_REG4_G, &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}


	//set LSM330_CTRL_REG1_G 0x20
	// set Gyro to power down in the initial mode
	data      = g_lsm330.pwr_mgmt_gyro;
	ret = i2c_write( g_lsm330.addr[1], LSM330_CTRL_REG1_G, &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}


	//set LSM330_CTRL_REG4_G 0x21
	//set high pass filter mode
	data      = gyro_HPF_mode0 | gyro_HPF_HPCF0;
	ret = i2c_write( g_lsm330.addr[1], LSM330_CTRL_REG2_G, &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	//set LSM330_CTRL_REG4_G 0x24
	//high pass filter enable
#if	LPF2_HPF_ON
	data      = gyro_HPF_on | gyro_Out_Sel_2;
#else
	data      = gyro_Out_Sel_2;
#endif
	ret = i2c_write( g_lsm330.addr[1], LSM330_CTRL_REG5_G, &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}

void lsm330_ctrl_accl( int f_ena )
{
	if( f_ena ) {
		g_lsm330.pwr_mgmt_accl = acc_axis_Enable | acc_power_100Hz | acc_BDU_on;
	} else {
		g_lsm330.pwr_mgmt_accl = acc_power_down;
	}
	i2c_write( g_lsm330.addr[0], LSM330_CTRL_REG5_A, &g_lsm330.pwr_mgmt_accl, 1 );
}

void lsm330_ctrl_gyro( int f_ena )
{
	if( f_ena ) {
		g_lsm330.pwr_mgmt_gyro = gyro_axis_Enable | gyro_ODR190Hz_12p5Hz | gyro_normalmode;
	} else {
		g_lsm330.pwr_mgmt_gyro = gyro_powerdown;
	}
	i2c_write( g_lsm330.addr[1], LSM330_CTRL_REG1_G, &g_lsm330.pwr_mgmt_gyro, 1 );
}

unsigned int lsm330_rcv_accl( unsigned int tick )
{
	g_lsm330.accl.recv_result = i2c_read( g_lsm330.addr[0], LSM330_OUT_X_L_A | 0x80, &g_lsm330.accl.buff[0], sizeof( g_lsm330.accl.buff ) );
	if( g_lsm330.accl.recv_result == 0 ) {
		g_lsm330.accl.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_lsm330.accl.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}
	return 0;
}

unsigned int lsm330_rcv_gyro( unsigned int tick )
{
	g_lsm330.gyro.recv_result = i2c_read( g_lsm330.addr[1], LSM330_OUT_TEMP_G | 0x80, &g_lsm330.gyro.buff[0], sizeof( g_lsm330.gyro.buff ) );
	if( g_lsm330.gyro.recv_result == 0 ) {
		g_lsm330.gyro.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_lsm330.gyro.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}
	return 0;
}

int lsm330_conv_accl( frizz_fp data[3] )
{
	frizz_fp4w f4w_buff;		// x:0, y:1, z:2
	frizz_fp* fz = ( frizz_fp* )&f4w_buff;
	float* fp = ( float* )&f4w_buff;
	sensor_util_half_t s_buff[3];	// x:0, y:1, z:2

	if( g_lsm330.accl.recv_result != 0 ) {
		data[0] = g_lsm330.accl.lasttime_data[0];
		data[1] = g_lsm330.accl.lasttime_data[1];
		data[2] = g_lsm330.accl.lasttime_data[2];
		return RESULT_SUCCESS_CONV;
	}


	/* data for Accel using LSM330 */
	// to half @ little endian
	s_buff[0].ubyte[0] = g_lsm330.accl.buff[0];
	s_buff[0].ubyte[1] = g_lsm330.accl.buff[1];
	s_buff[1].ubyte[0] = g_lsm330.accl.buff[2];
	s_buff[1].ubyte[1] = g_lsm330.accl.buff[3];
	s_buff[2].ubyte[0] = g_lsm330.accl.buff[4];
	s_buff[2].ubyte[1] = g_lsm330.accl.buff[5];
	// to 4way float
	// {Dx, Dy, Dz} = {Sx, Sy, Sz}
	fp[0] = ( float )( g_lsm330.accl.setting.negate_x ? ( -s_buff[g_lsm330.accl.setting.map_x].half ) : ( s_buff[g_lsm330.accl.setting.map_x].half ) );
	fp[1] = ( float )( g_lsm330.accl.setting.negate_y ? ( -s_buff[g_lsm330.accl.setting.map_y].half ) : ( s_buff[g_lsm330.accl.setting.map_y].half ) );
	fp[2] = ( float )( g_lsm330.accl.setting.negate_z ? ( -s_buff[g_lsm330.accl.setting.map_z].half ) : ( s_buff[g_lsm330.accl.setting.map_z].half ) );
	// convert
	f4w_buff = g_lsm330.accl.scale * f4w_buff;
	data[0] = fz[0];
	data[1] = fz[1];
	data[2] = fz[2];
	//
	g_lsm330.accl.lasttime_data[0] = data[0];
	g_lsm330.accl.lasttime_data[1] = data[1];
	g_lsm330.accl.lasttime_data[2] = data[2];

	return RESULT_SUCCESS_CONV;
}

int lsm330_conv_gyro( frizz_fp data[4] )
{
	frizz_fp4w f4w_buff;		// x:0, y:1, z:2
	frizz_fp* fz = ( frizz_fp* )&f4w_buff;
	float* fp = ( float* )&f4w_buff;
	sensor_util_half_t s_buff[4];	// x:0, y:1, z:2, t:3

	if( g_lsm330.gyro.recv_result != 0 ) {
		data[0] = g_lsm330.gyro.lasttime_data[0];
		data[1] = g_lsm330.gyro.lasttime_data[1];
		data[2] = g_lsm330.gyro.lasttime_data[2];
		data[3] = g_lsm330.gyro.lasttime_data[3];
		return RESULT_SUCCESS_CONV;
	}
	/* data for Gyro using LSM330 */
	// to half @ little endian
	s_buff[0].ubyte[0] = g_lsm330.gyro.buff[2];
	s_buff[0].ubyte[1] = g_lsm330.gyro.buff[3];
	s_buff[1].ubyte[0] = g_lsm330.gyro.buff[4];
	s_buff[1].ubyte[1] = g_lsm330.gyro.buff[5];
	s_buff[2].ubyte[0] = g_lsm330.gyro.buff[6];
	s_buff[2].ubyte[1] = g_lsm330.gyro.buff[7];
	s_buff[3].ubyte[0] = g_lsm330.gyro.buff[0];
	s_buff[3].ubyte[1] = ( g_lsm330.gyro.buff[0] & 0x80 ) ? 0xFF : 0x00; // if negative
	// to 4way float
	// {Dx, Dy, Dz} = {Sy, Sx, Sz}
	fp[0] = ( float )( g_lsm330.gyro.setting.negate_x ? ( -s_buff[g_lsm330.gyro.setting.map_x].half ) : ( s_buff[g_lsm330.gyro.setting.map_x].half ) );
	fp[1] = ( float )( g_lsm330.gyro.setting.negate_y ? ( -s_buff[g_lsm330.gyro.setting.map_y].half ) : ( s_buff[g_lsm330.gyro.setting.map_y].half ) );
	fp[2] = ( float )( g_lsm330.gyro.setting.negate_z ? ( -s_buff[g_lsm330.gyro.setting.map_z].half ) : ( s_buff[g_lsm330.gyro.setting.map_z].half ) );
	fp[3] = ( float )( s_buff[3].half );
	// convert
	f4w_buff = g_lsm330.gyro.scale * f4w_buff;
	data[0] = fz[0];
	data[1] = fz[1];
	data[2] = fz[2];
	data[3] = fz[3];
	//
	g_lsm330.gyro.lasttime_data[0] = data[0];
	g_lsm330.gyro.lasttime_data[1] = data[1];
	g_lsm330.gyro.lasttime_data[2] = data[2];
	g_lsm330.gyro.lasttime_data[3] = data[3];
	return RESULT_SUCCESS_CONV;
}

int lsm330_setparam_accl( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_lsm330.accl.setting.map_x		= setting->map_x;
		g_lsm330.accl.setting.map_y		= setting->map_y;
		g_lsm330.accl.setting.map_z		= setting->map_z;
		g_lsm330.accl.setting.negate_x	= setting->negate_x;
		g_lsm330.accl.setting.negate_y	= setting->negate_y;
		g_lsm330.accl.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

int lsm330_setparam_gyro( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_lsm330.gyro.setting.map_x		= setting->map_x;
		g_lsm330.gyro.setting.map_y		= setting->map_y;
		g_lsm330.gyro.setting.map_z		= setting->map_z;
		g_lsm330.gyro.setting.negate_x	= setting->negate_x;
		g_lsm330.gyro.setting.negate_y	= setting->negate_y;
		g_lsm330.gyro.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

int lsm330_accl_get_condition( void *data )
{
	return g_lsm330.accl.device_condition;
}


int lsm330_gyro_get_condition( void *data )
{
	return g_lsm330.gyro.device_condition;
}

int lsm330_accl_get_raw_data( void *data )
{
	short *temp_buffer = ( short* )data;
	temp_buffer[0] = ( short )( ( g_lsm330.accl.buff[1] << 8 ) | g_lsm330.accl.buff[0] );
	temp_buffer[1] = ( short )( ( g_lsm330.accl.buff[3] << 8 ) | g_lsm330.accl.buff[2] );
	temp_buffer[2] = ( short )( ( g_lsm330.accl.buff[5] << 8 ) | g_lsm330.accl.buff[4] );

	return	RESULT_SUCCESS_SET;
}

unsigned int lsm330_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int lsm330_get_name()
{
	return	D_DRIVER_NAME;
}

