/*!******************************************************************************
 * @file    bmi160.c
 * @brief   bmi160 sensor driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "timer.h"
#include "bmi160.h"
#include "config_type.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_BMI160

#define		DRIVER_VER_MAJOR		(1)						// Major Version
#define		DRIVER_VER_MINOR		(1)						// Minor Version
#define		DRIVER_VER_DETAIL		(0)						// Detail Version

typedef struct {
	frizz_fp			scale;		// scaler
	unsigned char		buff[6];	// transmission buffer
	setting_direction_t	setting;
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[3];
	//
} bmi160_accl_sensor_t;

typedef struct {
	frizz_fp4w			scale;		// scaler
	unsigned char		buff[6];	// transmission buffer
	setting_direction_t	setting;
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[4];
	//
} bmi160_gyro_sensor_t;

typedef struct {
	frizz_fp4w			scale;		// scaler
	unsigned char		buff[2];	// transmission buffer
} bmi160_temp_sensor_t;

struct {
	bmi160_accl_sensor_t	accl;
	bmi160_gyro_sensor_t	gyro;
	bmi160_temp_sensor_t	temp;
	unsigned char			addr;
	unsigned char			device_id;
} g_bmi160;

#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char	g_init_done = D_INIT_NONE;

static unsigned char	add_tbl[] = {
	BMI160_I2C_ADDRESS_H,
	BMI160_I2C_ADDRESS_L,
};

int bmi160_init( unsigned int param )
{
	unsigned char	buff;
	int				i, ret;

	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	/* parameter */
	g_bmi160.accl.scale = as_frizz_fp( BMI160_ACCL_PER_LSB );
	g_bmi160.gyro.scale = as_frizz_fp( 1 );
	( ( float* ) & ( g_bmi160.gyro.scale ) )[0] =
		( ( float* ) & ( g_bmi160.gyro.scale ) )[1] =
			( ( float* ) & ( g_bmi160.gyro.scale ) )[2] = BMI160_GYRO_PER_LSB;
	( ( float* ) & ( g_bmi160.gyro.scale ) )[3] = BMI160_TEMPERATURE_PER_LSB;

	if( param == 0 ) {
		g_bmi160.gyro.setting.map_x		= DEFAULT_MAP_X;
		g_bmi160.gyro.setting.map_y		= DEFAULT_MAP_Y;
		g_bmi160.gyro.setting.map_z		= DEFAULT_MAP_Z;
		g_bmi160.gyro.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_bmi160.gyro.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_bmi160.gyro.setting.negate_z	= SETTING_DIRECTION_ASSERT;
		//
		g_bmi160.accl.setting.map_x		= DEFAULT_MAP_X;
		g_bmi160.accl.setting.map_y		= DEFAULT_MAP_Y;
		g_bmi160.accl.setting.map_z		= DEFAULT_MAP_Z;
		g_bmi160.accl.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_bmi160.accl.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_bmi160.accl.setting.negate_z	= SETTING_DIRECTION_ASSERT;
	} else {
		EXPAND_MAP(	param,
					g_bmi160.gyro.setting.map_x,	g_bmi160.gyro.setting.map_y,	g_bmi160.gyro.setting.map_z,
					g_bmi160.gyro.setting.negate_x,	g_bmi160.gyro.setting.negate_y,	g_bmi160.gyro.setting.negate_z );
		EXPAND_MAP(	param,
					g_bmi160.accl.setting.map_x,	g_bmi160.accl.setting.map_y,	g_bmi160.accl.setting.map_z,
					g_bmi160.accl.setting.negate_x,	g_bmi160.accl.setting.negate_y,	g_bmi160.accl.setting.negate_z );
	}

	/* recognition */
	// bmi160
	// @@ i2c address search
	for( i = 0 ; i < NELEMENT( add_tbl ) ; i++ ) {
		g_bmi160.addr =  add_tbl[i];
		g_bmi160.device_id = 0;			// read id
		ret = i2c_read( g_bmi160.addr, BMI160_REG_ID, &g_bmi160.device_id, 1 );
		if( ret == I2C_RESULT_SUCCESS ) {
			if( g_bmi160.device_id == BMI160_WHOAMI_ID ) {
				break;
			}
		}
	}

	if( g_bmi160.device_id != BMI160_WHOAMI_ID ) {
		return RESULT_ERR_INIT;
	}
	// @@

	buff      = BMI160_ACCL_RANGE;
	i2c_write( g_bmi160.addr, BMI160_REG_ACC_RANGE, &buff, 1 );

	mdelay( 3 );

	buff      = BMI160_GYRO_RANGE;
	i2c_write( g_bmi160.addr, BMI160_REG_GYR_RANGE, &buff, 1 );

	mdelay( 3 );

	buff = BMI160_ACC_SUSPEND_MODE;
	i2c_write( g_bmi160.addr, BMI160_REG_CMD, &buff, 1 );

	mdelay( 3 );

	buff      = BMI160_GYR_SUSPEND_MODE;
	i2c_write( g_bmi160.addr, BMI160_REG_CMD, &buff, 1 );

	mdelay( 3 );

	//***********************************

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}

void bmi160_ctrl_accl( int f_ena )
{
	unsigned char       buff;

	if( f_ena ) {
		buff      = BMI160_ACC_NOMAL_MODE;
	} else {
		buff      = BMI160_ACC_SUSPEND_MODE;
	}
	i2c_write( g_bmi160.addr, BMI160_REG_CMD, &buff, 1 );
	mdelay( 3 );

}


void bmi160_ctrl_gyro( int f_ena )
{
	unsigned char       buff;
	if( f_ena ) {
		buff      = BMI160_GYR_NOMAL_MODE;
	} else {
		buff      = BMI160_GYR_SUSPEND_MODE;
	}

	i2c_write( g_bmi160.addr, BMI160_REG_CMD, &buff, 1 );

	mdelay( 3 );
}

unsigned int bmi160_rcv_accl( unsigned int tick )
{
	g_bmi160.accl.recv_result = i2c_read( g_bmi160.addr, BMI160_REG_ACC, &g_bmi160.accl.buff[0], sizeof( g_bmi160.accl.buff ) );
	if( g_bmi160.accl.recv_result == 0 ) {
		g_bmi160.accl.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_bmi160.accl.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}
	return 0;
}

unsigned int bmi160_rcv_gyro( unsigned int tick )
{
	i2c_read( g_bmi160.addr, BMI160_REG_TMP, &g_bmi160.temp.buff[0], sizeof( g_bmi160.temp.buff ) );

	g_bmi160.gyro.recv_result = i2c_read( g_bmi160.addr, BMI160_REG_GYR, &g_bmi160.gyro.buff[0], sizeof( g_bmi160.gyro.buff ) );
	if( g_bmi160.gyro.recv_result == 0 ) {
		g_bmi160.gyro.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_bmi160.gyro.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}

	return 0;
}

int bmi160_conv_accl( frizz_fp data[3] )
{
	frizz_fp4w f4w_buff;		// x:0, y:1, z:2
	frizz_fp* fz = ( frizz_fp* )&f4w_buff;
	float* fp = ( float* )&f4w_buff;
	sensor_util_half_t s_buff[4];	// x:0, y:1, z:2
	if( g_bmi160.accl.recv_result != 0 ) {
		data[0] = g_bmi160.accl.lasttime_data[0];
		data[1] = g_bmi160.accl.lasttime_data[1];
		data[2] = g_bmi160.accl.lasttime_data[2];
		return RESULT_SUCCESS_CONV;
	}

	/* data for Accel usingbmi160 */
	// to half @ big endian
	s_buff[0].ubyte[0] = g_bmi160.accl.buff[0];
	s_buff[0].ubyte[1] = g_bmi160.accl.buff[1];
	s_buff[1].ubyte[0] = g_bmi160.accl.buff[2];
	s_buff[1].ubyte[1] = g_bmi160.accl.buff[3];
	s_buff[2].ubyte[0] = g_bmi160.accl.buff[4];
	s_buff[2].ubyte[1] = g_bmi160.accl.buff[5];
	// to 4way float
	// {Dx, Dy, Dz} = {Sx, Sy, Sz} adjust sensor and board angle.
	fp[0] = ( float )( g_bmi160.accl.setting.negate_x ? ( -s_buff[g_bmi160.accl.setting.map_x].half ) : ( s_buff[g_bmi160.accl.setting.map_x].half ) );
	fp[1] = ( float )( g_bmi160.accl.setting.negate_y ? ( -s_buff[g_bmi160.accl.setting.map_y].half ) : ( s_buff[g_bmi160.accl.setting.map_y].half ) );
	fp[2] = ( float )( g_bmi160.accl.setting.negate_z ? ( -s_buff[g_bmi160.accl.setting.map_z].half ) : ( s_buff[g_bmi160.accl.setting.map_z].half ) );

	// convert
	f4w_buff = g_bmi160.accl.scale * f4w_buff;
	data[0] = fz[0];
	data[1] = fz[1];
	data[2] = fz[2];
	//
	g_bmi160.accl.lasttime_data[0] = data[0];
	g_bmi160.accl.lasttime_data[1] = data[1];
	g_bmi160.accl.lasttime_data[2] = data[2];

	return RESULT_SUCCESS_CONV;
}

int bmi160_conv_gyro( frizz_fp data[4] )
{
	frizz_fp4w			f4w_buff;		// x:0, y:1, z:2, t:3
	frizz_fp			*fz = ( frizz_fp* )&f4w_buff;
	float				*fp = ( float* )&f4w_buff;
	sensor_util_half_t	s_buff[4];	// x:0, y:1, z:2, t:3

	if( g_bmi160.gyro.recv_result != 0 ) {
		data[0] = g_bmi160.gyro.lasttime_data[0];
		data[1] = g_bmi160.gyro.lasttime_data[1];
		data[2] = g_bmi160.gyro.lasttime_data[2];
		data[3] = g_bmi160.gyro.lasttime_data[3];
		return RESULT_SUCCESS_CONV;
	}
	/* data for Gyro using BMI160 */
	// to half @ big endian
	s_buff[0].ubyte[0] = g_bmi160.gyro.buff[0];
	s_buff[0].ubyte[1] = g_bmi160.gyro.buff[1];
	s_buff[1].ubyte[0] = g_bmi160.gyro.buff[2];
	s_buff[1].ubyte[1] = g_bmi160.gyro.buff[3];
	s_buff[2].ubyte[0] = g_bmi160.gyro.buff[4];
	s_buff[2].ubyte[1] = g_bmi160.gyro.buff[5];
	s_buff[3].ubyte[0] = g_bmi160.temp.buff[0];
	s_buff[3].ubyte[1] = g_bmi160.temp.buff[1];
	// to 4way float
	// {Dx, Dy, Dz} = {Sx, Sy, Sz}
	fp[0] = ( float )( g_bmi160.gyro.setting.negate_x ? ( -s_buff[g_bmi160.gyro.setting.map_x].half ) : ( s_buff[g_bmi160.gyro.setting.map_x].half ) );
	fp[1] = ( float )( g_bmi160.gyro.setting.negate_y ? ( -s_buff[g_bmi160.gyro.setting.map_y].half ) : ( s_buff[g_bmi160.gyro.setting.map_y].half ) );
	fp[2] = ( float )( g_bmi160.gyro.setting.negate_z ? ( -s_buff[g_bmi160.gyro.setting.map_z].half ) : ( s_buff[g_bmi160.gyro.setting.map_z].half ) );
	fp[3] = ( float )( s_buff[3].half );

	// convert
	f4w_buff = g_bmi160.gyro.scale * f4w_buff;
	data[0] = fz[0];
	data[1] = fz[1];
	data[2] = fz[2];
	data[3] = fz[3] + as_frizz_fp( BMI160_TEMPERATURE_OFFSET );
	//
	g_bmi160.gyro.lasttime_data[0] = data[0];
	g_bmi160.gyro.lasttime_data[1] = data[1];
	g_bmi160.gyro.lasttime_data[2] = data[2];
	g_bmi160.gyro.lasttime_data[3] = data[3];

	return RESULT_SUCCESS_CONV;
}


int bmi160_setparam_accl( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_bmi160.accl.setting.map_x		= setting->map_x;
		g_bmi160.accl.setting.map_y		= setting->map_y;
		g_bmi160.accl.setting.map_z		= setting->map_z;
		g_bmi160.accl.setting.negate_x	= setting->negate_x;
		g_bmi160.accl.setting.negate_y	= setting->negate_y;
		g_bmi160.accl.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

int bmi160_setparam_gyro( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_bmi160.gyro.setting.map_x		= setting->map_x;
		g_bmi160.gyro.setting.map_y		= setting->map_y;
		g_bmi160.gyro.setting.map_z		= setting->map_z;
		g_bmi160.gyro.setting.negate_x	= setting->negate_x;
		g_bmi160.gyro.setting.negate_y	= setting->negate_y;
		g_bmi160.gyro.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

int bmi160_accl_get_condition( void *data )
{
	return g_bmi160.accl.device_condition;
}


int bmi160_gyro_get_condition( void *data )
{
	return g_bmi160.gyro.device_condition;
}

int bmi160_accl_get_raw_data( void *data )
{
	short *temp_buffer = ( short* )data;
	temp_buffer[0] = ( short )( ( g_bmi160.accl.buff[1] << 8 ) | g_bmi160.accl.buff[0] );
	temp_buffer[1] = ( short )( ( g_bmi160.accl.buff[3] << 8 ) | g_bmi160.accl.buff[2] );
	temp_buffer[2] = ( short )( ( g_bmi160.accl.buff[5] << 8 ) | g_bmi160.accl.buff[4] );

	return	RESULT_SUCCESS_SET;
}

unsigned int bmi160_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int bmi160_get_name()
{
	return	D_DRIVER_NAME;
}

