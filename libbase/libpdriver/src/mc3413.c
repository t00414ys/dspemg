/*!******************************************************************************
 * @file    mc3413.c
 * @brief   Mc3413 physical sensor driver
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "timer.h"
#include "mc3413.h"
#include "config_type.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_MC3413

#define		DRIVER_VER_MAJOR		(1)				// Major Version
#define		DRIVER_VER_MINOR		(0)				// Minor Version
#define		DRIVER_VER_DETAIL		(0)				// Detail Version

struct {
	frizz_fp			scale;		// scaler
	unsigned char		buff[6];	// transmission buffer
	unsigned char		addr;
	unsigned char		device_id;
	setting_direction_t	setting;	// direction of device
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[3];
	//
} g_mc3413;


#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char	g_init_done = D_INIT_NONE;

static unsigned char	add_tbl[] = {
	MC3413_I2C_ADDRESS_L,
};

int mc3413_init( unsigned int param )
{
	int ret, i;
	unsigned char buff = 0;
	unsigned char buff_who_check = 0;
	unsigned char buff_read = 0;

	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	/* parameter */
	g_mc3413.scale = as_frizz_fp( MC3413_ACCL_PER_LSB );

	/* setting direction default set */
	if( param == 0 ) {
		g_mc3413.setting.map_x		= DEFAULT_MAP_X;
		g_mc3413.setting.map_y		= DEFAULT_MAP_Y;
		g_mc3413.setting.map_z		= DEFAULT_MAP_Z;
		g_mc3413.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_mc3413.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_mc3413.setting.negate_z	= SETTING_DIRECTION_ASSERT;
	} else {
		EXPAND_MAP(	param,
					g_mc3413.setting.map_x,		g_mc3413.setting.map_y,		g_mc3413.setting.map_z,
					g_mc3413.setting.negate_x,	g_mc3413.setting.negate_y,	g_mc3413.setting.negate_z );
	}

	/* recognition */
	// mc3413
	// @@ i2c address search
	for( i = 0 ; i < NELEMENT( add_tbl ) ; i++ ) {
		g_mc3413.addr =  add_tbl[i];
		g_mc3413.device_id = 0;			// read id
		ret = i2c_read( g_mc3413.addr, MC3413_REG_PCODE, &g_mc3413.device_id, 1 );
		if( ret == I2C_RESULT_SUCCESS ) {
			buff_who_check = g_mc3413.device_id & MC3413_WHOAMI_CHECKID;
			if( buff_who_check == MC3413_WHOAMI_ID ) {
				break;
			}
		}
	}

	buff_who_check = g_mc3413.device_id & MC3413_WHOAMI_CHECKID;
	if( buff_who_check != MC3413_WHOAMI_ID ) {
		return RESULT_ERR_INIT;
	}
	// @@

	/* Sample Rate */
	ret = i2c_read( g_mc3413.addr, MC3413_REG_SRTFR, &buff_read, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}
	buff_read = buff_read & MC3413_SRTFR_RATE_OFF;
	buff = buff_read | MC3413_SRTFR_RATE;
	ret = i2c_write( g_mc3413.addr, MC3413_REG_SRTFR, &buff, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	/* Range Resolution */
	ret = i2c_write( g_mc3413.addr, MC3413_REG_OUTCFG, &buff, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}

void mc3413_ctrl_accl( int f_ena )
{
	unsigned char buff;
	unsigned char buff_read;

	i2c_read( g_mc3413.addr, MC3413_REG_OPCON, &buff_read, 1 );

	if( f_ena ) {
		buff_read = buff_read & MC3413_OPCON_STANDBY;
		buff = buff_read | MC3413_OPCON_WAKE;		// State Standby00 -> Wake01
	} else {
		buff = buff_read & MC3413_OPCON_STANDBY;	// State Wake01 -> Standby00
	}

	i2c_write( g_mc3413.addr, MC3413_REG_OPCON, &buff, 1 );
}

unsigned int mc3413_rcv_accl( unsigned int tick )
{
	unsigned char buff_read = 0;

	g_mc3413.recv_result = i2c_read( g_mc3413.addr, MC3413_REG_SR, &buff_read, 1 );
	if( g_mc3413.recv_result != 0 ) {
		g_mc3413.device_condition |= D_RAW_DEVICE_ERR_READ;
		return 0;
	}

	buff_read = buff_read & MC3413_ACQ_INT;

	if( buff_read == MC3413_ACQ_INT ) {
		// ACQ_INT bit 1
		g_mc3413.recv_result = i2c_read( g_mc3413.addr, MC3413_REG_XYZOUT, g_mc3413.buff, sizeof( g_mc3413.buff ) );

		if( g_mc3413.recv_result == 0 ) {
			g_mc3413.device_condition &= ( ~D_RAW_DEVICE_ERR_READ );
		} else {
			g_mc3413.device_condition |= D_RAW_DEVICE_ERR_READ;
		}
	} else {
		// ACQ_INT bit 0 ( Can not receive Sensor data )
		g_mc3413.recv_result = -1;
	}

	return 0;
}

int mc3413_conv_accl( frizz_fp data[3] )
{
	frizz_fp4w f4w_buff;			// x:0, y:1, z:2
	frizz_fp* fz = ( frizz_fp* )&f4w_buff;
	float* fp = ( float* )&f4w_buff;
	sensor_util_half_t s_buff[3];	// x:0, y:1, z:2

	if( g_mc3413.recv_result != 0 ) {
		data[0] = g_mc3413.lasttime_data[0];
		data[1] = g_mc3413.lasttime_data[1];
		data[2] = g_mc3413.lasttime_data[2];
		return RESULT_SUCCESS_CONV;
	}

	/* data for Accel using mc3413 */
	s_buff[0].ubyte[0] = g_mc3413.buff[0];	// XOUT_EX_L
	s_buff[0].ubyte[1] = g_mc3413.buff[1];	// XOUT_EX_H
	s_buff[1].ubyte[0] = g_mc3413.buff[2];	// YOUT_EX_L
	s_buff[1].ubyte[1] = g_mc3413.buff[3];	// YOUT_EX_H
	s_buff[2].ubyte[0] = g_mc3413.buff[4];	// ZOUT_EX_L
	s_buff[2].ubyte[1] = g_mc3413.buff[5];	// ZOUT_EX_H

	// to 4way float
	// {Dx, Dy, Dz} = {Sx, Sy, Sz} adjust sensor and board angle.
	fp[0] = ( float )( g_mc3413.setting.negate_x ? ( -s_buff[g_mc3413.setting.map_x].half ) : ( s_buff[g_mc3413.setting.map_x].half ) );
	fp[1] = ( float )( g_mc3413.setting.negate_y ? ( -s_buff[g_mc3413.setting.map_y].half ) : ( s_buff[g_mc3413.setting.map_y].half ) );
	fp[2] = ( float )( g_mc3413.setting.negate_z ? ( -s_buff[g_mc3413.setting.map_z].half ) : ( s_buff[g_mc3413.setting.map_z].half ) );

	// convert
	f4w_buff = g_mc3413.scale * f4w_buff;
	data[0] = fz[0];
	data[1] = fz[1];
	data[2] = fz[2];

	g_mc3413.lasttime_data[0] = data[0];
	g_mc3413.lasttime_data[1] = data[1];
	g_mc3413.lasttime_data[2] = data[2];

	return RESULT_SUCCESS_CONV;
}

int mc3413_setparam_accl( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_mc3413.setting.map_x		= setting->map_x;
		g_mc3413.setting.map_y		= setting->map_y;
		g_mc3413.setting.map_z		= setting->map_z;
		g_mc3413.setting.negate_x	= setting->negate_x;
		g_mc3413.setting.negate_y	= setting->negate_y;
		g_mc3413.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

int mc3413_get_condition( void *data )
{
	return g_mc3413.device_condition;
}

int mc3413_get_raw_data( void *data )
{
	short *temp_buffer = ( short* )data;
	temp_buffer[0] = ( short )( ( g_mc3413.buff[1] << 8 ) | g_mc3413.buff[0] );
	temp_buffer[1] = ( short )( ( g_mc3413.buff[3] << 8 ) | g_mc3413.buff[2] );
	temp_buffer[2] = ( short )( ( g_mc3413.buff[5] << 8 ) | g_mc3413.buff[4] );

	return	RESULT_SUCCESS_SET;
}

unsigned int mc3413_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int mc3413_get_name()
{
	return	D_DRIVER_NAME;
}


