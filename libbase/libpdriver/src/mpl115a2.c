/*!******************************************************************************
 * @file    mpl115a2.c
 * @brief   mpl115a2 sensor driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "mpl115a2.h"
#include "config_type.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_MPL115A2


#define		DRIVER_VER_MAJOR		(1)						// Major Version
#define		DRIVER_VER_MINOR		(1)						// Minor Version
#define		DRIVER_VER_DETAIL		(0)						// Detail Version


#define PRES_DELAY		5


struct {
	frizz_fp			a0;			// coefficient a0
	frizz_fp			b1;			// coefficient b1
	frizz_fp			b2;			// coefficient b2
	frizz_fp			c12;		// coefficient c12
	frizz_fp			coeff_mul;	// coefficient mul
	frizz_fp			coeff_add;	// coefficient add
	unsigned char		buff[4];	// transmission buffer
	unsigned char		conv;
	unsigned char		f_conv;
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[1];
	//
} g_mpl115a2;

#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char		g_init_done = D_INIT_NONE;

int mpl115a2_init( unsigned int param )
{
	int ret;
	sensor_util_half_t s_buff;
	unsigned char buff[8];

	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	/* parameter */
	g_mpl115a2.conv = 0;
	g_mpl115a2.coeff_mul = as_frizz_fp( MPL115A2_COEFF_MUL );
	g_mpl115a2.coeff_add = as_frizz_fp( MPL115A2_COEFF_ADD );

	/* recognition & get Coeff */
	// MPL115A2

	ret = i2c_read( MPL115A2_I2C_ADDRESS, MPL115A2_A0_MSB , &buff[0], sizeof( buff ) );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	// a0: exp = -3
	s_buff.ubyte[0] = buff[1];
	s_buff.ubyte[1] = buff[0];
	g_mpl115a2.a0 = as_frizz_fp( s_buff.half );
	g_mpl115a2.a0 *= as_frizz_fp( MPL115A2_SCALE_A0 );
	// b1: exp = -13
	s_buff.ubyte[0] = buff[3];
	s_buff.ubyte[1] = buff[2];
	g_mpl115a2.b1 = as_frizz_fp( s_buff.half );
	g_mpl115a2.b1 *= as_frizz_fp( MPL115A2_SCALE_B1 );
	// b2: exp = -14
	s_buff.ubyte[0] = buff[5];
	s_buff.ubyte[1] = buff[4];
	g_mpl115a2.b2 = as_frizz_fp( s_buff.half );
	g_mpl115a2.b2 *= as_frizz_fp( MPL115A2_SCALE_B2 );
	// c12: exp = -13 -9
	s_buff.ubyte[0] = buff[7];
	s_buff.ubyte[1] = buff[6];
	g_mpl115a2.c12 = as_frizz_fp( s_buff.half >> 2 );
	g_mpl115a2.c12 *= as_frizz_fp( MPL115A2_SCALE_C12 );

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}

void mpl115a2_ctrl( int f_ena )
{
	g_mpl115a2.f_conv = 0;
}

unsigned int mpl115a2_rcv( unsigned int tick )
{

	unsigned int remain;

	if( g_mpl115a2.f_conv == 0 ) {
		i2c_write( MPL115A2_I2C_ADDRESS, MPL115A2_CONVERT , &g_mpl115a2.conv, 1 );

		g_mpl115a2.f_conv = 1;
		remain = PRES_DELAY;
	} else {
		g_mpl115a2.recv_result = i2c_read( MPL115A2_I2C_ADDRESS, MPL115A2_P_MSB , &g_mpl115a2.buff[0], sizeof( g_mpl115a2.buff ) );
		if( g_mpl115a2.recv_result == 0 ) {
			g_mpl115a2.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
		} else {
			g_mpl115a2.device_condition  |= D_RAW_DEVICE_ERR_READ;
		}

		g_mpl115a2.f_conv = 0;
		remain = tick - PRES_DELAY;
	}



	return remain;
}

int mpl115a2_conv( frizz_fp data[1] )
{
	frizz_fp p, t, d;
	sensor_util_half_t s_buff;

	if( g_mpl115a2.recv_result != 0 ) {
		data[0] = g_mpl115a2.lasttime_data[0];
		return RESULT_SUCCESS_CONV;
	}

	// Convert
	s_buff.ubyte[0] = g_mpl115a2.buff[1];
	s_buff.ubyte[1] = g_mpl115a2.buff[0];
	p = as_frizz_fp( s_buff.uh >> 6 );
	s_buff.ubyte[0] = g_mpl115a2.buff[3];
	s_buff.ubyte[1] = g_mpl115a2.buff[2];
	t = as_frizz_fp( s_buff.uh >> 6 );
	d = g_mpl115a2.a0 + ( g_mpl115a2.b1 + g_mpl115a2.c12 * t ) * p + g_mpl115a2.b2 * t;
	d = d * g_mpl115a2.coeff_mul + g_mpl115a2.coeff_add;
	data[0] = d;
	//
	g_mpl115a2.lasttime_data[0] = data[0];
	return RESULT_SUCCESS_CONV;
}


int mpl115a2_get_condition( void *data )
{
	return g_mpl115a2.device_condition;
}


int mpl115a2_setparam( void *ptr )
{
	return RESULT_ERR_SET;
}

unsigned int mpl115a2_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int mpl115a2_get_name()
{
	return	D_DRIVER_NAME;
}

