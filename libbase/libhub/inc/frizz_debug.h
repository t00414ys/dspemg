/*!******************************************************************************
 * @file frizz_debug.h
 * @brief frizz debug routine Interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __FRIZZ_DEBUG_H__
#define __FRIZZ_DEBUG_H__

/**
 * @name Frizz Debug Status
 * @{ */
#define FRIZZ_NOMSG			(0)			///< no message
#define FRIZZ_EMERG			(1)			///< system is unusable
#define FRIZZ_ERR			(2)			///< error conditions
#define FRIZZ_WARNING		(3)			///< warning conditions
#define FRIZZ_INFO			(4)			///< informational
#define FRIZZ_DEBUG			(5)			///< debug-level messages
/**  @} */


/**
 * @name Frizz Debug Data Package Element Size
 * @{ */
#define FRIZZ_DEBUG_ELEMENT	(4)			///< element size
/**  @} */


/**
 *  @struct sFrizzDEBUG
 *  @brief frizz debug data package(default)
 */
typedef struct {
	unsigned long  data[FRIZZ_DEBUG_ELEMENT];	///< debug send data
} sFrizzDEBUG;

#define POS_MACRO(SenId, File)	(unsigned long)(( ((SenId) & 0xff)  << 24) | ((File) << 16) | __LINE__)

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief frizz host debug data transmit interface.<BR>
 *        This function is Without free will to FIFO, do not send.
 *
 * @param [in] status		message status (FRIZZ_NOMSG ~ FRIZZ_DEBUG)
 * @param [in] pos			position Information
 * @param [in] buff			send data pointer
 * @param [in] length		buffer length
 * @retval	0	Success
 * @retval	-1	FIFO full
 *
 * @note ex).<BR>
 *        #include "frizz_debug.h"
 *        #define POS_GESTURE_CORE POS_MACRO(SENSOR_ID_GESTURE, 0x01);  => Unique number of this file <BR>
 *                              :                                                                     <BR>
 *        static int calculate(void)                                                                  <BR>
 *        {                                                                                           <BR>
 *          short        count,sum;                                                                   <BR>
 *          sFrizzDEBUG  data;                                                                        <BR>
 *                              :                                                                     <BR>
 *                              :                                                                     <BR>
 *          data.data[0] = 0x11111111;                                                                <BR>
 *          data.data[1] = (count << 16) | sum;                                                       <BR>
 *          data.data[2] = 0x12345678;                                                                <BR>
 *          data.data[3] = 0x00000000;                                                                <BR>
 *          frizz_debug(FRIZZ_WARNING,POS_GESTURE_CORE,&data,sizeof(data));                           <BR>
 */
int frizz_debug( unsigned char status, unsigned long pos, sFrizzDEBUG *data );

/**
 * @brief set frizz host debug level.<BR>
 *
 * @param [in] sen_id		sensor id
 * @param [in] level		debug level
 * @retval	void
 *
 */
void frizz_debug_level_set( unsigned char sen_id, unsigned char level );


/**
 * @brief get frizz host debug level.<BR>
 *
 * @param [in] sen_id		sensor id
 * @retval	debug level
 */
unsigned char frizz_debug_level_get( unsigned char sen_id );

#ifdef __cplusplus
}
#endif


#endif // __FRIZZ_DEBUG_H__

