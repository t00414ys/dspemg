/*!******************************************************************************
 * @file  frizz_type.h
 * @brief
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __FRIZZ_TYPE_H__
#define __FRIZZ_TYPE_H__

#include "frizz_fp.h"
#include "frizz_fp4w.h"

#if !defined(RUN_ON_PC)
#define RUN_ON_FRIZZ
#endif

#endif // __FRIZZ_TYPE_H__
