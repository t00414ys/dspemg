/*!******************************************************************************
 * @file  frizz_math.h
 * @brief For Trigonometric and Elementary function
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __FRIZZ_MATH_H__
#define __FRIZZ_MATH_H__
#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief make power
 */
#define frizz_pow2(_X_)	((_X_)*(_X_))

/**
 * @brief for index of constant value
 */
enum FrizzMathConstIdx {
	FRIZZ_MATH_CONST_IDX_PI = 0,		///< pi
	FRIZZ_MATH_CONST_IDX_PI_x2,		///< pix2
	FRIZZ_MATH_CONST_IDX_PI_HALF,	///< pi/2
	FRIZZ_MATH_CONST_IDX_PI_QUAT,	///< pi/4
	FRIZZ_MATH_CONST_IDX_PI_INV,	///< 1/pi
	FRIZZ_MATH_CONST_IDX_DEG2RAD,	///< deg -> pi
	FRIZZ_MATH_CONST_IDX_RAD2DEG,	///< pi -> deg
	FRIZZ_MATH_CONST_IDX_TWO25,		///< 2^25
};
extern const frizz_fp *frizz_math_const;

#define FRIZZ_MATH_PI		frizz_math_const[FRIZZ_MATH_CONST_IDX_PI]		///< =Pi
#define FRIZZ_MATH_PI_x2	frizz_math_const[FRIZZ_MATH_CONST_IDX_PI_x2]	///< =2Pi
#define FRIZZ_MATH_PI_HALF	frizz_math_const[FRIZZ_MATH_CONST_IDX_PI_HALF]	///< =0.5Pi
#define FRIZZ_MATH_PI_QUAT	frizz_math_const[FRIZZ_MATH_CONST_IDX_PI_QUAT]	///< =0.25Pi
#define FRIZZ_MATH_PI_INV	frizz_math_const[FRIZZ_MATH_CONST_IDX_PI_INV]	///< =1/Pi
#define FRIZZ_MATH_DEG2RAD	frizz_math_const[FRIZZ_MATH_CONST_IDX_DEG2RAD]	///< =Pi/180.0 
#define FRIZZ_MATH_RAD2DEG	frizz_math_const[FRIZZ_MATH_CONST_IDX_RAD2DEG]	///< =180/Pi
#define FRIZZ_MATH_TWO25	frizz_math_const[FRIZZ_MATH_CONST_IDX_TWO25]	///< =2^25


/**
 * @brief absolute value
 *
 * @param [in] in
 *
 * @return absolute value of in
 */
frizz_fp frizz_fabs( frizz_fp in );

/**
 * @brief square root
 *
 * @param [in] in
 *
 * @return value of square root of in
 */
frizz_fp frizz_sqrt( frizz_fp in );

/**
 * @brief sine
 *
 * @param [in] rad angle in radian
 *
 * @return sine of rad
 */
frizz_fp frizz_cos( frizz_fp rad );

/**
 * @brief cosine
 *
 * @param [in] rad angle in radian
 *
 * @return cosine of rad
 */
frizz_fp frizz_sin( frizz_fp rad );

/**
 * @brief arc sine
 *
 * @param [in] x [-1, +1]
 *
 * @return arc sine of x in radian, [-pi/2, +pi/2]
 */
frizz_fp frizz_asin( frizz_fp x );

/**
 * @brief arc cosine
 *
 * @param [in] x [-1, +1]
 *
 * @return arc cosine of x in radian, [0, pi]
 */
frizz_fp frizz_acos( frizz_fp x );

/**
 * @brief arc tangent
 *
 * @param [in] a
 *
 * @return arc tangent of a in radian, [-pi/2, +pi/2]
 */
frizz_fp frizz_atan( frizz_fp a );

/**
 * @brief arc tangent with two parameters
 *
 * @param [in] y
 * @param [in] x
 *
 * @return arc tangent of y/x in radian, [-pi, +pi]
 */
frizz_fp frizz_atan2( frizz_fp y, frizz_fp x );

/**
 * @brief truncate range to [0, +2pi]
 *
 * @param [in] rad in radian
 *
 * @return truncated radian, [0, +2pi]
 */
frizz_fp frizz_0_2pi_range( frizz_fp rad );

/**
 * @brief truncate range to [-pi, +pi]
 *
 * @param [in] rad in radian
 *
 * @return truncated radian, [-pi, +pi]
 */
frizz_fp frizz_pi_pi_range( frizz_fp rad );

/**
 * @brief divide opreation
 *
 * @param [in] xin
 * @param [in] yin
 *
 * @return value of xin/yin
 */
frizz_fp frizz_div( frizz_fp xin, frizz_fp yin );

/**
 * @brief get significand and exponent
 * x = significand * 2^exp
 *
 * @param [in] x value
 * @param [out] exp exponent
 *
 * @return significand value [0.5, 1.0)
 */
frizz_fp frizz_frexp( frizz_fp x, int *exp );

/**
 * @brief natural logarithm
 *
 * @param [in] x
 *
 * @return natural logarithm of x
 */
frizz_fp frizz_log( frizz_fp x );

/**
 * @brief Fast inverse sqrt
 *
 * @param [in] x
 *
 * @return natural 1/sqrt(x)
 */
frizz_fp frizz_inv_sqrt( frizz_fp x );

#ifdef __cplusplus
}
#endif
#endif//__FRIZZ_MATH_H__
