/*!******************************************************************************
 * @file kf_info.h
 * @brief struct for Kalman Filter
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "kalman_filter.h"
#include "matrix.h"

///	structure for Kalman Filter
typedef struct {
	KF_Callback	cb;	//!< structure for callback

	Matrix *stv;	//!< vector (matrix) of states
	Matrix *prd;	//!< vector (matrix) of predicted states at stage of prediction

	Matrix *mes;	//!< vector (matrix) of measurement
	Matrix *res;	//!< vector (matrix) of residual between measurements and predictons

	Matrix *con;	//!< vector (matrix) of control-input

	Matrix *PHI_m;	//!< Phi: Transition matrix
	Matrix *G_m;	//!< G: Control-input transition matrix
	Matrix *H_m;	//!< H: Measurement matrix

	Matrix *M_m;	//!< convariance matrix representing errors in the state estimates before an update
	Matrix *K_m;	//!< Kalman gain matrix

	Matrix *P_m;	//!< convariance matrix representing errors in the state estimates
	Matrix *Q_m;	//!< discrete process noise matrix
	Matrix *R_m;	//!< the variance of each of the measurement noise source
	Matrix *S_m;	//!< covarriance for control vector

	Matrix *I_m;	//!< identity matrix


	/// \name temporary
	/// @{
	Matrix	*S_0;
	Matrix	*SS_0;
	Matrix	*SS_1;
	Matrix	*SM_0;
	Matrix	*MM_0;
	Matrix	*MM_1;
	Matrix	*MM_2;
	Matrix	*SC_0;
	/// @}

} KF_t;

