/*!******************************************************************************
 * @file matrix.c
 * @brief matrix operation library
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
/*
	necessary include
	<stdio.h> \n
	<stdlib.h> \n
	<MATRIX_ASSERT.h> \n
*/
#include "matrix.h"
#include "frizz_const.h"
#include "frizz_math.h"
#include "frizz_mem.h"

#include "frizz_type.h"
#include "emutie.h"

#ifndef RUN_ON_PC
#define FILE int
#define fgets(...) NULL
#define fopen(...) NULL
#define fclose(...)
#define sscanf(...)
#define printf(...)
#define fprintf(...)
#define exit(x)
#else
#include <stdio.h>
#endif // UNUSE_NEWLIB

#ifdef FRIZZ_VALID_INLINE
#include "emutie.c"
#endif

#define MATRIX_ASSERT(_PAT_)

/**
 * @brief create matrix
 *
 * @param [in] rows num of rows
 * @param [in] cols num of columns
 *
 * @return matrix object
 */
Matrix *alloc_matrix( int rows, int cols )
{
	Matrix *mtx;
	int ii;
	frizz_fp zero = FRIZZ_CONST_ZERO;

	mtx = ( Matrix * )frizz_malloc( sizeof( Matrix ) );
	// rows
	mtx->rows = rows;
	mtx->vec = ( frizz_fp4w** )frizz_malloc( mtx->rows * sizeof( frizz_fp4w* ) );
	// cols
	mtx->cols = cols;
	mtx->blks = ( cols + MATRIX_ALIGN_NUM - 1 ) >> MATRIX_ALIGN_NUM_SHIFT;
	// create colmns
	for( ii = 0; ii < mtx->rows; ii++ ) {
		mtx->vec[ii] = ( frizz_fp4w* )frizz_malloc( mtx->blks * sizeof( frizz_fp4w ) );
		frizz_tie_store_const( zero, mtx->vec[ii], mtx->blks );
	}
	// return
	return mtx;
}

/**
 * @brief delete matrix
 *
 * @param [in] mtx matrix object
 */
void free_matrix( Matrix *mtx )
{
	int ii;
	for( ii = 0; ii < mtx->rows; ii++ ) {
		frizz_free( mtx->vec[ii] );
	}
	frizz_free( mtx->vec );
	frizz_free( mtx );
}

/**
 * @brief print element of matrix for Debug
 *
 * @param [in] mtx matrix object
 * @param [out] pointer to print strings
 */
#ifdef RUN_ON_PC
void print_matrix( Matrix *mtx, const char *str )
{
	int ii, jj;
	printf( "# %s\n", str );
	for( ii = 0; ii < mtx->rows; ii++ ) {
		printf( "# { " );
		for( jj = 0; jj < mtx->cols; jj++ ) {
			printf( "%8.10le, ", ( float )mtx->data[ii][jj] );
			//printf("%08x, ",*(unsigned int *)(&(mtx->data[ii][jj])));
		}
		printf( " }\n" );
	}

}
#else
void print_matrix( Matrix *mtx, const char *str ) {}
#endif

/**
 * @brief setting element of matrix
 *
 * @param [out] mtx matrix object
 * @param [in] src data to set
 */
void set_matrix( Matrix *mtx, const frizz_fp *src )
{
	int ii, jj;
	for( ii = 0; ii < mtx->rows; ii++ ) {
		for( jj = 0; jj < mtx->cols; jj++ ) {
			mtx->data[ii][jj] = *src;
			src++;
		}
	}
}

/**
 * @brief set as identity matrix
 *
 * @param [out] mtx
 *
 * @note please be sure that input is square matrix
 */
void set_matrix_identity( Matrix *mtx )
{
	int ii;
	frizz_fp zero = FRIZZ_CONST_ZERO;
	frizz_fp one = FRIZZ_CONST_ONE;
	for( ii = 0; ii < mtx->rows; ii++ ) {
		frizz_tie_store_const( zero, mtx->vec[ii], mtx->blks );
		mtx->data[ii][ii] = one;
	}
}

/**
 * @brief set element of matrix at diag
 *
 * @param [in,out] mtx matrix object
 * @param [in] num num of entry
 * @param [in] entry data tot set element
 */
void set_matrix_diag( Matrix *mtx, int num, const frizz_fp *entry )
{
	int ii;
	for( ii = 0; ii < num; ii++ ) {
		mtx->data[ii][ii] = entry[ii];
	}
}

/**
 * @brief copy matrix
 *
 * @param [in] src
 * @param [out] dst
 */
void copy_matrix( Matrix *src, Matrix *dst )
{
	int ii;
	MATRIX_ASSERT( src->rows == dst->rows );
	MATRIX_ASSERT( src->cols == dst->cols );
	for( ii = 0; ii < dst->rows; ii++ ) {
		frizz_tie_ppmov( src->vec[ii], dst->vec[ii], dst->blks );
	}
}

/**
 * @brief matrix add
 * dst = src0 + src1
 *
 * @param [in] src0
 * @param [in] src1
 * @param [out] dst
 *
 * @note please be sure that parameters are correct combination of rows/cols.
 */
void add_matrix( Matrix *src0, Matrix *src1, Matrix *dst )
{
	int ii;
	MATRIX_ASSERT( src0->rows == dst->rows );
	MATRIX_ASSERT( src1->rows == dst->rows );
	MATRIX_ASSERT( src0->cols == dst->cols );
	MATRIX_ASSERT( src1->cols == dst->cols );
	for( ii = 0; ii < dst->rows; ii++ ) {
		frizz_tie_ppadd( src0->vec[ii], src1->vec[ii], dst->vec[ii], dst->blks );
	}
}

/**
 * @brief matrix sub
 * dst = src0 - src1
 *
 * @param [in] src0
 * @param [in] src1
 * @param [out] dst
 *
 * @note please be sure that parameters are correct combination of rows/cols.
 */
void sub_matrix( Matrix *src0, Matrix *src1, Matrix *dst )
{
	int ii;
	MATRIX_ASSERT( src0->rows == dst->rows );
	MATRIX_ASSERT( src1->rows == dst->rows );
	MATRIX_ASSERT( src0->cols == dst->cols );
	MATRIX_ASSERT( src1->cols == dst->cols );
	for( ii = 0; ii < dst->rows; ii++ ) {
		frizz_tie_ppsub( src0->vec[ii], src1->vec[ii], dst->vec[ii], dst->blks );
	}
}

/**
 * @brief matrix mul
 * dst = src0 * src1
 *
 * @param [in] src0
 * @param [in] src1
 * @param [out] dst
 *
 * @note please be sure that parameters are correct combination of rows/cols.
 * @note please don't set same Matrix object between input and output.
 */
void mul_matrix( Matrix *src0, Matrix *src1, Matrix *dst )
{
	int ii, jj;
	MATRIX_ASSERT( src0->rows == dst->rows );
	MATRIX_ASSERT( src1->cols == dst->cols );
	MATRIX_ASSERT( src0->cols == src1->rows );
	for( ii = 0; ii < src0->rows; ii++ ) {
		jj = 0;
		frizz_tie_spmul( src0->data[ii][jj], src1->vec[jj], dst->vec[ii], src1->blks );
		jj++;
		for( ; jj < src0->cols; jj++ ) {
			frizz_tie_spmul_ppadd( src0->data[ii][jj], src1->vec[jj], dst->vec[ii], src1->blks );
		}
	}
}

/**
 * @brief matrix mul with transpose
 * dst = src0 * src1^T
 *
 * @param [in] src0
 * @param [in] src1
 * @param [out] dst
 *
 * @note please be sure that parameters are correct combination of rows/cols.
 * @note please don't set same Matrix object between input and output.
 */
void mul_trmatrix( Matrix *src0, Matrix *src1, Matrix *dst )
{
	int ii, jj;
	MATRIX_ASSERT( src0->cols == src1->cols );
	MATRIX_ASSERT( src0->rows == dst->rows );
	MATRIX_ASSERT( src1->rows == dst->cols );
	for( ii = 0; ii < dst->rows; ii++ ) {
		for( jj = 0; jj < dst->cols; jj++ ) {
			frizz_tie_ppmul_pdadd( src0->vec[ii], src1->vec[jj], &( dst->data[ii][jj] ), src0->blks );
		}
	}
}

/**
 * @brief matrix mul with transpose
 * dst = src0^T * src1
 *
 * @param [in] src0
 * @param [in] src1
 * @param [out] dst
 *
 * @note please be sure that parameters are correct combination of rows/cols.
 * @note please don't set same Matrix object between input and output.
 */
void mul_trmatrix2( Matrix *src0, Matrix *src1, Matrix *dst )
{
	int ii, jj;
	MATRIX_ASSERT( src0->cols == src1->cols );
	MATRIX_ASSERT( src0->rows == dst->rows );
	MATRIX_ASSERT( src1->rows == dst->cols );
	for( ii = 0; ii < src0->cols; ii++ ) {
		jj = 0;
		frizz_tie_spmul( src0->data[jj][ii], src1->vec[jj], dst->vec[ii], src1->blks );
		jj++;
		for( ; jj < src0->rows; jj++ ) {
			frizz_tie_spmul_ppadd( src0->data[jj][ii], src1->vec[jj], dst->vec[ii], src1->blks );
		}
	}
}

/**
 * @brief transpose matrix
 * dst = src^T
 *
 * @param [in] src
 * @param [out] dst
 *
 * @note please be sure that parameters are correct combination of rows/cols.
 * @note please don't set same Matrix object between input and output.
 */
void transpose_matrix( Matrix *src, Matrix *dst )
{
	int ii, jj;
	MATRIX_ASSERT( src->rows == dst->cols );
	MATRIX_ASSERT( src->cols == dst->rows );
	for( ii = 0; ii < src->rows; ii++ ) {
		for( jj = 0; jj < src->cols; jj++ ) {
			dst->data[jj][ii] = src->data[ii][jj];
		}
	}
}

/**
 * @brief scale matrix
 * mtx = scalar * matx
 *
 * @param [in,out] mtx
 * @param [in] scalar
 */
void scale_matrix( Matrix *mtx, frizz_fp scalar )
{
	int ii;
	int rows = mtx->rows;
	int dup = mtx->blks;
	for( ii = 0; ii < rows; ii++ ) {
		frizz_tie_spmul( scalar, mtx->vec[ii], mtx->vec[ii], dup );
	}
}

/**
 * @brief invert matrix
 * inv = src^(-1)
 *
 * @param [in] src
 * @param [out] inv
 * @param [out] mtx rows/cols are same src matrix
 *
 * @return 0; success, -1: no answer
 *
 * @note please be sure that parameters are correct combination of rows/cols.
 * @note please don't set same Matrix object between input and output.
 */
int invert_matrix( Matrix *src, Matrix *inv, Matrix *mtx )
{
#define SWAP(type, s0, s1) {	\
	type tmp = s0;				\
	s0 = s1;	s1 = tmp;		\
}
	int ii, jj;
	frizz_fp zero = FRIZZ_CONST_ZERO;
	frizz_fp one = FRIZZ_CONST_ONE;
	MATRIX_ASSERT( src->rows == inv->rows );
	MATRIX_ASSERT( src->cols == inv->cols );
	MATRIX_ASSERT( src->rows == src->cols );

	set_matrix_identity( inv );
	if( src != mtx ) {
		copy_matrix( src, mtx );
	}
	for( ii = 0; ii < mtx->rows; ii++ ) {
		if( mtx->data[ii][ii] == zero ) {
			for( jj = ii + 1; jj < mtx->rows; jj++ ) {
				if( mtx->data[jj][ii] != zero ) {
					SWAP( frizz_fp*, mtx->data[ii], mtx->data[jj] );
					SWAP( frizz_fp*, inv->data[ii], inv->data[jj] );
					break;
				}
			}
		}
		if( mtx->data[ii][ii] == zero ) {
			return -1;
		}
		// to Unit
		frizz_fp div = frizz_div( one, mtx->data[ii][ii] );
		//frizz_fp div = 1 / mtx->data[ii][ii];
		frizz_tie_spmul( div, mtx->vec[ii], mtx->vec[ii], mtx->blks );
		frizz_tie_spmul( div, inv->vec[ii], inv->vec[ii], inv->blks );
		for( jj = 0; jj < mtx->rows; jj++ ) {
			if( jj != ii ) {
				frizz_fp scl = mtx->data[jj][ii];
				frizz_tie_spmul_ppsub( scl, mtx->vec[ii], mtx->vec[jj], mtx->blks );
				frizz_tie_spmul_ppsub( scl, inv->vec[ii], inv->vec[jj], mtx->blks );
			}
		}
	}
	return 0;
#undef SWAP
}

/**
 * @brief check wheter or not zero matrix
 *
 * @param [in] mtx
 *
 * @return 0: OK, -1:NG
 *
 * @note checking with 1.0e-5 as zero
 */
int chk_matrix( Matrix *mtx )
{
	int ii, jj;
	frizz_fp det;
	frizz_fp zero = FRIZZ_CONST_ZERO;
	frizz_fp one = FRIZZ_CONST_ONE;
	frizz_fp thr = as_frizz_fp( 1.0e-5 );

	for( ii = 0; ii < mtx->rows; ii++ ) {
		for( jj = 0; jj < mtx->cols; jj++ ) {
			det = mtx->data[ii][jj] - ( ( ii == jj ) ? one : zero );
			if( thr < frizz_fabs( det ) ) {
				return -1;
			}
		}
	}

	return 0;
}

/**
 * @brief dot product of vector
 * result = vec0 * vec1
 *
 * @param [in] vec0
 * @param [in] vec1
 *
 * @return result
 *
 * @note please be sure that parameters are correct combination of rows/cols.
 */
frizz_fp inner_product( Matrix *vec0, Matrix *vec1 )
{
	int ii;
	frizz_fp iprod = FRIZZ_CONST_ZERO;
	MATRIX_ASSERT( vec0->rows == vec1->rows );
	MATRIX_ASSERT( vec0->cols == 1 );
	MATRIX_ASSERT( vec1->cols == 1 );

	for( ii = 0; ii < vec0->rows; ii++ ) {
		iprod += vec0->data[ii][0] * vec1->data[ii][0];
	}
	return iprod;
}

/**
 * @brief cross product of 3-Axis vector
 * dst = vec0 x vec1
 *
 * @param [in] vec0
 * @param [in] vec1
 * @param [out] dst
 *
 * @note please be sure that input of rows are 3 and cols are 1.
 * @note please don't set same Matrix object between input and output.
 */
void vector_product( Matrix *vec0, Matrix *vec1, Matrix *dst )
{
	frizz_fp **v0;
	frizz_fp **v1;
	frizz_fp **dt;
	MATRIX_ASSERT( vec0->rows == vec1->rows );
	MATRIX_ASSERT( vec0->rows == dst->rows );
	MATRIX_ASSERT( vec0->rows == 3 );
	MATRIX_ASSERT( vec0->cols == 1 );
	MATRIX_ASSERT( vec1->cols == 1 );
	MATRIX_ASSERT( dst->cols == 1 );

	v0 = vec0->data;
	v1 = vec1->data;
	dt = dst->data;

	dt[0][0] = v0[1][0] * v1[2][0] - v0[2][0] * v1[1][0];
	dt[1][0] = v0[2][0] * v1[0][0] - v0[0][0] * v1[2][0];
	dt[2][0] = v0[0][0] * v1[1][0] - v0[1][0] * v1[0][0];
}

static frizz_fp get_max_tri_area( Matrix *mtx, int *row, int *col )
{
	frizz_fp max = FRIZZ_CONST_ZERO;
	int i, j;

	*row = *col = 0;
	for( i = 0; i < mtx->rows; i++ ) {
		for( j = i + 1; j < mtx->cols; j++ ) {
			frizz_fp val = frizz_fabs( mtx->data[i][j] );
			if( max < val ) {
				max = val;
				*row = i;
				*col = j;
			}
		}
	}
	return max;
}

static const float _eigen_const[] = {
	1.0e-3,
};
static const frizz_fp *eigen_const = ( const frizz_fp* )_eigen_const;

/**
 * @brief calculate eigenvector at specialization symmetric matx
 * sym * eig_vec = eig_vec * eig_val
 *
 * @param [in] sym symmetric matrix
 * @param [out] eig_vec eigenvector in each column (3x3 matrix)
 * @param [out] eig_val eigenvalue in each diag (3x3 matrix)
 *
 * @note sym * eig_vec[i] = eig_val[i][i] * eig_vec[i]
 * @note please don't set same Matrix object between input and output.
 */
void eigen_matrix( Matrix *sym, Matrix *eig_vec, Matrix *eig_val )
{
	frizz_fp rr, cc, rc;
	frizz_fp s, c, alpha, beta, gamma, temp;
	int row, col, i;
	// setting
	copy_matrix( sym, eig_val );
	set_matrix_identity( eig_vec );
	// loop
	while( eigen_const[0] <= get_max_tri_area( eig_val, &row, &col ) ) {
		rr = eig_val->data[row][row];
		rc = eig_val->data[row][col];
		cc = eig_val->data[col][col];

		alpha = FRIZZ_CONST_HALF * ( rr - cc );
		beta = -rc;
		gamma = frizz_div( frizz_fabs( alpha ),  frizz_sqrt( alpha * alpha + beta * beta ) );

		s = frizz_sqrt( FRIZZ_CONST_HALF * ( FRIZZ_CONST_ONE - gamma ) );
		c = frizz_sqrt( FRIZZ_CONST_HALF * ( FRIZZ_CONST_ONE + gamma ) );
		if( alpha * beta < FRIZZ_CONST_ZERO ) {
			s = -s;
		}

		// P^T * A
		for( i = 0; i < sym->cols; i++ ) {
			temp =					c * eig_val->data[row][i] - s * eig_val->data[col][i];
			eig_val->data[col][i] =	s * eig_val->data[row][i] + c * eig_val->data[col][i];
			eig_val->data[row][i] =	temp;
		}

		// P^T * A * P
		for( i = 0; i < sym->rows; i++ ) {
			eig_val->data[i][row] = eig_val->data[row][i];
			eig_val->data[i][col] = eig_val->data[col][i];
		}

		eig_val->data[row][row] = c * c * rr + s * s * cc - FRIZZ_CONST_TWO * s * c * rc;
		eig_val->data[row][col] =
			eig_val->data[col][row] = s * c * ( rr - cc ) + ( c * c - s * s ) * rc;
		eig_val->data[col][col] = s * s * rr + c * c * cc + FRIZZ_CONST_TWO * s * c * rc;

		// P * P'
		for( i = 0; i < sym->rows; i++ ) {
			temp =					c * eig_vec->data[i][row] - s * eig_vec->data[i][col];
			eig_vec->data[i][col] =	s * eig_vec->data[i][row] + c * eig_vec->data[i][col];
			eig_vec->data[i][row] =	temp;
		}
	}
}

#ifdef DEBUG_MATRIX
int main( void )
{
	Matrix	*src0;
	Matrix	*src1;
	Matrix	*dist;

	src0 = alloc_matrix( 2, 2 );
	src1 = alloc_matrix( 2, 2 );
	dist = alloc_matrix( 2, 2 );

	src0->data[0][0] = 1.0;
	src0->data[0][1] = 2.0;
	src0->data[1][0] = 3.0;
	src0->data[1][1] = 4.0;
	src1->data[0][0] = 1.0;
	src1->data[0][1] = 2.0;
	src1->data[1][0] = 3.0;
	src1->data[1][1] = 4.0;

	mul_matrix( src0, src1, dist );
	print_matrix( dist, "dist" );

}
#endif
