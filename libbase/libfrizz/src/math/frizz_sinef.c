/*!******************************************************************************
 * @file frizz_sinef.c
 * @brief frizz_sinef() function
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_math_common.h"
#include "frizz_math.h"
#include "frizz_sinef.h"

static int fp2int( frizz_fp f )
{
	_MathFPCast c;
	int exp, val, sign_mask;
	c.zp = f;
	exp = c.fbit.exp - 127 - 23;
	val = 0;
	sign_mask = -c.fbit.sign;
	if( -23 <= exp ) {
		val = 0x800000 | c.fbit.frac;
		if( exp < 0 ) {
			val >>= -exp;
		} else if( 6 < exp ) {
			val = c.fbit.sign ? 0x80000000 : 0x7FFFFFFF;
		} else {
			val <<= exp;
		}
	}
	val = ( val ^ sign_mask ) - sign_mask;
	return val;
}

static const float sinef_r[] = {
	// z_rooteps_f
	1.7263349182589107e-4,
	// Taylor Coeff
	-0.1666665668,
	0.8333025139e-02,
	-0.1980741872e-03,
	0.2601903036e-5
};

frizz_fp frizz_sinef( frizz_fp x, int cosine )
{
	int N;
	const frizz_fp *r = ( const frizz_fp * )sinef_r;
	frizz_fp y, XN, g, R, res;
	frizz_fp __pi = FRIZZ_MATH_PI;
	frizz_fp half_pi = FRIZZ_MATH_PI_HALF;
	frizz_fp one_over_pi = FRIZZ_MATH_PI_INV;
	frizz_fp zero = CONST_ZERO;
	frizz_fp half = CONST_HALF;
	frizz_fp sgn = CONST_ONE;

	/* Use sin and cos properties to ease computations. */
	if( cosine ) {
		y = frizz_fabs( x ) + half_pi;
	} else {
		if( x < zero ) {
			sgn = -sgn;
			y = -x;
		} else {
			y = x;
		}
	}

	/* calculate the exponent. */
	if( y < zero ) {
		N = fp2int( y * one_over_pi - half );
	} else {
		N = fp2int( y * one_over_pi + half );
	}
	XN = as_frizz_fp( N );

	if( N & 1 ) {
		sgn = -sgn;
	}

	if( cosine ) {
		XN -= half;
	}

	y = frizz_fabs( x ) - XN * __pi;

	if( -r[0] < y && y < r[0] ) {
		res = y;
	} else {
		g = y * y;

		/* calculate the taylor series. */
		R = ( ( ( r[4] * g + r[3] ) * g + r[2] ) * g + r[1] ) * g;

		/* finally, compute the result. */
		res = y + y * R;
	}

	res *= sgn;

	return res;
}

