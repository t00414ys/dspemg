/*!******************************************************************************
 * @file frizz_sin.c
 * @brief frizz_sin() function
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_math_common.h"
#include "frizz_sinef.h"
#include "frizz_math.h"

/**
 * @brief sine
 *
 * @param [in] rad angle in radian
 *
 * @return sine of rad
 */
frizz_fp frizz_sin( frizz_fp rad )
{
	return frizz_sinef( rad, 0 );
}

