/*!******************************************************************************
 * @file frizz_atan.c
 * @brief frizz_atan() function
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_math_common.h"
#include "frizz_math.h"

#define CORDICBITS         24		///< Bit accuracy of Col Dick

#if 0 // Taylor expansion atangent {{{
static const float atan_r[] = {
	// z_rooteps_f
	1.7263349182589107e-4,
	// root(3.0);
	1.732050807,
	// a[]
	0.0, 0.523598775, 1.570796326, 1.047197551,
	// q[]
	0.1412500740e+1,
	// p[]
	-0.4708325141, -0.5090958253e-1,
};
static frizz_fp atangentf( frizz_fp x, frizz_fp v, frizz_fp u, int arctan2 )
{
	const frizz_fp *r = ( const frizz_fp* )atan_r;
	_MathFPCast c_u, c_v;
	frizz_fp res, f, g, R, P, Q, A;
	int N;
	int branch = 0;

	/* Preparation for calculating arctan2. */
	if( arctan2 ) {
		if( u == CONST_ZERO )
			if( v == CONST_ZERO ) {
				_MathFPCast c_res;
				c_res.ui = 0x7FC00000;
				return c_res.zp;
			} else {
				branch = 1;
				res = FRIZZ_MATH_PI_HALF;
			}

		if( !branch ) {
			int e;
			/* Get the exponent values of the inputs. */
			c_u.zp = u;
			c_v.zp = v;

			/* See if a divide will overflow. */
			e = c_v.fbit.exp - c_u.fbit.exp;
			if( 127 < e ) {
				branch = 1;
				res = FRIZZ_MATH_PI_HALF;
			}

			/* Also check for underflow. */
			else if( e < -126 ) {
				branch = 2;
				res = CONST_ZERO;
			}
		}
	}

	if( !branch ) {
		if( arctan2 ) {
			f = frizz_fabs( frizz_div( v, u ) );
		} else {
			f = frizz_fabs( x );
		}

		if( CONST_ONE < f ) {
			f = frizz_div( CONST_ONE, f );
			N = 2;
		} else {
			N = 0;
		}

		if( f > ( CONST_TWO - r[1] ) ) {
			A = r[1] - CONST_ONE;
			f = frizz_div( ( ( ( A * f - CONST_HALF ) - CONST_HALF ) + f ), ( r[1] + f ) );
			N++;
		}

		/* Check for values that are too small. */
		if( -r[0] < f && f < r[0] ) {
			res = f;
		}

		/* Calculate the Taylor series. */
		else {
			g = f * f;
			P = ( r[8] * g + r[7] ) * g;
			Q = g + r[6];
			R = frizz_div( P, Q );

			res = f + f * R;
		}

		if( N > 1 ) {
			res = -res;
		}

		res += r[N + 2];
	}

	if( arctan2 ) {
		if( u < CONST_ZERO ) {
			res = FRIZZ_MATH_PI - res;
		}
		if( v < CONST_ZERO ) {
			res = -res;
		}
	} else if( x < CONST_ZERO ) {
		res = -res;
	}

	return ( res );
}
#endif // }}}

static const float _atanTable[] = {
	7.85398163e-01, 4.63647609e-01, 2.44978663e-01, 1.24354995e-01,
	6.24188100e-02, 3.12398334e-02, 1.56237286e-02, 7.81234106e-03,
	3.90623013e-03, 1.95312252e-03, 9.76562190e-04, 4.88281211e-04,
	2.44140620e-04, 1.22070312e-04, 6.10351562e-05, 3.05175781e-05,
	1.52587891e-05, 7.62939453e-06, 3.81469727e-06, 1.90734863e-06,
	9.53674316e-07, 4.76837158e-07, 2.38418579e-07, 1.19209290e-07,
};
static const frizz_fp *atanTable = ( frizz_fp* )_atanTable;

static void cordic1( frizz_fp* x0, frizz_fp* y0, frizz_fp* z0, frizz_fp vecmode )
{
	frizz_fp tt = CONST_ONE;
	frizz_fp xx, yy, zz;
	int ii;

	xx = *x0;
	yy = *y0;
	zz = *z0;
	ii = 0;
	do {
		int sel = ( vecmode >= CONST_ZERO ) ? ( yy < vecmode ) : ( zz >= CONST_ZERO );
		frizz_fp x1 = xx;
		//if ((vecmode >= (frizz_fp)0 && yy < vecmode) || (vecmode<(frizz_fp)0  && zz >= (frizz_fp)0))
		if( sel ) {
			x1 = x1 - yy * tt;
			yy = yy + xx * tt;
			zz = zz - atanTable[ii];
		} else {
			x1 = x1 + yy * tt;
			yy = yy - xx * tt;
			zz = zz + atanTable[ii];
		}
		xx = x1;
		tt *= CONST_HALF;
		ii++;
	} while( ii < CORDICBITS );
	*x0 = xx;
	*y0 = yy;
	*z0 = zz;
}

/**
 * @brief arc tangent
 *
 * @param [in] a
 *
 * @return arc tangent of a in radian, [-pi/2, +pi/2]
 */
frizz_fp frizz_atan( frizz_fp a )
{
	frizz_fp x = CONST_ONE;
	frizz_fp z = CONST_ZERO;
	cordic1( &x, &a, &z, CONST_ZERO );
	return z;
}

