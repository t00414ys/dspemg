/*!******************************************************************************
 * @file kalman_filter.c
 * @brief code for Kalman Filter
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_const.h"
#include "frizz_mem.h"
#include "matrix.h"
#include "kalman_filter.h"
#include "kf_info.h"

#if 1
#define FILE int
#define fgets(...) NULL
#define fopen(...) NULL
#define fclose(...)
#define sscanf(...)
#define printf(...)
#define fprintf(...)
#define exit(x) return
#endif // UNUSE_NEWLIB

/**
 * @brief Create KalmanFilter handler
 *
 * @param stv_num num of states
 * @param mes_num num of measurements
 * @param con_num num of control-inputs
 * @param p_cb structure for callback
 *
 * @return KalmanFilter handler
 *
 * @note Please refer & set parameters with KF_Get*()
 */
KF_h KF_New( int stv_num, int mes_num, int con_num, KF_Callback *p_cb )
{
	KF_t *kf = ( KF_t* )frizz_malloc( sizeof( KF_t ) );

	if( p_cb ) {
		kf->cb.arg = p_cb->arg;
		kf->cb.init_func = p_cb->init_func;
		kf->cb.del_func = p_cb->del_func;
		kf->cb.pre_func = p_cb->pre_func;
	} else {
		kf->cb.arg = 0;
		kf->cb.init_func = 0;
		kf->cb.del_func = 0;
		kf->cb.pre_func = 0;
	}

	kf->stv = alloc_matrix( stv_num, 1 );	///< vector
	kf->prd = alloc_matrix( stv_num, 1 );	///< vector

	kf->mes = alloc_matrix( mes_num, 1 );	///< vector
	kf->res = alloc_matrix( mes_num, 1 );	///< vector

	kf->con = alloc_matrix( con_num, 1 );	///< vector

	kf->G_m		= alloc_matrix( stv_num, con_num );
	kf->H_m		= alloc_matrix( mes_num, stv_num );

	kf->PHI_m	= alloc_matrix( stv_num, stv_num );

	kf->M_m		= alloc_matrix( stv_num, stv_num );
	kf->K_m		= alloc_matrix( stv_num, mes_num );

	kf->P_m 	= alloc_matrix( stv_num, stv_num );
	kf->Q_m 	= alloc_matrix( stv_num, stv_num );
	kf->R_m 	= alloc_matrix( mes_num, mes_num );
	kf->S_m		= alloc_matrix( con_num, con_num );

	kf->I_m 	= alloc_matrix( stv_num, stv_num );

	kf->S_0		= alloc_matrix( stv_num, 1 );
	kf->SS_0	= alloc_matrix( stv_num, stv_num );
	kf->SS_1	= alloc_matrix( stv_num, stv_num );
	kf->SM_0	= alloc_matrix( stv_num, mes_num );
	kf->MM_0	= alloc_matrix( mes_num, mes_num );
	kf->MM_1	= alloc_matrix( mes_num, mes_num );
	kf->MM_2	= alloc_matrix( mes_num, mes_num );
	kf->SC_0	= alloc_matrix( stv_num, con_num );
	// set const
	set_matrix_identity( kf->I_m );

	return kf;
}

/**
 * @brief Initilize
 *
 * @param h KalmanFilter handler
 */
void KF_Init( KF_h h )
{
	KF_t *kf = ( KF_t* )h;
	// callback
	if( kf->cb.init_func ) {
		kf->cb.init_func( kf->cb.arg, h );
	}
}

/**
 * @brief Delete KalmanFilter handler
 *
 * @param h KalmanFilter handler
 */
void KF_Delete( KF_h h )
{
	KF_t *kf = ( KF_t* )h;

	free_matrix( kf->stv );
	free_matrix( kf->prd );

	free_matrix( kf->mes );
	free_matrix( kf->res );

	free_matrix( kf->con );

	free_matrix( kf->G_m );
	free_matrix( kf->H_m );

	free_matrix( kf->PHI_m );

	free_matrix( kf->M_m );
	free_matrix( kf->K_m );

	free_matrix( kf->P_m );
	free_matrix( kf->Q_m );
	free_matrix( kf->R_m );
	free_matrix( kf->S_m );

	free_matrix( kf->I_m );

	free_matrix( kf->S_0 );
	free_matrix( kf->SS_0 );
	free_matrix( kf->SS_1 );
	free_matrix( kf->SM_0 );
	free_matrix( kf->MM_0 );
	free_matrix( kf->MM_1 );
	free_matrix( kf->MM_2 );
	free_matrix( kf->SC_0 );

	// callback
	if( kf->cb.del_func ) {
		kf->cb.del_func( kf->cb.arg );
	}

	frizz_free( kf );
}

/**
 * @brief refer vector (matrix) of states
 *
 * @param h KalmanFilter handler
 *
 * @return vector(matrix) of states
 */
Matrix* KF_GetSTV( KF_h h )
{
	KF_t *kf = ( KF_t* )h;
	return kf->stv;
}

/**
 * @brief refer vector (matrix) of predicted states at stage of prediction
 *
 * @param h KalmanFilter handler
 *
 * @return vector(matrix) of predicted states
 */
Matrix* KF_GetPRD( KF_h h )
{
	KF_t *kf = ( KF_t* )h;
	return kf->prd;
}

/**
 * @brief refer vector (matrix) of residual between measurements and predictons
 *
 * @param h KalmanFilter handler
 *
 * @return vector(matrix) of residual
 */
Matrix* KF_GetRES( KF_h h )
{
	KF_t *kf = ( KF_t* )h;
	return kf->res;
}

/**
 * @brief refer Phi: Transition matrix
 *
 * @param h KalmanFilter handler
 *
 * @return Phi matrix
 *
 * @note Phi = (I+dtF)
 */
Matrix* KF_GetPHI( KF_h h )
{
	KF_t *kf = ( KF_t* )h;
	return kf->PHI_m;
}

/**
 * @brief refer G: Control-input transition matrix
 *
 * @param h KalmanFilter handler
 *
 * @return G matrix
 *
 * @note prd = (I+dtF) * stv + G * input
 */
Matrix* KF_GetG( KF_h h )
{
	KF_t *kf = ( KF_t* )h;
	return kf->G_m;
}

/**
 * @brief refer H: Measurement matrix
 *
 * @param h KalmanFilter handler
 *
 * @return H matri
 *
 * @note mes = H * stv
 */
Matrix* KF_GetH( KF_h h )
{
	KF_t *kf = ( KF_t* )h;
	return kf->H_m;
}

/**
 * @brief refer P: Error covariance matrix
 *
 * @param h KalmanFilter handler
 *
 * @return P matrix
 */
Matrix* KF_GetP( KF_h h )
{
	KF_t *kf = ( KF_t* )h;
	return kf->P_m;
}

/**
 * @brief refer Q: Covariance matrix for noise of process at discrete time domain
 *
 * @param h KalmanFilter handler
 *
 * @return Q matrix
 */
Matrix* KF_GetQ( KF_h h )
{
	KF_t *kf = ( KF_t* )h;
	return kf->Q_m;
}

/**
 * @brief refer R: Covariance matrix for noise of measurement
 *
 * @param h KalmanFilter handler
 *
 * @return matrix[row][col]
 */
Matrix* KF_GetR( KF_h h )
{
	KF_t *kf = ( KF_t* )h;
	return kf->R_m;
}

/**
 * @brief refer S: Covariance matrix for noise of control-input
 *
 * @param h KalmanFilter handler
 *
 * @return S matrix
 */
Matrix* KF_GetS( KF_h h )
{
	KF_t *kf = ( KF_t* )h;
	return kf->S_m;
}

/**
 * @brief refer Identity matrix (num of states x num of states)
 *
 * @param h KalmanFilter handler
 *
 * @return Identity matrix
 */
Matrix* KF_GetI( KF_h h )
{
	KF_t *kf = ( KF_t* )h;
	return kf->I_m;
}

/**
 * @brief make covariance matrix for noise of process at discrete time domain from countinuous time domain
 *
 * @param h KalmanFilter handler
 * @param F_m Dynamics matrix (stv' = F_m * stv)
 * @param Q_c Q matrix at continuous time domain (Q_c = E[w * wt])
 * @param dtm time of sampling interval
 */
void KF_MakeQ( KF_h h, Matrix* F_m, Matrix* Q_c, frizz_fp dtm )
{
	KF_t *kf = ( KF_t* )h;
	frizz_fp per3 = as_frizz_fp( 1.0 / 3.0 );
	frizz_fp half = FRIZZ_CONST_HALF;

	// create Q
	copy_matrix( Q_c, kf->Q_m );

	if( 0 < kf->con->rows ) {
		mul_matrix( kf->G_m, kf->S_m, kf->SC_0 );
		mul_trmatrix( kf->SC_0, kf->G_m, kf->SS_0 );
		add_matrix( kf->Q_m, kf->SS_0, kf->Q_m );
	}

	mul_matrix( F_m, kf->Q_m, kf->SS_0 );

	mul_trmatrix( kf->SS_0, F_m, kf->SS_1 );
	scale_matrix( kf->SS_1, ( dtm * dtm * dtm )*per3 );	// divide numerid
	scale_matrix( kf->SS_0, ( dtm * dtm )*half );
	add_matrix( kf->SS_0, kf->SS_1, kf->SS_0 );

	mul_trmatrix( kf->Q_m, F_m, kf->SS_1 );
	scale_matrix( kf->SS_1, ( dtm * dtm )*half );
	add_matrix( kf->SS_0, kf->SS_1, kf->SS_0 );

	scale_matrix( kf->Q_m, dtm );

	add_matrix( kf->Q_m, kf->SS_0, kf->Q_m );
}

/**
 * @brief prediction
 *
 * @param h KalmanFilter handler
 * @param con control-input (not use ,if NULL)
 * @param dtm time of sampling interval
 */
void KF_Predict( KF_h h, frizz_fp *con, frizz_fp dtm )
{
	KF_t *kf = ( KF_t* )h;

	// callback
	if( kf->cb.pre_func ) {
		kf->cb.pre_func( kf->cb.arg, h, con, dtm );
	}
	// set parameter
	if( 0 < kf->con->rows ) {
		if( con != 0 ) {
			set_matrix( kf->con, con );
		}
	}
	// predict
	mul_matrix( kf->PHI_m, kf->stv, kf->prd );
	if( con != 0 && 0 < kf->con->rows ) {
		mul_matrix( kf->G_m, kf->con, kf->S_0 );
		add_matrix( kf->prd, kf->S_0, kf->prd );
	}
	// M_m: pre P
	mul_matrix( kf->PHI_m, kf->P_m, kf->SS_0 );
	mul_trmatrix( kf->SS_0, kf->PHI_m, kf->M_m );
	add_matrix( kf->M_m, kf->Q_m, kf->M_m );
}

/**
 * @brief make residual between measurements and predictons
 *
 * @param h KalmanFilter handler
 * @param mes measurement
 */
void KF_MakeResidual( KF_h h, frizz_fp *mes )
{
	KF_t *kf = ( KF_t* )h;

	// subtraction
	if( mes == 0 ) {
		return;
	}
	set_matrix( kf->mes, mes );
	mul_matrix( kf->H_m, kf->prd, kf->res );
	sub_matrix( kf->mes, kf->res, kf->res );
}

/**
 * @brief filtering
 *
 * @param h KalmanFilter handler
 */
void KF_Update( KF_h h )
{
	KF_t *kf = ( KF_t* )h;

	// generate Gain
	mul_trmatrix( kf->M_m, kf->H_m, kf->SM_0 );
	mul_matrix( kf->H_m, kf->SM_0, kf->MM_0 );
	add_matrix( kf->MM_0, kf->R_m, kf->MM_0 );
	if( invert_matrix( kf->MM_0, kf->MM_1, kf->MM_2 ) != 0 ) {
		fprintf( stderr, "invert_error \n" );
		print_matrix( kf->M_m, ( char * )"M_m" );
		print_matrix( kf->R_m, ( char * )"R_m" );
		print_matrix( kf->H_m, ( char * )"H_m" );
		print_matrix( kf->MM_0, ( char * )"MM_0" );
		print_matrix( kf->MM_1, ( char * )"MM_1" );
		print_matrix( kf->SM_0, ( char * )"SM_0" );
		exit( 1 );
	}
#ifdef CHKREVMTX
	mul_matrix( kf->MM_0, kf->MM_1, kf->MM_2 );
	if( chk_matrix( kf->MM_2 ) != 0 ) {
		fprintf( stderr, "invert_error\n" );
		print_matrix( kf->MM_0, ( char * )"MM_0" );
		print_matrix( kf->MM_1, ( char * )"MM_1" );
		exit( 1 );
	}
#endif
	mul_matrix( kf->SM_0, kf->MM_1, kf->K_m );	// K_m

	// filtering
	mul_matrix( kf->K_m, kf->res, kf->stv );
	add_matrix( kf->prd, kf->stv, kf->stv );

	// update P
	mul_matrix( kf->K_m, kf->H_m, kf->SS_0 );
	sub_matrix( kf->I_m, kf->SS_0, kf->SS_0 );
	mul_matrix( kf->SS_0, kf->M_m, kf->P_m );
}

