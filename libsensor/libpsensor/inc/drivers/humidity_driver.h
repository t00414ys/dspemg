/*!******************************************************************************
 * @file    humidity_driver.h
 * @brief   sample program for humidity sensor
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __HUMIDITY_DRIVER_H__
#define __HUMIDITY_DRIVER_H__

#include "config.h"
#include "base_driver.h"

#if defined(USE_BME280)
#include "bme280_api.h"
#define	BME280_DATA	{	bme280_init,			bme280_ctrl,			bme280_rcv,			bme280_conv, \
						bme280_setparam,		bme280_get_ver,			bme280_get_name ,		0,			\
						{bme280_get_condition,	0,			0,			0}					},
#else
#define	BME280_DATA
#endif

#if defined(USE_HTS221)
#include "hts221_api.h"
#define	HTS221_DATA	{	hts221_init,			hts221_ctrl,			hts221_rcv,			hts221_conv, \
						hts221_setparam,				hts221_get_ver,		hts221_get_name ,		0,			\
						{hts221_get_condition,	0,			0,			0}					},
#else
#define	HTS221_DATA
#endif


#endif /* __ACCL_DRIVER_H__ */
