/*!******************************************************************************
 * @file    light_driver.h
 * @brief   sample program for light sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/


#ifndef __LIGHT_DRIVER_H__
#define __LIGHT_DRIVER_H__

#include "config.h"
#include "base_driver.h"

#if defined(USE_STK3420)
#include "stk3420_api.h"
#define	STK3420_DATA		{	stk3420_init,						stk3420_ctrl_light,		stk3420_rcv_light ,		stk3420_conv_light,			\
								stk3420_setparam,					stk3420_get_ver,		stk3420_get_name ,		0,							\
								{stk3420_light_get_condition,		0,						0,						0}							},
#else
#define	STK3420_DATA
#endif

#if defined(USE_GP2AP054A10F)
#include "gp2ap054a10f_api.h"
#define	GP2AP054A10F_DATA	{	gp2ap054a10f_init,					gp2ap054a10f_ctrl_light,gp2ap054a10f_rcv_light,	gp2ap054a10f_conv_light, 	\
								gp2ap054a10f_setparam,				gp2ap054a10f_get_ver,	gp2ap054a10f_get_name,	0,							\
								{gp2ap054a10f_light_get_condition,	0,						0,						0}							},
#else
#define	GP2AP054A10F_DATA
#endif

#if defined(USE_PAC7673EE)
#include "pac7673ee_api.h"
#define	PAC7673EE_DATA		{	pac7673ee_init,						pac7673ee_ctrl_light,		pac7673ee_rcv_light ,		pac7673ee_conv_light,			\
								pac7673ee_setparam,					pac7673ee_get_ver,		pac7673ee_get_name ,		0,							\
								{pac7673ee_light_get_condition,		0,						0,						0}							},
#else
#define	PAC7673EE_DATA
#endif

#if defined(USE_PA12201001)
#include "pa12201001_api.h"
#define	PA12201001_DATA		{	pa12201001_init,						pa12201001_ctrl_light,		pa12201001_rcv_light ,		pa12201001_conv_light,			\
								pa12201001_setparam,					pa12201001_get_ver,		pa12201001_get_name ,		0,							\
								{pa12201001_light_get_condition,		0,						0,						0}							},
#else
#define	PA12201001_DATA
#endif

#if defined(USE_LIGHT_EMU)
#include "lightemu_api.h"
#define	LIGHTEMU_DATA		{	lightemu_init,						lightemu_ctrl,			lightemu_rcv ,			lightemu_conv, 				\
								lightemu_setparam,					lightemu_get_ver,		lightemu_get_name ,		0,							\
								{lightemu_get_condition,			0,						0,						0}							},
#else
#define	LIGHTEMU_DATA
#endif

#endif /*  */
