/*!******************************************************************************
 * @file    group_activity.h
 * @brief   group of virtual activity sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/**
 *  @brief activity header group
 */
#ifndef __GROUP_ACTIVITY_H__
#define __GROUP_ACTIVITY_H__

#include "activity_detector.h"
#include "motion_detector.h"
#include "motion_sensing.h"
#include "calorie_sen.h"

#endif
