/*!******************************************************************************
 * @file    group_gesture.h
 * @brief   group of virtual gesture sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/**
 *  @brief gesture_core sensor header group
 */

#ifndef __GROUP_GESTURE_H__
#define __GROUP_GESTURE_H__

#include "gesture_core.h"
#include "glance_stat.h"
#include "tilt_det.h"
#include "pickup_det.h"
#include "glance_det.h"
#include "wakeup_det.h"
#include "wrist_tilt_det.h"

#endif
