/*!******************************************************************************
 * @file    mag_util.h
 * @brief   utility header for Magnet sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MAG_UTIL_H__
#define __MAG_UTIL_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief create magnet utility
 */
void MagUtil_Create( void );

/**
 * @brief destroy magnet utility
 */
void MagUtil_Destroy( void );

/**
 * @brief initialize magnet utility
 */
void MagUtil_Init( void );

/**
 * @brief set calibration data
 *
 * @param mag[3] magnetic data
 * set sample data for calibration calculation
 * input '3dimension rotate data' at resting state
 */
void MagUtil_CalibPush( frizz_fp mag[3] );

/**
 * @brief calculate result of calibration
 *
 * @return -1:can't calculate, 0:success
 * base of #MagUtil_CalibPush()
 * when sample data are a few, it can't calculate
 */
int MagUtil_CalibCalc( void );

/**
 * @brief magnetic sensor Hard-Iron collection
 *
 * @param mag[3] magnetic data
 * #MagUtil_CalibCalc() result param (calibration data)
 * #MagUtil_SetHardComp(), #MagUtil_SetSoftComp() setting param
 * calibrate input magnetic data
 */
void MagUtil_CompHard( frizz_fp mag[3] );

/**
 * @brief magnetic sensor Soft-Iron collection
 *
 * @param mag[3] magnetic data
 * #MagUtil_CalibCalc() result param (calibration data)
 * #MagUtil_SetHardComp(), #MagUtil_SetSoftComp() setting param
 * calibrate input magnetic data
 */
void MagUtil_CompSoft( frizz_fp mag[3] );

/**
 * @brief set Hard-Iron collection value of magnetic sensor
 *
 * @param hard[3] Hard-Iron collection value
 */
void MagUtil_SetHardComp( frizz_fp hard[3] );

/**
 * @brief get Hard-Iron collection value of magnetic sensor
 *
 * @param hard[3] Hard-Iron value
 */
void MagUtil_GetHardComp( frizz_fp hard[3] );

/**
 * @brief set Hard-Iron collection value of magnetic sensor
 *
 * @param soft[9] Soft-Iron collection matrix setting
 */
void MagUtil_SetSoftComp( frizz_fp soft[9] );

/**
 * @brief get Soft-Iron collection value of magnetic sensor
 *
 * @param soft[9] Soft-Iron collection matrix
 */
void MagUtil_GetSoftComp( frizz_fp soft[9] );

#ifdef __cplusplus
}
#endif

#endif//__MAG_UTIL_H__
