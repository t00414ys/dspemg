/*!******************************************************************************
 * @file    accl_fall_down.h
 * @brief   virtual accel fall down sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup ACCEL_FALL_DOWN ACCEL FALL DOWN SENSOR
 *  @ingroup virtualgroup Virtual Sensor
 *  @brief accel fall down sensor<br>
 *  @brief -# <B>Contents</B><br>
 *  @brief A sensor for taking out fall down detection.<br>
 *  <br>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><br>
 *  @brief Sensor ID : #SENSOR_ID_ACCEL_FALL_DOWN<br>
 *  @brief Parent Sensor ID : #SENSOR_ID_ACCEL_RAW, #SENSOR_ID_PRESSURE_RAW <br>
 *  <br>
 *  \dot
 *  digraph structs {
 *  	node [shape=record,  fontsize=10];
 *		struct1 [label="SENSOR_ID_ACCEL_FALL_DOWN" color=red];
 *		struct2 [label="SENSOR_ID_ACCEL_RAW" color=blue];
  *		struct3 [label="SENSOR_ID_PRESSURE_RAW " color=blue];
 *		struct2 -> struct1;struct3 -> struct1;
 *	}
 *  \enddot
 *  <br>
 *  @brief -# <B>Detection Timing</B><br>
 *  @brief Detection Timing : On-changed (This sensor type output sensor
 * when sensor data have a change.)
 */


#ifndef __ACCL_FALL_DOWN_H__
#define __ACCL_FALL_DOWN_H__

//#include "frizz_type.h"
#include "sensor_if.h"
//#include "libsensors_id.h"

/** @defgroup ACCEL_FALL_DOWN ACCEL FALL DOWN SENSOR
 *  @{
 */
#define	AAC_count_interval	2		//Calculate amount of acceleration cahnge sample interval
#define	APC_count_interval	100		//Calculate amount of pressure change sample interval
#define	window_length	200//(ms)	//Observe window length
#define	sampling_rate	10 //(ms)	//G-sensor and pressure sensor raw data sampling rate
#define	window_size		(window_length/sampling_rate)//(sample)//V1 and V2 summation window length
#define FilterCoef		0.98		//Pressure sensor raw data low pass filter alpha parameter
#define FilterCoef2		1-0.98		//Pressure sensor raw data low pass filter 1-alpha parameter

#define	WeightlessnessThd	0.6//0.4//Freefall weightlessness threshold (No use now)
#define	ImpactThd			1.8		//Freefall impact rise threshold (No use now)
#define AAC_ChkCunt			330//(sample)//When impact be detected check another behavior interval
#define APC_Thd				0.04	//Amout of pressure change threshold (Altitude change behavior threshold)
#define V3_Thd				19		//Different velocity threshold
#define	AAC_Peak_Thd		15//6//3.5	//Amount of pressure change threshold (Imapct behavior threshold)

#define XYZ_drop_Thd		0.4		//G-sensor X,Y,Z axis dorp freefall threshold
#define Drop_Thd			0.5		//SMV and AAC drop freefall threshold
#define Drop_ChkCunt		30//(sample)//Drop time threshold

#define APC_bf_AAC_ChkCunt	50		//Amount of pressure change check counter threshold
#define APC_bfThd			0.04	//Amount of pressure change threshold during 2 Secs before
#define ZeroINI				0		//For frizz_fp table
#define OneINI				1		//For frizz_fp table

#define DampingCunt			100
#define CheckOrientation	200
#define	NoMoveTHD			0.1
#define NoMoveCunt			1

/*!**********************************************************************
 *@brief  Calculate amount of acceleration cahnge value
 *@par    External public functions
 *
 *@param[in]   		g_x		G-sensor X axis raw data
 *@param[in]   		g_y		G-semsor Y axis raw data
 *@param[in]    	g_z		G-sensor Z axis raw data
 *@param[in]		*count	index to calculation sample interval definition
 *
 *@retval   return amount of acceleration value (frizz_fp( sqrt((temp_gx*temp_gx) + (temp_gy*temp_gy) + (temp_gz*temp_gz))))
 *
****************************************************************************/
frizz_fp frizz_Calculate_AAC_curve( frizz_fp g_x, frizz_fp g_y, frizz_fp g_z, frizz_fp *count );

/*!**********************************************************************
 *@brief  Calculate signal magnitude vector
 *@par    External public functions
 *
 *@param[in]   		g_x		G-sensor X axis raw data
 *@param[in]   		g_y		G-sensor Y axis raw data
 *@param[in]    	g_z		G-sensor Z axis raw data
 *
 *@retval   return signal megnitude vector (frizz_fp( sqrt((g_x*g_x) + (g_y*g_y) + (g_z*g_z))))
 *
****************************************************************************/
frizz_fp frizz_Calculate_SMV_curve( frizz_fp g_x, frizz_fp g_y, frizz_fp g_z );

/*!**********************************************************************
 *@brief  Pressure sensor low pass filter
 *@par    External public functions
 *
 *@param[in]   		Pres 	pressure sensor raw data
 *
 *@retval   return LPF result
 *
****************************************************************************/
frizz_fp frizz_Calculate_PresFilter( frizz_fp Pres );

/*!**********************************************************************
 *@brief  Calculate amoumt of pressure change
 *@par    External public functions
 *
 *@param[in]   		Pres 	pressure sensor raw data
 *@param[in]   		*count 	index to calculation sample interval definition
 *
 *@retval   return LPF result
 *
****************************************************************************/
frizz_fp frizz_Calculate_APC_curve( frizz_fp Pres, frizz_fp *count );

/*!**********************************************************************
 *@brief  Velocity 1 estemator
 *@par    External public functions
 *
 *@param[in]   		length 	summation window length
 *
 *@retval   return velocity 1 value (frizz_fp(summation( sqrt((g_x*g_x) + (g_y*g_y) + (g_z*g_z)) - 9.81)dt))
 *
****************************************************************************/
frizz_fp frizz_Calculate_V1_curve( int length );

/*!**********************************************************************
 *@brief  Velocity 2 estemator
 *@par    External public functions
 *
 *@param[in]   		length 	summation window length
 *
 *@retval   return velocity 2 value (frizz_fp(sqrt((summation(g_x)dt)^2 + (summation(g_y)dt)^2 + (summation(g_z)dt)^2) - summation(9.81)dt))
 *
****************************************************************************/
frizz_fp frizz_Calculate_V2_curve( int length );

/*!**********************************************************************
 *@brief  Fall down detect judgement flow
 *@par    External public functions
 *
 *@param  void
 *
 *@retval   0	Fall down not be detected
 *@retval   1	Fall down be detected
 *
****************************************************************************/
char frizz_Calculate_falling_detection( void );

/*!**********************************************************************
 *@brief  Sensor initial
 *@par    External public functions
 *
 *@param  void
 *
 *@retval   sensor_if_t		@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *
****************************************************************************/
sensor_if_t* accl_fall_down_init( void );

#endif

/** @} */
