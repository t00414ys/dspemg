/*!******************************************************************************
 * @file    gyro_dummy.h
 * @brief   virtual gyro dummy sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/**
 *  @brief	Virtual gyro dummy sensor
 *  @note	When you check gyro sensor data by PC, this sensor is used.
 */
#ifndef __GYRO_DUMMY_H__
#define __GYRO_DUMMY_H__

#include "sensor_if.h"
#include "libsensors_id.h"

/**
 * @brief	Initialization gyro dummy sensor
 * @par		External public functions
 *
 * @retval	sensor_if_t
 */
sensor_if_t* gyro_dummy_init( void );

#endif
