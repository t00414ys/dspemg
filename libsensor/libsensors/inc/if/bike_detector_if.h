/*!******************************************************************************
 * @file    bike_detector_if.h
 * @brief   virtual bike detector sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __BIKE_DETECTOR_MCC_IF_H__
#define __BIKE_DETECTOR_MCC_IF_H__

#include "libsensors_id.h"

/** @defgroup BIKE_DETECTOR BIKE DETECTOR
 *  @{
 */
#define BIKE_DETECTOR_ID SENSOR_ID_BIKE_DETECTOR	//!< An bike detector interface ID

/**	@struct bike_detector_data_t
 *	@brief Output data structure for bike detector sensor
 */
typedef struct {
	unsigned int	state;				//!< activity state (0:walk 1:run 2:bike 3:checking 4:rest 5:vehicle)
} bike_detector_data_t;

/**
 * @name Command List
 */
//@{
/**
 * @brief Get Version
 *
 * @param cmd_code BIKE_DETECTOR_GET_VERSION
 *
 * @return libactivity version, -1: Command Error
 */
#define BIKE_DETECTOR_GET_VERSION			0xFF
//@}

/** @} */
#endif
