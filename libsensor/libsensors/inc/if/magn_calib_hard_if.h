/*!******************************************************************************
 * @file    magn_calib_hard_if.h
 * @brief   virtual magnet calibration hard sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MAGN_CALIB_HARD_IF_H__
#define __MAGN_CALIB_HARD_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup MAGNET_CALIB_HARD MAGNET CALIB HARD
 *  @{
 */
#define MAGNET_CALIB_HARD_ID	SENSOR_ID_MAGNET_CALIB_HARD //!< Hard-Iron calibration

/**
 * @struct magnet_calib_hard_data_t
 * @brief 	Output data structure for magnet calibration hard sensor
 */
typedef struct {
	FLOAT		data[3];	//!< Hard-Iron magnetic field parameter[uT] data[0]:X-axis parameter[uT], data[1]:Y-axis parameter[uT], data[2]:Z-axis parameter[uT], (u:micro)
} magnet_calib_hard_data_t;

/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif
