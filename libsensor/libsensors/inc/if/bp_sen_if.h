/*!******************************************************************************
 * @file    bp_sen_if.h
 * @brief   virtual blood pressure sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __BP_SEN_IF_H__
#define __BP_SEN_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup SENSOR_ID_BLOOD_PRESSURE BLOOD PRESSURE
 *  @{
 */
#define BLOOD_PRESSURE_ID	SENSOR_ID_BLOOD_PRESSURE //!< Pedestrian Dead Reckoning

#define BP_DETAIL_PARAM_SIMPLE				0x00	//!< output data structure is from "version" to "BnCal" of #ST_BLOOD_PRESSURE_DATA.
/** @} */

#define BP_DETAIL_PARAM_TM_DETAIL			0x01	//!< output data structure is all of #ST_BLOOD_PRESSURE_DATA.
#define BP_DETAIL_PARAM_FFT_DETAIL			0x02	//!< output data structure is all of #ST_BLOOD_PRESSURE_DATA.
#define BP_DETAIL_PARAM_FFT_DETAIL2			0x03	//!< output data structure is all of #ST_BLOOD_PRESSURE_DATA.

#define BP_DETAIL_PARAM_MAX					BP_DETAIL_PARAM_FFT_DETAIL2	//!<param data

/** @defgroup SENSOR_ID_BLOOD_PRESSURE BLOOD PRESSURE
 *  @{
 */

/**
 * @struct hr_sample_data
 * @brief A part of #ST_BLOOD_PRESSURE_DATA.
 */
typedef struct {
	short			index;					//!< wave data index(graph x-position)
	short			value;					//!< wave data value(graph y-position)
} hr_sample_data;

/**
 * @struct ST_BLOOD_PRESSURE_DATA
 * @brief Output data structure for blood pressure sensor
 * @brief In #BP_DETAIL_PARAM_SIMPLE , output data structure is from "version" to "BnCal".\n
 */
typedef struct {
	unsigned short	version;				//!< version
	unsigned short	length; 				//!< length
	short			HRF; 					//!< FFT HeartRate
	short			BP_MAX; 				//!< Blood presser MAX
	short			BP_MIN; 				//!< Blood presser MIN
	short			SAMPLE_HR; 				//!< Heart Rate(TM)
	float			BnCal;					//!< Calorie (kcal)
	unsigned char	status; 				//!< Blood pressure status (0:PPG wave is NG, 1:the pulse is OK, 2:blood pressure is OK)
	unsigned char	reserve1; 				//!< reserve
	unsigned char	detail_type;			//!< detail data type (BP_DETAIL_PARAM_TM_DETAIL,BP_DETAIL_PARAM_FFT_DETAIL or BP_DETAIL_PARAM_FFT_DETAIL2)
	unsigned char	reserve2;				//!< reserve
	short			data1;					//!< Data calculating BP1
	short			data2;					//!< Data calculating BP2
	short			data3;					//!< Data calculating BP3
	short			data4;					//!< Data calculating BP4
	hr_sample_data	sample[50]; 			//!< wave pattern data

	float			max_data;				//!<Peak position of the wave pattern which removed a DC ingredient after FFT enforcemen
} ST_BLOOD_PRESSURE_DATA;

// @}
/** @} */

/**
 * @struct ST_BLOOD_PRESSURE_RAW_STAT
 * @brief Output data structure for blood pressure sensor
 */
typedef struct {
	unsigned int	ppg_max;	//!< PPG max
	unsigned int	ppg_min;	//!< PPG max
	unsigned int	upper_limit;	//!< upper limit
	unsigned int	lower_limit;	//!< lower limit
	unsigned int	dynamic_range;	//!< dynamic range
	unsigned int	is_dark;		//!< dark
} ST_BLOOD_PRESSURE_RAW_STAT;


/**
 * @brief Set Result Detail
 * @param param[0] output data structure mode
 * @brief output data structure mode (#BP_DETAIL_PARAM_SIMPLE, #BP_DETAIL_PARAM_TM_DETAIL, #BP_DETAIL_PARAM_FFT_DETAIL)
 */
#define BLOOD_PRESSURE_SET_DETAIL			0x00

/**
 * @brief Get Pressure Raw Sensor Statistics
 */
#define BLOOD_PRESSURE_RAW_STAT				0x01

/** @defgroup SENSOR_ID_BLOOD_PRESSURE BLOOD PRESSURE
 *  @{
 */

/**
 * @name Command List
 */
//@{

/**
 * @brief Get Version
 * @param cmd_code BLOOD_PRESSURE_GET_VERSION
 * @return BLOOD_PRESSURE version, -1: Command Error
 */
#define BLOOD_PRESSURE_GET_VERSION			0xFF

//@}

/** @} */

#endif
