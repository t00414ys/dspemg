/*!******************************************************************************
 * @file    activity_detector_if.h
 * @brief   virtual activity detector sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ACTIVITY_DETECTOR_IF_H__
#define __ACTIVITY_DETECTOR_IF_H__

#include "libsensors_id.h"

/** @defgroup ACTIVITY_DETECTOR ACTIVITY DETECTOR
 *  @{
 */
#define ACTIVITY_DETECTOR_ID SENSOR_ID_ACTIVITY_DETECTOR	//!< An activity detector interface ID

/**	@struct activity_data_t
 *	@brief Output data structure for activity detector sensor
 */
typedef struct {
	unsigned int	timestamp;				//!< used to report whenever activity was detected
	unsigned int	activity;				//!< activity status (1:SLEEP 2:REST 3:WALK 4:RUN)
	unsigned int	step_cnt;				//!< step count during active status
	unsigned int	tossAndTurn_cnt;		//!< toss-and-turn count during non-active status
} activity_data_t;

/**
 * @name Command List
 */
//@{

//@}

/** @} */
#endif
