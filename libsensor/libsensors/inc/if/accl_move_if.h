/*!******************************************************************************
 * @file    accl_move_if.h
 * @brief   virtual accel move sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ACCL_MOVE_IF_H__
#define __ACCL_MOVE_IF_H__

#include "libsensors_id.h"

/** @defgroup ACCEL_MOVE ACCEL MOVE SENSOR
 *  @{
 */
#define ACCEL_MOVE_ID	SENSOR_ID_ACCEL_MOVE	//!< MOVE Detection with Accelerator

/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif
