/*!******************************************************************************
 * @file    accl_pos_det_if.h
 * @brief   virtual accel posture detect interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ACCL_POS_DET_IF_H__
#define __ACCL_POS_DET_IF_H__

#include "libsensors_id.h"


/** @defgroup ACCEL_POS_DET ACCEL POS DET
 *  @{
 */
#define ACCEL_POS_DET_ID SENSOR_ID_ACCEL_POS_DET	//!< An accel posture detect interface ID

/**
 * @struct accel_pos_det_data_t
 * @brief Output data structure for accel posture detect sensor
 */
typedef struct {
	unsigned int data;		//!<  a fall down counter
} accel_pos_det_data_t;

/**
 * @name Command List
 */
//@{
/**
 * @brief Clear a fall down counter
 * @return 0: OK other:NG
 */
#define ACCEL_POS_DET_CMD_CLEAR_COUNT		0x00
/**
 * @brief Set sensitivity
 * @brief This parameter for falling down sensitivity level setting.
 * @return 0: OK other:NG
 */
#define ACCEL_POS_DET_CMD_SET_SENSITIVITY		0x01
/**
 * @brief Set threshold
 * @param param[0] (float) The detection time of fall down.
 * In this time period second falling down event will not accepted.
 * Default value is 330.0 (3.3Sec).
 * @param param[1] (float) The value is related with velocity toward the floor.
 * Range: 1~30.
 * Default value is 19.
 * @param param[2] (float) The value is related with force of fall down.
 * Range: 1~30.
 * Default value is 15.
 * @param param[3] (float) The sensitivity threshold of detecting move based on amount of acceleration change after damping time.
 * Default value is 0.1.
 * @param param[4] (int) Damping count.
 * Avoid damping time based on move or not check function.
 * Default value is 100(1Sec).
 * @param param[5] (int) Orientation time.
 * The detection time based on move or not check function after end of avoid damping time.
 * Default value is 200(2Sec).
 */
#define ACCEL_POS_DET_CMD_SET_PARAMETER		0x02
/**
 * @brief Get threshold
 * @brief These thresholds are outputed by frizz uart.
 * @return 0: OK other:NG
 */
#define ACCEL_POS_DET_CMD_GET_PARAMETER		0x03
//@}

/** @} */
#endif
