/*!******************************************************************************
 * @file    pickup_det_if.h
 * @brief   virtual pickup detector if sensor interface
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PICKUP_DET_IF_H__
#define __PICKUP_DET_IF_H__

#include "libsensors_id.h"

/** @defgroup PICKUP_DET PICKUP DETECTOR
 *  @{
 */
#define PICKUP_DET_ID SENSOR_ID_PICKUP_DETECTOR	//!< An pickup detector sensor interface ID

/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif
