/*!******************************************************************************
 * @file    pdr_sen_if.h
 * @brief   virtual pdr sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PDR_SEN_IF_H__
#define __PDR_SEN_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup SENSOR_ID_PDR PDR
 *  @{
 */
#define PDR_ID	SENSOR_ID_PDR //!< Pedestrian Dead Reckoning

/**
 * @struct pdr_sen_data_t
 * @brief Output data structure for pdr sensor
 * @brief world coordinate system ENU(x:East, y:North, z:Up)
 */
typedef struct {
	unsigned int	count;		//!< step counter
	FLOAT			rpos[2];	//!< relative position (0:x, 1:y) [m]
	FLOAT			velo[2];	//!< velocity (0:x, 1:y) [m/sec]
	FLOAT			total_dst;	//!< migration distance [m]
	unsigned int	hmode;		//!< hold mode value 0:stable, 2:swing
} pdr_sen_data_t;

/**
 * @name Command List
 */
//@{
/**
 * @brief Set holding mode
 * @param param[0] int 0:stable, 1:fix, 2:swing, 3:wrist, 4:auto, 5:watching
 * @return 0: OK, -1: Command Error, -2: Parameter Error
 */
#define PDR_SET_HOLDING_MODE		0x00

/**
 * @brief Set GeoMagnet status
 * @param param[0] int 0:not available, 1:available, 2:auto(not implement)
 * @return 0: OK, -1: Command Error, -2: Parameter Error
 */
#define PDR_SET_GEO_MAG_STATUS		0x01

/**
 * @brief Set Stride Mode
 * @param param[0] int 0:auto, 1:fix
 * @return 0: OK, -1: Command Error, -2: Parameter Error
 */
#define PDR_SET_STRIDE_MODE		0x02

/**
 * @brief Set Fix Stride
 * @param param[0] unsigned int length of step's stride [cm] (0~200)
 * @return 0: OK, -1: Command Error, -2: Parameter Error
 */
#define PDR_SET_FIX_STRIDE		0x03

/**
 * @brief Set Position Offset
 *
 * @param cmd_code PDR_SET_POS_OFFSET
 * @param param[0] frizz_fp: position offset of X-axis [m]
 * @param param[1] frizz_fp: position offset of Y-axis [m]
 *
 * @return 0: OK, -1: Command Error
 * @note new_x = cur_x + ofst_x
 * @note new_y = cur_y + ofst_y
 */
#define PDR_SET_POS_OFFSET		0x04

/**
 * @brief Set Yaw Offset
 *
 * @param cmd_code PDR_SET_YAW_OFFSET
 * @param param[0] frizz_fp degree of yaw offset [rad]
 *
 * @return 0: OK, -1: Command Error
 * @note new_yaw = cur_yaw + ofst_yaw
 */
#define PDR_SET_YAW_OFFSET		0x05

/**
 * @brief Get Version
 * @param cmd_code PDR_GET_VERSION
 * @return PDR version, -1: Command Error
 */
#define PDR_GET_VERSION			0xFF
//@}

/** @} */
#endif
