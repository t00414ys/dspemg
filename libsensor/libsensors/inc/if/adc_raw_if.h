/*!******************************************************************************
 * @file    adc_raw_if.h
 * @brief   virtual accel raw sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup ADC_RAW ADC RAW
 *  @ingroup physicalgroup Physical Sensor
 *  @brief adc raw sensor<BR>
 *  @brief -# <B>Contents</B><BR>
 *  @brief A sensor for taking out an adc component.<BR>
 *  <BR>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><BR>
 *  @brief Sensor ID : #SENSOR_ID_ADC_RAW <BR>
 *  Parent Sensor ID : none<BR>
 *  <BR>
 *  @brief -# <B>Detection Timing</B><BR>
 *  @brief	Detection Timing : Continuous (This sensor type output sensor
 *			data every priod when host determines sensor data or frizz optimized sensor data.)
 */

#ifndef __ADC_RAW_IF_H__
#define __ADC_RAW_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup ADC_RAW ADC RAW
 *  @{
 */

#define ADC_RAW_ID	SENSOR_ID_ADC_RAW //!< Adc from Physical Sensor interface ID

/**
 * @struct adc_raw_data_t
 * @brief Output data structure for an adc sensor
 */
typedef struct {
	unsigned int	data;	//!< data: adc data
} adc_raw_data_t;


/** @} */
#endif
