/*!******************************************************************************
 * @file    magn_util.c
 * @brief   virtual magnet utility sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_const.h"
#include "matrix.h"
#include "frizz_mem.h"
#include "frizz_type.h"
#include "frizz_math.h"
#include "mag_hard.h"
#include "mag_soft.h"
#include "mag_util.h"

//#define PLOT_LOCUS

/// utility of magnetism
typedef struct {
	Matrix		*m_m;				// 3 x 1: Magnet data
	Matrix		*h_m;				// 3 x 1: Hard-Iron
	Matrix		*S_m;				// 3 x 3: Soft-Iron

	Matrix		*T0_3x1;			// 3 x 1: Temp

	HardIron_h	hard;
	SoftIron_h	soft;
} MagUtil_t;

static MagUtil_t g_magutil;

static void get_matrix( Matrix *mtx, frizz_fp *data )
{
	int ii, jj;
	for( ii = 0; ii < mtx->rows; ii++ ) {
		for( jj = 0; jj < mtx->cols; jj++ ) {
			*data = mtx->data[ii][jj];
			data++;
		}
	}
}

/**
 * @brief create magnet utility
 */
void MagUtil_Create( void )
{
	// Matrix
	g_magutil.m_m = alloc_matrix( 3, 1 );
	g_magutil.h_m = alloc_matrix( 3, 1 );
	g_magutil.S_m = alloc_matrix( 3, 3 );

	g_magutil.T0_3x1 = alloc_matrix( 3, 1 );

	// Utility
	g_magutil.hard = HardIron_New();
	g_magutil.soft = SoftIron_New();
	// Iron
	g_magutil.h_m->data[0][0] =
		g_magutil.h_m->data[1][0] =
			g_magutil.h_m->data[2][0] = FRIZZ_CONST_ZERO;
	set_matrix_identity( g_magutil.S_m );

	SoftIron_Init( g_magutil.soft );
	HardIron_Init( g_magutil.hard );
}

/**
 * @brief destroy magnet utility
 */
void MagUtil_Destroy( void )
{
	HardIron_Delete( g_magutil.hard );
	SoftIron_Delete( g_magutil.soft );
	// Matrix
	free_matrix( g_magutil.m_m );
	free_matrix( g_magutil.h_m );
	free_matrix( g_magutil.S_m );

	free_matrix( g_magutil.T0_3x1 );
}

/**
 * @brief initialize magnet utility
 */
void MagUtil_Init( void )
{
	// Iron
	g_magutil.h_m->data[0][0] =
		g_magutil.h_m->data[1][0] =
			g_magutil.h_m->data[2][0] = FRIZZ_CONST_ZERO;
	set_matrix_identity( g_magutil.S_m );

	SoftIron_Init( g_magutil.soft );
	HardIron_Init( g_magutil.hard );
}

/**
 * @brief set calibration data
 *
 * @param mag[3] magnetic data
 */
void MagUtil_CalibPush( frizz_fp mag[3] )
{
	SoftIron_PushData( g_magutil.soft, mag );
}

/**
 * @brief calculate result of calibration
 *
 * @return -1:can't calculate, 0:success
 */
int MagUtil_CalibCalc( void )
{
	if( SoftIron_CalibCalc( g_magutil.soft, g_magutil.h_m, g_magutil.S_m ) == 0 ) {
		copy_matrix( g_magutil.h_m, g_magutil.T0_3x1 );
		mul_matrix( g_magutil.S_m, g_magutil.T0_3x1, g_magutil.h_m );
		return 0;
	}
	return -1;
}

#ifdef PLOT_LOCUS
static void debug_ellipsoid( frizz_fp mag[3] )
{
	frizz_fp **m;

	m = g_magutil.m_m->data;
	m[2][0] = frizz_div( FRIZZ_CONST_ONE, frizz_sqrt( mag[0] * mag[0] + mag[1] * mag[1] + mag[2] * mag[2] ) );
	m[0][0] = m[2][0] * mag[0];
	m[1][0] = m[2][0] * mag[1];
	m[2][0] = m[2][0] * mag[2];
	mul_matrix( g_magutil.soft->A_m, g_magutil.m_m, g_magutil.T0_3x1 );
	m = g_magutil.T0_3x1->data;
	printf( "na na na na na na na na na na na na na na na na na na " );
	printf( "%e %e %e ", m[0][0], m[1][0], m[2][0] );
	printf( "\n" );
}
#endif

/**
 * @brief magnetic sensor Hard-Iron collection
 *
 * @param mag[3] magnetic data
 */
void MagUtil_CompHard( frizz_fp mag[3] )
{
	frizz_fp **m;

	// set data
	m = g_magutil.m_m->data;
	m[0][0] = mag[0];
	m[1][0] = mag[1];
	m[2][0] = mag[2];
	// Hard-Iron
	sub_matrix( g_magutil.m_m, g_magutil.h_m, g_magutil.T0_3x1 );
	m = g_magutil.T0_3x1->data;
	mag[0] = m[0][0];
	mag[1] = m[1][0];
	mag[2] = m[2][0];
	//	if(HardIron_CalibPush(g_magutil.hard, mag)){
	//		HardIron_CalibCalc(g_magutil.hard, g_magutil.m_m);
	//	}
}

/**
 * @brief magnetic sensor Soft-Iron collection
 *
 * @param mag[3] magntic data
 */
void MagUtil_CompSoft( frizz_fp mag[3] )
{
	frizz_fp **m;

	// set data
	m = g_magutil.m_m->data;
	m[0][0] = mag[0];
	m[1][0] = mag[1];
	m[2][0] = mag[2];
	// Soft-Iron
	mul_matrix( g_magutil.S_m, g_magutil.m_m, g_magutil.T0_3x1 );
	m = g_magutil.T0_3x1->data;
	mag[0] = m[0][0];
	mag[1] = m[1][0];
	mag[2] = m[2][0];
}

/**
 * @brief set Hard-Iron collection value of magnetic sensor
 *
 * @param hard[3] Hard-Iron collection value
 */
void MagUtil_SetHardComp( frizz_fp hard[3] )
{
	set_matrix( g_magutil.h_m, hard );
}

/**
 * @brief get Hard-Iron collection value of magnetic sensor
 *
 * @param hard[3] Hard-Iron value
 */
void MagUtil_GetHardComp( frizz_fp hard[3] )
{
	get_matrix( g_magutil.h_m, hard );
}

/**
 * @brief set Hard-Iron collection value of magnetic sensor
 *
 * @param soft[9] Soft-Iron collection matrix setting
 */
void MagUtil_SetSoftComp( frizz_fp soft[9] )
{
	set_matrix( g_magutil.S_m, soft );
}

/**
 * @brief get Soft-Iron collection value of magnetic sensor
 *
 * @param soft[9] Soft-Iron collection matrix
 */
void MagUtil_GetSoftComp( frizz_fp soft[9] )
{
	get_matrix( g_magutil.S_m, soft );
}

