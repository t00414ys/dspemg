/*!******************************************************************************
 * @file    magn_calib_raw.c
 * @brief   virtual magnet calibration raw  sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <string.h>
#include "frizz_type.h"
#include "mag_util.h"
#include "sensor_if.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "frizz_util.h"
#include "libsensors_id.h"
#include "magn_calib_soft.h"
#include "if/magn_param_if.h"
#include "if/magn_calib_raw_if.h"

#define		SENSOR_VER_MAJOR		(1)						// Major Version
#define		SENSOR_VER_MINOR		(0)						// Minor Version
#define		SENSOR_VER_DETAIL		(0)						// Detail Version

#define DEF_INIT(x) x ## _init


#define MAGN_RAW_CALIB_INTERVAL_TICK	(10)

typedef struct {
	// ID
	unsigned char				id;
	unsigned char				par_ls[1];
	// IF
	sensor_if_t					pif;
	sensor_if_get_t				*p_magn;
	// status
	int							f_active;
	int							tick;
	int							f_need;
	unsigned int				ts;

	unsigned int				start_ts;
	unsigned int				send_ts;

	// data
	magnet_calib_raw_result_t	carib_result;
	magnet_calib_raw_data_t		carib_data;
	//unsigned char				bCount;
} device_sensor_t;

static device_sensor_t g_device;

EXTERN_C unsigned int hubhal_block_get_ts( void );

static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	*list = g_device.par_ls;
	return sizeof( g_device.par_ls );
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = &g_device.carib_data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return sizeof( g_device.carib_data ) / sizeof( int );
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
	if( gettor->id() == SENSOR_ID_MAGNET_RAW ) {
		g_device.p_magn = gettor;
	}
}

static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		DEBUG_PRINT( "MAGNET_CALIB_RAW set_active%d\r\n", f_active );
		g_device.f_active = f_active;
		hub_mgr_set_sensor_interval( g_device.id, g_device.p_magn->id(), MAGN_RAW_CALIB_INTERVAL_TICK );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_magn->id(), g_device.f_active );
		if( f_active ) {
			g_device.start_ts = hubhal_block_get_ts();
			g_device.send_ts = hubhal_block_get_ts();
			g_device.carib_data.status.bCount = 0;
			g_device.carib_data.status.bDoneInit = 0;
			g_device.carib_data.status.bMagnvalue = 0;
			g_device.carib_data.status.quality = 0;
		}
	}
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	//hub_mgr_set_sensor_interval(g_device.id, g_device.p_magn->id(), tick);
	return 0;	// On-event
}

static int get_interval( void )
{
	return 0;	// On-event
}

static int command( unsigned int cmd, void* param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case SENSOR_GET_VERSION:
		ret =	make_version( SENSOR_VER_MAJOR, SENSOR_VER_MINOR, SENSOR_VER_DETAIL );
		break;
	case MAGNET_CALIB_RAW_GET_STATUS:
		DEBUG_PRINT( "MAGNET_CALIB_RAW_GET_STATUS!!!\r\n" );
		hub_mgr_snd_cmd( g_device.id, g_device.p_magn->id(), INNER_SENSOR_CMD_CODE_TO_CMD( MAGNET_CALIB_RAW_GET_STATUS ), &g_device.carib_result );
		ret = *( int * ) &g_device.carib_result;
		break;
	case MAGNET_CALIB_RAW_SET_CALIBDATA:
		DEBUG_PRINT( "MAGNET_CALIB_RAW_SET_CALIBDATA!!!\r\n" );
		if( param != 0 ) {
			memcpy(	&g_device.carib_data, param , sizeof( g_device.carib_data ) );
			hub_mgr_snd_cmd( g_device.id, g_device.p_magn->id(), INNER_SENSOR_CMD_CODE_TO_CMD( MAGNET_CALIB_RAW_SET_CALIBDATA ), &g_device.carib_data );
			ret = 0;
		} else {
			ret = -1;
		}
		break;
	case MAGNET_CALIB_RAW_GET_CALIBDATA:
		DEBUG_PRINT( "MAGNET_CALIB_RAW_GET_CALIBDATA!!!\r\n" );
		hub_mgr_snd_cmd( g_device.id, g_device.p_magn->id(), INNER_SENSOR_CMD_CODE_TO_CMD( MAGNET_CALIB_RAW_GET_CALIBDATA ), &g_device.carib_data );
		g_device.f_need = 1;
		ret = 0;
		break;
	default:
		ret = RESULT_ERR_CMD;
		break;
	}
	return ret;
}

static unsigned int notify_ts( unsigned int ts )
{
	g_device.ts = ts;
	g_device.f_need = 1;
	return ts + g_device.tick;
}

static void notify_updated( unsigned char sen_id, SENSOR_NOTIFY ntfy )
{
}

#define		D_RESULT_MAX_TIME		(10000)
#define		D_WAIT_INTERVAL			(1000)
static int calculate( void )
{
	int 						ret = 0;
	unsigned int				save_data, get_data;
	magnet_calib_raw_result_t	carib_result;

	hub_mgr_snd_cmd( g_device.id, g_device.p_magn->id(), INNER_SENSOR_CMD_CODE_TO_CMD( MAGNET_CALIB_RAW_GET_STATUS ), &carib_result );
	save_data = *( unsigned int * ) &g_device.carib_data.status;
	get_data  = *( unsigned int * ) &carib_result;

	if( ( save_data != get_data ) && ( ( g_device.send_ts + D_WAIT_INTERVAL ) < hubhal_block_get_ts() ) ) {
		g_device.start_ts = 0;
		g_device.send_ts = hubhal_block_get_ts();

		hub_mgr_snd_cmd( g_device.id, g_device.p_magn->id(), INNER_SENSOR_CMD_CODE_TO_CMD( MAGNET_CALIB_RAW_GET_CALIBDATA ), &g_device.carib_data );
		g_device.carib_data.status = carib_result;

		DEBUG_PRINT( "SENSOR_ID_MAGNET_RAW EVENT!!!  [%d][%d][%d][%d]\r\n",	( int ) g_device.carib_data.status.bDoneInit,	( int ) g_device.carib_data.status.quality,
					 ( int ) g_device.carib_data.status.bMagnvalue,	( int ) g_device.carib_data.status.bCount );
		ret = 1;
	} else {
		if( ( g_device.start_ts != 0 ) && ( ( g_device.start_ts + D_RESULT_MAX_TIME ) < hubhal_block_get_ts() ) ) {
			g_device.start_ts = 0;
			g_device.send_ts = hubhal_block_get_ts();
			//
			hub_mgr_snd_cmd( g_device.id, g_device.p_magn->id(), INNER_SENSOR_CMD_CODE_TO_CMD( MAGNET_CALIB_RAW_GET_CALIBDATA ), &g_device.carib_data );
			g_device.carib_data.status = carib_result;
			DEBUG_PRINT( "SENSOR_ID_MAGNET_RAW EVENT!!!  [%d][%d][%d][%d]\r\n",	( int ) g_device.carib_data.status.bDoneInit,	( int ) g_device.carib_data.status.quality,
						 ( int ) g_device.carib_data.status.bMagnvalue, ( int ) g_device.carib_data.status.bCount );
			ret = 1;
		}
	}

	//
	g_device.f_need = 0;
	return ret;
}

sensor_if_t* DEF_INIT( magn_calib_raw )( void )
{
	// ID
	g_device.id = MAGNET_CALIB_RAW_ID;
	g_device.par_ls[0] = SENSOR_ID_MAGNET_RAW;
	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = notify_updated;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
	g_device.pif.end = 0;
	// param
	g_device.f_active = 0;
	g_device.tick = 10;
	g_device.f_need = 0;
	g_device.ts = 0;

	return &( g_device.pif );
}

