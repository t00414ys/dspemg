/*!******************************************************************************
 * @file    pa12201001.h
 * @brief   pa12201001 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PA12201001_H__
#define __PA12201001_H__
#include "pa12201001_api.h"

/*
 *
 *		The rest of this is okay not change
 *
 */


#define PA12201001_I2C_ADDRESS			0x1E		///< Ambient Light Sensor and Proximity Sensor(made from PixArt)

#define PA12201001_CFG0				0x00
#define PA12201001_CFG1				0x01
#define PA12201001_CGF2				0x02
#define PA12201001_CGF3				0x03
#define PA12201001_ATLL				0x04
#define PA12201001_ATLH				0x05
#define PA12201001_ATHL				0x06
#define PA12201001_ATHH				0x07
#define PA12201001_PTL				0x08

#define PA12201001_PTH				0x0A
#define PA12201001_ADL				0x0B
#define PA12201001_ADH				0x0C

#define PA12201001_PDH				0x0E

#define PA12201001_POFS				0x10
#define PA12201001_PSET				0x11

#define ALS_ACTIVE    		0x01
#define PS_ACTIVE    		0x02

#define ALS_INT_ACTIVE    	0x01
#define PS_INT_ACTIVE    	0x02

/*Driver Parameters  */

/*pa12200001 als/ps Default*/
#define PA12_ALS_TH_HIGH		35000
#define PA12_ALS_TH_LOW			30000
#define PA12_ALS_TH_RANGE		2000

#define PA12_PS_TH_HIGH			90
#define PA12_PS_TH_LOW			65
#define PA12_PS_TH_MIN		  	0	// Minimun value
#define PA12_PS_TH_MAX 			255     // 8 bit MAX

#define PA12_PS_TH_BASE_HIGH 		30
#define PA12_PS_TH_BASE_LOW		20

#define PA12_PS_NEAR_DISTANCE           0       //Near distance 0 cm
#define PA12_PS_FAR_DISTANCE            1       //Far distance 1 cm

#define PA12_PS_OFFSET_DEFAULT  	10 	// for X-talk cannceling
#define PA12_PS_OFFSET_MAX              100
#define PA12_PS_OFFSET_MIN              10
#define PA12_FAST_CAL			1
#define PA12_FAST_CAL_ONCE		0

#define PA12_ALS_GAIN			1 	// 0:125lux 1:1000lux 2:2000lux 3:10000lux
#define PA12_LED_CURR			2 	// 0:150mA 1:100mA 2:50mA 3:25mA

#define PA12_PS_PRST			2	// 0:1point 1:2points 2:4points 3:8points (for INT)
#define PA12_ALS_PRST			0	// 0:1point 1:2points 2:4points 3:8points (for INT)

#define PA12_PS_SET			1	// 0:ALS interrupt only 1:PS interrupt only 3:BOTH interrupt *no use now
#define PA12_PS_MODE			3	// 0:OFFSET 3:NORMAL

#define PA12_INT_TYPE			1 	// 0:Window type 1:Hysteresis type for Auto Clear flag , if ALS use interrupt mode,should use windows mode
#define PA12_PS_PERIOD			0//2	// 2:25 ms 3:50 ms
#define PA12_ALS_PERIOD			0	// 0:0 ms 1:100ms


#define ALS_PS_INT		        0 	//gpio_to_irq(xxxx) GPIO Define
#define ALS_POLLING		  	1	// 0:INT Mode 1:Polling Mode
#define PS_POLLING			1	// 0:INT Mode 1:Polling Mode

#define ALS_POLLING_DELAY		200	//ms
#define PS_POLLING_DELAY		200     //ms

#define I2C_RETRY_TIMES			3
#define I2C_RETRY_DELAY			10	//10ms

#define ALS_AVG_ENABLE			0


#endif // __PA12201001_H__

