/*!******************************************************************************
 * @file    ak0991X.h
 * @brief   ak0991X sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __AK0991X_H__
#define __AK0991X_H__
#include "ak0991X_api.h"
/*
 *
 *		By environment, you must edit this file
 *
 */

/*
 * This is use magnet calibration
 *(DEFINITED) : Enable  the calibration process maker create
 *(~DEFINITED): Disable the calibration process maker create
 */
//#define	D_USE_CALIB_AK09911
//#define	D_USE_CALIB_AK09912








/*
 *
 *		The rest of this is okay not change
 *
 */
#if 0
#include "config.h"
#if	defined(D_USE_CALIB_AK0991X)
#if	defined(USE_AK09911)
#define	D_USE_CALIB_AK09911
#endif
#if	defined(USE_AK09912)
#define	D_USE_CALIB_AK09912
#endif
#endif
#endif
#if	defined(D_USE_CALIB_AK09911) && defined(D_USE_CALIB_AK09912)
#error	"CALIB define Error"
#endif


// Constant
// Gauss = 10^2*uT
// 0.6 (uT/LSB) => 0.006 (Gauss/LSB)
#if defined(D_USE_CALIB_AK09911)
#define AK09911_MICRO_TESLA_PER_LSB		((float)(0.06f))
#else
#define AK09911_MICRO_TESLA_PER_LSB		((float)(0.6f))
#endif

#if defined(D_USE_CALIB_AK09912)
#define AK09912_MICRO_TESLA_PER_LSB		((float)(0.06f))
#else
#define AK09912_MICRO_TESLA_PER_LSB		((float)(0.15f))
#endif

#define AK0991X_I2C_ADDRESS_C		0x0C
#define AK0991X_I2C_ADDRESS_D		0x0D

// WHO AMDevice ID
#define AK09911_WHOAMI_ID			0x5
#define AK09912_WHOAMI_ID			0x4


// range with auto increment: 0x00-0x0C-0x00

#define AK0991X_WIA1				0x00		//Campany ID
#define AK0991X_WIA2				0x01		//Device ID
#define AK0991X_INFO1				0x02		//Information1
#define AK0991X_INFO2				0x03
#define AK0991X_ST1					0x10		//Data status
#define AK0991X_HXL					0x11		//Measurement Magnetic Data(X-axis data)
#define AK0991X_HXH					0x12		//Measurement Magnetic Data(X-axis data)
#define AK0991X_HYL					0x13		//Measurement Magnetic Data(Y-axis data)
#define AK0991X_HYH					0x14		//Measurement Magnetic Data(Y-axis data)
#define AK0991X_HZL					0x15		//Measurement Magnetic Data(Z-axis data)
#define AK0991X_HZH					0x16		//Measurement Magnetic Data(Z-axis data)
#define AK0991X_TMPS				0x17		//Dummy Register
#define AK0991X_ST2					0x18		//Data status
#define AK0991X_CNTL1				0x30		//Dummy Register
#define AK0991X_CNTL2				0x31		//Control settings
#define AK0991X_CNTL3				0x32		//Control settings

#define AK0991X_TS1					0x33	// Do Not Access

// range with auto increment: 0x10-0x12-0x10
#define AK0991X_FUSE_ASAX					0x60	// Fuse ROM
#define AK0991X_FUSE_ASAY					0x61	// Fuse ROM
#define AK0991X_FUSE_ASAZ					0x62	// Fuse ROM


// register constant
#define AK09911_CNTL2_SNG_MEASURE			0x01
#define AK09911_CNTL2_CONT_MEASURE_MODE1	0x02
#define AK09911_CNTL2_CONT_MEASURE_MODE2	0x04
#define AK09911_CNTL2_CONT_MEASURE_MODE3	0x06
#define AK09911_CNTL2_CONT_MEASURE_MODE4	0x08
#define AK09911_CNTL2_SELF_TEST				0x10
#define AK09911_CNTL2_FUSE_ACCESS			0x1F
#define AK09911_CNTL2_POWER_DOWN			0x00

#define AK09911_CNTL3_SOFT_RESET			0x01

#endif
