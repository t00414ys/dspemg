/*!******************************************************************************
 * @file    gp2ap054a10f_api.h
 * @brief   gp2ap054a10f sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __GP2AP054A10F_API_H__
#define __GP2AP054A10F_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int gp2ap054a10f_init( unsigned int param );
void gp2ap054a10f_ctrl_prox( int f_ena );
unsigned int gp2ap054a10f_rcv_prox( unsigned int tick );
int gp2ap054a10f_conv_prox( frizz_fp *prox_data );

void gp2ap054a10f_ctrl_light( int f_ena );
unsigned int gp2ap054a10f_rcv_light( unsigned int tick );
int gp2ap054a10f_conv_light( frizz_fp *light_data );

int gp2ap054a10f_setparam( void *ptr );
unsigned int gp2ap054a10f_get_ver( void );
unsigned int gp2ap054a10f_get_name( void );

int gp2ap054a10f_light_get_condition( void *data );
int gp2ap054a10f_prox_get_condition( void *data );

#ifdef __cplusplus
}
#endif


#endif // __GP2AP054A10F_API_H__
