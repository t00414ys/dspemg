/*!******************************************************************************
 * @file    bmd101.h
 * @brief   bmd101 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __BMD101_H__
#define __BMD101_H__
#include "bmd101_api.h"

#define CTRL_ACTIVATE					(1)
#define CTRL_DEACTIVATE					(0)

/*
 *
 *		The rest of this is okay not change
 *
 */

#define	SYNC	0xAA
#define	EXCODE	0x55

/* Parser types */
#define PARSER_TYPE_NULL 		0x00
#define PARSER_TYPE_PACKETS 	0x01 /* Stream bytes as ThinkGear Packets */
#define PARSER_TYPE_2BYTERAW 	0x02 /* Stream bytes as 2-byte raw data */
/* Data CODE definitions */
#define	PARSER_POOR_SIGNAL_CODE	0x02		// Signal Quality (0-sensor off, 200-sensor on) 
#define	PARSER_HEARTRATE_CODE	0x03		// Real-time Heart Rate (Beats Per Minute) 
#define	PARSER_RAW_CODE			0x80		// 16-bit Raw Data (2's Complement) 
#define	PARSER_DEBUGA_CODE		0x08		// Don't Care 
#define	PARSER_DEBUGB_CODE		0x84		// Don't Care 	
#define	PARSER_DEBUGC_CODE		0x85		// Don't Care 

#endif // __BMD101_H__
