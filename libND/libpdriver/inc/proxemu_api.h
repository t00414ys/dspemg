/*!******************************************************************************
 * @file    proxemu_api.h
 * @brief   sample program for accel/gyro sensor emulation
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PROXEMU_API_H__
#define __PROXEMU_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

//
int  proxemu_init( unsigned int param );
void proxemu_ctrl( int f_ena );
unsigned int proxemu_rcv( unsigned int tick );
int  proxemu_conv( frizz_fp* data );

int proxemu_setparam( void *ptr );
unsigned int proxemu_get_ver( void );
unsigned int proxemu_get_name( void );
int proxemu_get_condition( void *data );

#ifdef __cplusplus
}
#endif


//
#endif
