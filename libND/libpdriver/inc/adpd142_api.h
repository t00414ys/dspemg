/*!******************************************************************************
 * @file    adpd142_api.h
 * @brief   adap142 sensor api header
 * @par     copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ADPD142_API_H__
#define __ADPD142_API_H__

#include "frizz_type.h"

int adpd142_ppg_init( unsigned int param );
void adpd142_ppg_ctrl( int f_ena );
unsigned int adpd142_ppg_rcv( unsigned int tick );
int adpd142_ppg_conv( frizz_fp *data );
int adpd142_ppg_setparam( void *ptr );
unsigned int adpd142_ppg_get_ver( void );
unsigned int adpd142_ppg_get_name( void );

int adpd142_ppg_get_condition( void *data );

//
int adpd142_spo2_init( unsigned int param );
void adpd142_spo2_ctrl( int f_ena );
unsigned int adpd142_spo2_rcv( unsigned int tick );
int adpd142_spo2_conv( frizz_fp *data );
int adpd142_spo2_setparam( void *ptr );
unsigned int adpd142_get_ver( void );
unsigned int adpd142_get_name( void );

int adpd142_get_condition( void *data );


#endif // __ADPD142_API_H__

