/*!******************************************************************************
 * @file    mmc3516x_api.h
 * @brief   mmc3516x sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MMC3516X_API_H__
#define __MMC3516X_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int mmc3516x_init( unsigned int param );
void mmc3516x_ctrl( int f_ena );
unsigned int mmc3516x_rcv( unsigned int tick );
int mmc3516x_conv( frizz_fp data[3] );

int mmc3516x_setparam( void *ptr );
unsigned int mmc3516x_get_ver( void );
unsigned int mmc3516x_get_name( void );

int mmc3516x_calib_get_status( void *result );
int mmc3516x_calib_get_data( void *data );
int mmc3516x_calib_set_data( void *data );

int mmc3516x_get_condition( void *data );

#ifdef __cplusplus
}
#endif


#endif
