/*!******************************************************************************
 * @file    pah8001_api.h
 * @brief   pah8001 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PAH8001_API_H__
#define __PAH8001_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int pah8001_init( unsigned int param );
void pah8001_ctrl( int f_ena );
unsigned int pah8001_rcv( unsigned int tick );
int pah8001_conv( frizz_fp *data );
int pah8001_setparam( void *ptr );
unsigned int pah8001_get_ver( void );
unsigned int pah8001_get_name( void );

int pah8001_get_raw_data( void *data );
int pah8001_get_condition( void *data );

#ifdef __cplusplus
}
#endif

#endif
